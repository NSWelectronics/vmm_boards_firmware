----------------------------------------------------------------------------------
-- Company: NTUA - BNL
-- Engineer: Christos Bakalis, Panagiotis Gkountoumis & Paris Moschovakos
--
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis, Panagiotis Gkountoumis & Paris Moschovakos
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
--   
-- Create Date: 01.08.2018 14:36:27
-- Design Name: VMM Readout/Configuration top
-- Module Name: vmm_readout_top - RTL
-- Project Name: Depends on the board
-- Target Devices: Artix7 xc7a200t-2fbg484 & xc7a200t-3fbg484 
-- Tool Versions: Vivado 2018.2
-- Description: Top module
-- 
-- Dependencies: 
-- 
-- Changelog:
-- 04.08.2016 Added the XADC Component and multiplexer to share fifo UDP Signals (Reid Pinkham)
-- 11.08.2016 Corrected the fifo resets to go through select_data (Reid Pinkham)
-- 16.09.2016 Added Dynamic IP configuration. (Lev Kurilenko)
-- 16.02.2017 Added new configuration component (udp_data_in_handler) (Christos Bakalis)
-- 27.02.2017 Changed main logic clock to 125MHz (Paris)
-- 10.03.2017 Added configurable CKTP/CKBC module. (Christos Bakalis)
-- 12.03.2017 Changed flow_fsm's primary cktp assertion to comply with cktp_gen
-- module. (Christos Bakalis)
-- 14.03.2017 Added register address/value configuration scheme. (Christos Bakalis)
-- 28.03.2017 Changes to accomodate to MMFE8 VMM3. (Christos Bakalis)
-- 31.03.2017 Added 2 CKBC readout mode (Paris)
-- 30.04.2017 Added vmm_readout_wrapper that contains level-0 readout mode besides
-- the pre-existing continuous mode. (Christos Bakalis)
-- 06.06.2017 Added ART readout handling (Paris)
-- 12.06.2017 Added support for MMFE1 board (Paris)
-- 21.06.2017 Added support for GPVMM board (Paris)
-- 22.06.2017 Added ODDRs for VMM clock forwarding optimization. (Christos Bakalis)
-- 01.08.2018 Version 3 deployed. (Christos Bakalis)
--
----------------------------------------------------------------------------------
library UNISIM;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use UNISIM.VComponents.all;
use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;

entity vmm_readout_top is
    port(
        ------------------------------------
        ----------- VMM Interface ----------
        -- data
        DATA0_P     : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
        DATA0_N     : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
        DATA1_P     : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
        DATA1_N     : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
        ART_1_P     : IN  STD_LOGIC;
        ART_1_N     : IN  STD_LOGIC;
        -- configuration
        SDO         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
        SDI         : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        SCK         : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        CS          : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        ENA_P       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        ENA_N       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        TKI_P       : OUT STD_LOGIC;
        TKI_N       : OUT STD_LOGIC;
        TKI_MMFE8_P : OUT STD_LOGIC;
        TKI_MMFE8_N : OUT STD_LOGIC;
        TKO_P       : IN  STD_LOGIC;
        TKO_N       : IN  STD_LOGIC;
        -- clocks
        CKTK_P      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        CKTK_N      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        CKBC_P      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        CKBC_N      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        CKDT_P      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        CKDT_N      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        CKTP_P      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        CKTP_N      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        CKART_P     : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
        CKART_N     : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
        ------------------------------------
        ----- Board-Specific Interface -----
        LEMO_TRG    : IN  STD_LOGIC;
        REFCLK_P    : IN  STD_LOGIC;
        REFCLK_N    : IN  STD_LOGIC;
        TRG_OUT_P   : OUT STD_LOGIC;
        TRG_OUT_N   : OUT STD_LOGIC;
        MO          : OUT STD_LOGIC;
        GPIO_LED    : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
        ------------------------------------
        --------- xADC Interface -----------
        VN0         : IN  STD_LOGIC;
        VP0         : IN  STD_LOGIC;       
        VAUX0_V_N   : IN  STD_LOGIC;
        VAUX0_V_P   : IN  STD_LOGIC;
        VAUX1_V_N   : IN  STD_LOGIC;
        VAUX1_V_P   : IN  STD_LOGIC;
        VAUX2_V_N   : IN  STD_LOGIC;
        VAUX2_V_P   : IN  STD_LOGIC;
        VAUX3_V_N   : IN  STD_LOGIC;
        VAUX3_V_P   : IN  STD_LOGIC;
        VAUX8_V_N   : IN  STD_LOGIC;
        VAUX8_V_P   : IN  STD_LOGIC;
        VAUX9_V_N   : IN  STD_LOGIC;
        VAUX9_V_P   : IN  STD_LOGIC;
        VAUX10_V_N  : IN  STD_LOGIC;
        VAUX10_V_P  : IN  STD_LOGIC;
        VAUX11_V_N  : IN  STD_LOGIC;
        VAUX11_V_P  : IN  STD_LOGIC;
        MUXADDR     : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        MUXADDR_P   : OUT STD_LOGIC;
        MUXADDR_N   : OUT STD_LOGIC;
        ------------------------------------
        ----- Transceiver Interface --------
        GTREFCLK_P  : IN  STD_LOGIC;
        GTREFCLK_N  : IN  STD_LOGIC;
        RXP         : IN  STD_LOGIC;
        RXN         : IN  STD_LOGIC;
        TXP         : OUT STD_LOGIC;
        TXN         : OUT STD_LOGIC;
        PHY_INT     : OUT STD_LOGIC;
        PHY_RSTN    : OUT STD_LOGIC;
        ------------------------------------
        ----- Flash/EEPROM Interface -------
        -- spi
        IO0_IO      : INOUT STD_LOGIC;
        IO1_IO      : INOUT STD_LOGIC;
        SS_IO       : INOUT STD_LOGIC;
        -- i2c
        SDA         : INOUT STD_LOGIC;
        SCL         : OUT   STD_LOGIC;
        EEPROM_GND  : OUT   STD_LOGIC_VECTOR(3 DOWNTO 0)
    );
end vmm_readout_top;

architecture RTL of vmm_readout_top is

    -------------------------------------------------------------------
    -- Global Settings
    ------------------------------------------------------------------- 
    -- Default IP and MAC address of the board
    signal default_IP       : std_logic_vector(31 downto 0) := x"c0a80002";
    signal default_MAC      : std_logic_vector(47 downto 0) := x"002320189223";
    signal default_destIP   : std_logic_vector(31 downto 0) := x"c0a80010";

    -- Set to '1' for MU2E_RevB or MDT_446/436
    -- Set to '0' for later versions of MU2E board and all other boards
    constant is_old_mdt     : std_logic := '0';
    -- Set to '1' for MMFE8, or '0' for 1-VMM boards
    constant is_mmfe8       : std_logic := '0';
    -- Set to '0' for continuous readout mode, or '1' for L0 readout mode
    constant is_level0      : std_logic := '1';
    -- Set to '1' to switch to analog readout mode
    constant is_analog      : std_logic := '0';
    -- Set to '1' if using with VMM3a
    constant is_vmm3a       : std_logic := '1';
    -- Set to '1' if using external readout-inhibit signal
    constant use_inhib      : std_logic := '0';
    -- Set to '1' if want to enable external soft_reset acceptance
    constant use_extSR      : std_logic := '0';
    -- Set to large than 12 in dec to activate the periodic soft reset (do not use with analog)
    -- use_extSR must be zero for periodic soft reset to work
    constant sr_offset      : std_logic_vector(11 downto 0)    := x"001"; -- 400/4= ~100 CKBC

    -- ART-readout related (MMFE1)
    -- Set to '1' to enable the ART deserializing
    constant artEnabled     : std_logic := '1';
    -- Set to '1' when the art is coming faster than the external trigger
    constant slowTrigger    : std_logic := '1';


    -------------------------------------------------------------------
    -- Component Declaration
    -------------------------------------------------------------------
component clk_wiz_gen
    port(
        -- Clock in ports
        clk_in1_p       : in  std_logic;
        clk_in1_n       : in  std_logic;
        -- Clock out ports
        clk_out_40      : out std_logic;
        clk_out_40_90   : out std_logic;
        clk_out_160     : out std_logic;
        clk_out_320     : out std_logic;
        -- Status and control signals
        reset           : in  std_logic;
        gen_locked      : out std_logic
    );
end component;

component clk_wiz_aux
    port(
        -- Clock in ports
        clk_in1         : in  std_logic;
        -- Clock out ports
        clk_out_50      : out std_logic;
        clk_out_200     : out std_logic;
        clk_out_500     : out std_logic;
        -- Status and control signals
        reset           : in  std_logic;
        aux_locked      : out std_logic
    );
end component;

component startup is
    port(
        clk_40          : in  std_logic;
        mmcm_locked     : in  std_logic;
        tr_hold         : in  std_logic;
        gpio_led        : out std_logic_vector(5 downto 0);
        phy_rstn_out    : out std_logic;
        rst_init_out    : out std_logic
    );
end component;

component vmm_intf_block
    generic(is_mmfe8    : std_logic := '1';
            is_level0   : std_logic := '0');
    port(
        ------------------------------------
        ----- FPGA Logic Interface ---------
        clk_40              : in  std_logic;
        clk_160             : in  std_logic;
        clk_320             : in  std_logic;
        mmcm_locked         : in  std_logic;
        cktp                : in  std_logic;
        ckdt                : in  std_logic;
        ckbc                : in  std_logic;
        ckbc_mode           : in  std_logic;
        trg_en              : in  std_logic;
        req_ckbc            : in  std_logic;
        ckbc_max_num        : in  std_logic_vector(7 downto 0);
        ckbc_freq           : in  std_logic_vector(5 downto 0);
        vmm_tki_in          : in  std_logic;
        vmm_tko_out         : out std_logic;
        ext_trg_out         : out std_logic;
        ext_rst_out         : out std_logic;
        ext_inh_out         : out std_logic;
        ext_busy_in         : in  std_logic;
        vmm_ckbc_enable     : in  std_logic;
        vmm_cktp_enable     : in  std_logic;
        vmm_ckdt_enable     : in  std_logic_vector(7 downto 0);
        vmm_ena_in          : in  std_logic_vector(7 downto 0);
        vmm_cs_in           : in  std_logic_vector(7 downto 0);
        vmm_sdi_in          : in  std_logic_vector(7 downto 0);
        vmm_sck_in          : in  std_logic_vector(7 downto 0);
        vmm_cktk_in         : in  std_logic_vector(7 downto 0);
        vmm_data0_out       : out std_logic_vector(7 downto 0);
        vmm_data1_out       : out std_logic_vector(7 downto 0);
        ------------------------------------
        ------------ VMM Interface ---------
        vmm_ena_out         : out std_logic_vector(7 downto 0);
        vmm_cktk_out        : out std_logic_vector(7 downto 0);
        vmm_ckbc_out        : out std_logic_vector(7 downto 0);
        vmm_ckdt_out        : out std_logic_vector(7 downto 0);
        vmm_cktp_out        : out std_logic_vector(7 downto 0);
        vmm_ckart_out       : out std_logic_vector(8 downto 0);
        vmm_sdi_out         : out std_logic_vector(7 downto 0);
        vmm_sck_out         : out std_logic_vector(7 downto 0);
        vmm_cs_out          : out std_logic_vector(7 downto 0);
        vmm_tki_out         : out std_logic;
        vmm_tki_mmfe8_out   : out std_logic;
        vmm_tko_in          : in  std_logic;
        vmm_data0_in        : in  std_logic_vector(7 downto 0);
        vmm_data1_in        : in  std_logic_vector(7 downto 0);
        ------------------------------------
        ------------ Ext Interface ---------
        ext_rst_in          : in  std_logic;
        ext_trg_in          : in  std_logic;
        ext_inh_in          : in  std_logic;
        ext_busy_out        : out std_logic
    );
end component;

component flow_fsm
    generic(is_level0   : std_logic := '0';
            is_vmm3a    : std_logic := '0';
            use_extSR   : std_logic := '0';
            use_inhib   : std_logic := '0');
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_40          : in  std_logic;
        rst             : in  std_logic;
        daq_inhib       : in  std_logic;
        trg_mode        : in  std_logic;
        analog_mode     : in  std_logic;
        ext_sr          : in  std_logic;
        per_sr          : in  std_logic;
        mmcm_locked     : in  std_logic;
        st_flow_dbg     : out std_logic_vector(4 downto 0);
        ------------------------------------
        ----- UDP din/dout Interface -------
        vmm_bitmask     : in  std_logic_vector(7 downto 0);
        vmm_conf        : in  std_logic;
        vmm_confRdy     : in  std_logic;
        vmm_confEnd     : in  std_logic;
        ip_conf         : in  std_logic;
        xadc_conf       : in  std_logic;
        daq_on          : in  std_logic;
        cs_udp          : in  std_logic;
        sck_udp         : in  std_logic;
        sdi_udp         : in  std_logic;
        reply_done      : in  std_logic;
        udp_done        : in  std_logic;
        reply_enable    : out std_logic;
        sel_reply       : out std_logic;
        vmm_confEnable  : out std_logic;
        ------------------------------------
        -------- Submodules Interface ------
        xadc_busy       : in  std_logic;
        ip_busy         : in  std_logic;
        tki_analog      : in  std_logic;
        vmm_ena_analog  : in  std_logic;
        trg_ext         : in  std_logic;
        cktp_enable     : out std_logic;
        cktp_primary    : out std_logic;
        trg_enable      : out std_logic;
        daq_enable      : out std_logic;
        analog_enable   : out std_logic;
        xadc_start      : out std_logic;
        ip_start        : out std_logic;
        rst_readout     : out std_logic;
        inhib_conf      : out std_logic;
        ------------------------------------
        ------------ VMM Interface ---------
        ckbc_enable     : out std_logic;
        vmm_ena         : out std_logic_vector(7 downto 0);
        vmm_cs          : out std_logic_vector(7 downto 0);
        vmm_sdi         : out std_logic_vector(7 downto 0);
        vmm_sck         : out std_logic_vector(7 downto 0);
        vmm_tki         : out std_logic
     );
end component;

component vmmFE_eth_wrapper
    port(
        ---------------------------------
        ------ General Interface --------
        clk_200         : in  std_logic;    -- Independent clock (from BUFG)
        clk_40          : in  std_logic;    -- For CDCC
        fifo_init       : in  std_logic;    -- Initialize the ICMP FIFO
        rst_eth         : in  std_logic;    -- Reset the ethernet cores
        FPGA_IP         : in  std_logic_vector(31 downto 0);
        FPGA_MAC        : in  std_logic_vector(47 downto 0);
        userclk2        : out std_logic;    -- 125 Mhz for user logic
        TXRXmmcm_locked : out std_logic;    -- Transceiver MMCM lock signal
        ---------------------------------
        ---- UDP/ICMP Block Interface ---
        udp_tx_start    : in  std_logic;
        udp_txi         : in  udp_tx_type;
        udp_rxo         : out udp_rx_type;
        udp_tx_dout_rdy : out std_logic;
        ---------------------------------
        ----- Transceiver Interface -----
        gtrefclk_p      : in  std_logic;    -- Differential +ve of reference clock for transceiver: 125MHz, very high quality
        gtrefclk_n      : in  std_logic;    -- Differential -ve of reference clock for transceiver: 125MHz, very high quality
        rxp             : in  std_logic;    -- Differential +ve for serial reception from PMD to PMA.
        rxn             : in  std_logic;    -- Differential -ve for serial reception from PMD to PMA.
        txp             : out std_logic;    -- Differential +ve of serial transmission from PMA to PMD.
        txn             : out std_logic     -- Differential -ve of serial transmission from PMA to PMD.
    );
end component;

component udp_data_in_handler
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_125             : in  std_logic;
        clk_40              : in  std_logic;
        inhibit_conf        : in  std_logic;
        rst                 : in  std_logic;
        rst_fifo_init       : in  std_logic;
        state_o             : out std_logic_vector(2 downto 0);
        valid_o             : out std_logic;
        ------------------------------------
        -------- FPGA Config Interface -----
        latency             : out std_logic_vector(15 downto 0);
        latency_extra       : out std_logic_vector(15 downto 0);
        serial_number       : out std_logic_vector(31 downto 0);
        artTimeout          : out std_logic_vector(7 downto 0);
        daq_on              : out std_logic;
        analog_on           : out std_logic;
        ext_trigger         : out std_logic;
        ckbcMode            : out std_logic;
        fpga_rst            : out std_logic;
        ------------------------------------
        -------- UDP Interface -------------
        udp_rx              : in  udp_rx_type;
        ------------------------------------
        ---------- AXI4SPI Interface -------
        flash_busy          : in  std_logic;
        newIP_rdy           : out std_logic;
        myIP_set            : out std_logic_vector(31 downto 0);
        myMAC_set           : out std_logic_vector(47 downto 0);
        destIP_set          : out std_logic_vector(31 downto 0);
        ------------------------------------
        -------- CKTP/CKBC Interface -------
        ckbc_freq           : out std_logic_vector(7 downto 0);
        cktk_max_num        : out std_logic_vector(7 downto 0);
        cktp_max_num        : out std_logic_vector(15 downto 0);
        cktp_skew           : out std_logic_vector(7 downto 0);
        cktp_period         : out std_logic_vector(15 downto 0);
        cktp_width          : out std_logic_vector(7 downto 0);
        ckbc_max_num        : out std_logic_vector(7 downto 0);
        ------------------------------------
        ------ VMM Config Interface --------
        vmm_bitmask         : out std_logic_vector(7 downto 0);
        vmmConf_came        : out std_logic;
        vmmConf_rdy         : out std_logic;
        vmmConf_done        : out std_logic;
        vmm_sck             : out std_logic;
        vmm_cs              : out std_logic;
        vmm_cfg_bit         : out std_logic;
        top_rdy             : in  std_logic;
        ------------------------------------
        ---------- XADC Interface ----------
        xadc_busy           : in  std_logic;
        xadc_rdy            : out std_logic;
        vmm_id_xadc         : out std_logic_vector(15 downto 0);
        xadc_sample_size    : out std_logic_vector(10 downto 0);
        xadc_delay          : out std_logic_vector(17 downto 0)
    );
end component;

component udp_reply_handler
    port(
        ------------------------------------
        ------- General Interface ----------
        clk             : in  std_logic;
        enable          : in  std_logic;
        serial_number   : in  std_logic_vector(31 downto 0);
        UDP_done        : in  std_logic;
        reply_done      : out std_logic;
        ------------------------------------
        ---- FIFO Data Select Interface ----
        rst_fifo        : out std_logic;
        wr_en_conf      : out std_logic;
        dout_conf       : out std_logic_vector(15 downto 0);
        packet_len_conf : out std_logic_vector(11 downto 0);
        end_conf        : out std_logic       
    );
end component;

component FIFO2UDP
    port( 
        clk_125                 : in  std_logic;
        clk_160                 : in  std_logic;
        destinationIP           : in  std_logic_vector(31 downto 0);
        daq_data_in             : in  std_logic_vector(15 downto 0);
        fifo_data_out           : out std_logic_vector (7 downto 0);
        udp_txi                 : out udp_tx_type;  
        udp_tx_start            : out std_logic;
        UDPDone                 : out std_logic;
        UDPBusy                 : out std_logic;
        xadc_busy               : in  std_logic;
        analog_mode             : in  std_logic;
        udp_tx_data_out_ready   : in  std_logic;
        wr_en                   : in  std_logic;
        end_packet              : in  std_logic;
        global_reset            : in  std_logic;
        packet_length_in        : in  std_logic_vector(11 downto 0);
        reset_DAQ_FIFO          : in  std_logic;
        confReply_packet        : in  std_logic
    );
end component;

component vmm_readout_wrapper
    generic(is_vmm3a    : std_logic;
            is_mmfe8    : std_logic;
            is_level0   : std_logic;
            is_analog   : std_logic);
    port(
        ------------------------------------
        --- Continuous Readout Interface ---
        clkTkProc       : in  std_logic;                    -- Used to clock checking for data process
        clkDtProc       : in  std_logic;                    -- Used to clock word readout process
        clk             : in  std_logic;                    -- Main clock
        --
        daq_enable      : in  std_logic;
        trigger_pulse   : in  std_logic;                     -- Trigger
        cktk_max        : in  std_logic_vector(7 downto 0);  -- Max number of CKTKs
        --
        dt_state_o      : out std_logic_vector(3 downto 0); -- for debugging
        dt_cntr_st_o    : out std_logic_vector(3 downto 0); -- for debugging
        ------------------------------------
        ---- Level-0 Readout Interface -----
        clk_ckdt        : in  std_logic;                    -- will be forwarded to the VMM
        clk_ckbc        : in  std_logic;
        rst_buff        : in  std_logic;                    -- reset the level-0 buffer
        rst_intf_proc   : in  std_logic;                    -- reset the pf interface
        --
        level_0         : in  std_logic;                    -- level-0 signal
        wr_accept       : in  std_logic;                    -- buffer acceptance window
        --
        vmm_conf        : in  std_logic;                    -- high during VMM configuration
        daq_on_inhib    : out std_logic;                    -- prevent daq_on state before checking link health
        level0_idle     : out std_logic;
        l0_sreg_out     : out std_logic;
        --
        state_buff_dbg  : out std_logic_vector(1 downto 0);
        fifo_full_dbg   : out std_logic;
        commas_dbg      : out std_logic;
        reset_done      : out std_logic;
        ------------------------------------
        -- 2phaseAnalog Readout Interface --
        fifo_init       : in  std_logic;
        analog_on       : in  std_logic;
        analog_busy     : out std_logic;
        trint           : in  std_logic;
        xadc_busy       : in  std_logic;
        xadc_start      : out std_logic;
        xadc_read_tdo   : out std_logic;
        UDPDone         : in  std_logic;
        wr_en_udp       : out std_logic;
        packLen_udp     : out std_logic_vector(11 downto 0);
        endPacket_udp   : out std_logic;
        dout_udp        : out std_logic_vector(15 downto 0);
        vmm_ena         : out std_logic;
        vmm_tki         : out std_logic;
        ------------------------------------
        ---- Packet Formation Interface ----
        vmmWordReady    : out std_logic;
        vmmWord         : out std_logic_vector(15 downto 0);
        vmmEventDone    : out std_logic;
        rd_ena_buff     : in  std_logic;                     -- read the readout buffer (level0 or continuous)
        vmmId           : in  std_logic_vector(2 downto 0);  -- VMM to be readout
        linkHealth_bmsk : out std_logic_vector(7 downto 0);  -- status of comma alignment links
        ------------------------------------
        ---------- VMM3 Interface ----------
        vmm_data0_vec   : in  std_logic_vector(7 downto 0);  -- Single-ended data0 from VMM
        vmm_data1_vec   : in  std_logic_vector(7 downto 0);  -- Single-ended data1 from VMM
        vmm_ckdt_glbl   : out std_logic;                     -- Strobe to VMM CKDT
        vmm_ckdt_enable : out std_logic_vector(7 downto 0);  -- Enable signal for VMM CKDT
        vmm_cktk_vec    : out std_logic_vector(7 downto 0)   -- Strobe to VMM CKTK
    );
end component;

component packet_formation
    generic(is_mmfe8        : std_logic;
            is_level0       : std_logic;
            artEnabled      : std_logic);
    port(
        clk_art         : in std_logic;
        bc_clk          : in std_logic;
        reset           : in std_logic;
        fifo_init       : in std_logic;
        vmm_ena         : in std_logic;
        ckbc_enable     : in std_logic;
        CKBCstrobe      : in std_logic;

        newCycle        : in std_logic;
        
        trigVmmRo       : out std_logic;
        vmmId           : out std_logic_vector(2 downto 0);
        vmmWord         : in std_logic_vector(15 downto 0);
        vmmWordReady    : in std_logic;
        vmmEventDone    : in std_logic;

        UDPBusy         : in std_logic;
        UDPDone         : in std_logic;
        inhibit         : in std_logic;
        pfBusy          : out std_logic;                        -- Control signal to ETR
        glBCID          : in std_logic_vector(11 downto 0);     -- glBCID counter from ETR

        packLen         : out std_logic_vector(11 downto 0);
        dataout         : out std_logic_vector(15 downto 0);
        wrenable        : out std_logic;
        end_packet      : out std_logic;
        wr_accept       : out std_logic;
        
        rd_ena_buff     : out std_logic;
        rst_l0          : out std_logic;
        level0_idle     : in  std_logic;
        trig_l0         : in  std_logic;
        latency_done    : in  std_logic;
        level0_resetDone: in  std_logic;
        
        tr_hold         : out std_logic;
        
        rst_vmm         : out std_logic;
        linkHealth_bmsk : in std_logic_vector(8 downto 1);
        rst_FIFO        : out std_logic;
        event_cnt_o     : out std_logic_Vector(31 downto 0);
        
        latency         : in std_logic_vector(15 downto 0);
        dbg_st_o        : out std_logic_vector(4 downto 0);
        tr_raw          : in std_logic;
        vmmArtData      : in std_logic_vector(5 downto 0);
        art2trigger     : in std_logic_vector(5 downto 0);
        vmmArtReady     : in std_logic
    );
end component;

component trigger
    generic(is_level0 : std_logic);
    port(
        ckbc                : in  std_logic;
        clk_art             : in  std_logic;
        clk_art2            : in  std_logic;
        rst_trig            : in  std_logic;
        state_trg           : out std_logic_vector(2 downto 0);
        
        ckbcMode            : in  std_logic;
        cktp_enable         : in  std_logic;
        cktp_pulse_width    : in  std_logic_vector(4 downto 0);
        CKTP_raw            : in  std_logic;
        request2ckbc        : out std_logic;
        
        tr_en               : in  std_logic; -- synced to 160
        tr_hold             : in  std_logic; -- synced to 160
        tr_mode             : in  std_logic;
        tr_ext              : in  std_logic; -- synced to 160
        level_0             : out std_logic;
        busy_l0             : out std_logic;
        trigger_pf          : out std_logic;

        ckbc_max_conf       : in  std_logic_vector(7 downto 0);
        ckbc_max_num        : out std_logic_vector(7 downto 0);
        latency_extra       : in  std_logic_vector(15 downto 0);
        latency             : in  std_logic_vector(15 downto 0);

        tr_out              : out std_logic;
        tr_raw              : out std_logic
    );
end component;

component artReadout
    generic(is_mmfe8    : std_logic;
            artEnabled  : std_logic;
            slowTrigger : std_logic);
    port(
        clk_art         : in std_logic;
        trigger160      : in std_logic;
        tr_hold         : in std_logic;
        artData         : in std_logic_vector(7 downto 0);
        art2trigger     : out std_logic_vector(5 downto 0);
        vmmArtData160   : out std_logic_vector(5 downto 0);
        vmmArtReady     : out std_logic;
        artTimeout      : in std_logic_vector(7 downto 0);
        request2ckbc    : in std_logic
        );
end component;

component select_data
    port(
        configuring             : in  std_logic;
        analog                  : in  std_logic;
        data_acq                : in  std_logic;
        xadc                    : in  std_logic;
        we_analog               : in  std_logic;
        we_data                 : in  std_logic;
        we_conf                 : in  std_logic;
        we_xadc                 : in  std_logic;
        analog_data_in          : in  std_logic_vector(15 downto 0);
        daq_data_in             : in  std_logic_vector(15 downto 0);
        conf_data_in            : in  std_logic_vector(15 downto 0);
        xadc_data_in            : in  std_logic_vector(15 downto 0);
        analog_packet_length    : in  std_logic_vector(11 downto 0);
        data_packet_length      : in  std_logic_vector(11 downto 0);
        xadc_packet_length      : in  std_logic_vector(11 downto 0);
        conf_packet_length      : in  std_logic_vector(11 downto 0);
        end_packet_analog       : in  std_logic;
        end_packet_conf         : in  std_logic;
        end_packet_daq          : in  std_logic;
        end_packet_xadc         : in  std_logic;
        fifo_rst_daq            : in  std_logic;
        fifo_rst_xadc           : in  std_logic;
        fifo_rst_conf           : in  std_logic;
        rstFIFO_top             : in  std_logic;

        data_out                : out std_logic_vector(15 downto 0);
        packet_length           : out std_logic_vector(11 downto 0);
        we                      : out std_logic;
        end_packet              : out std_logic;
        fifo_rst                : out std_logic
    );
end component;

component xadcModule
    port(
        clk                 : in std_logic;
        rst                 : in std_logic;
        
        VP_0                : in std_logic;
        VN_0                : in std_logic;
        Vaux0_v_n           : in std_logic;
        Vaux0_v_p           : in std_logic;
        Vaux1_v_n           : in std_logic;
        Vaux1_v_p           : in std_logic;
        Vaux2_v_n           : in std_logic;
        Vaux2_v_p           : in std_logic;
        Vaux3_v_n           : in std_logic;
        Vaux3_v_p           : in std_logic;
        Vaux8_v_n           : in std_logic;
        Vaux8_v_p           : in std_logic;
        Vaux9_v_n           : in std_logic;
        Vaux9_v_p           : in std_logic;
        Vaux10_v_n          : in std_logic;
        Vaux10_v_p          : in std_logic;
        Vaux11_v_n          : in std_logic;
        Vaux11_v_p          : in std_logic;
        data_in_rdy         : in std_logic;
        read_tdo            : in std_logic;
        vmm_id              : in std_logic_vector(15 downto 0);
        sample_size         : in std_logic_vector(10 downto 0);
        delay_in            : in std_logic_vector(17 downto 0);
        UDPDone             : in std_logic;

        MuxAddr0            : out std_logic;
        MuxAddr1            : out std_logic;
        MuxAddr2            : out std_logic;
        MuxAddr3_p          : out std_logic;
        MuxAddr3_n          : out std_logic;
        end_of_data         : out std_logic;
        fifo_bus            : out std_logic_vector(15 downto 0);
        data_fifo_enable    : out std_logic;
        packet_len          : out std_logic_vector(11 downto 0);
        xadc_busy           : out std_logic
    );
end component;

component AXI4_SPI
    port(
        clk_125         : in  std_logic;
        clk_50          : in  std_logic;
        clk_40          : in  std_logic;

        myIP            : out std_logic_vector(31 downto 0);    -- Signal going out to mmfe8_top and used as main IP
        myMAC           : out std_logic_vector(47 downto 0);    -- Signal going out to mmfe8_top and used as main MAC
        destIP          : out std_logic_vector(31 downto 0);    -- Signal going out to mmfe8_top and used as main destIP

        default_IP      : in std_logic_vector(31 downto 0);
        default_MAC     : in std_logic_vector(47 downto 0);
        default_destIP  : in std_logic_vector(31 downto 0);

        myIP_set        : in std_logic_vector(31 downto 0);     -- Signal coming from config_logic. Used to set myIP
        myMAC_set       : in std_logic_vector(47 downto 0);     -- Signal coming from config_logic. Used to set myMAC
        destIP_set      : in std_logic_vector(31 downto 0);     -- Signal coming from config_logic. Used to set destIP

        newip_start     : in std_logic;                         -- Flag that initiates the process for setting newIP
        flash_busy      : out std_logic;                        -- Flag that indicates the module is busy setting IP

        -- refer to Micron documentation for the signals below: https://www.micron.com/~/media/documents/products/data-sheet/nor-flash/serial-nor/n25q/n25q_256mb_1_8v.pdf
        io0_i           : in std_logic;                         -- Signals for DQ0 (MOSI)
        io0_o           : out std_logic;
        io0_t           : out std_logic;
        io1_i           : in std_logic;                         -- Signals for DQ1 (MISO)
        io1_o           : out std_logic;
        io1_t           : out std_logic;
        ss_i            : in std_logic_vector(0 downto 0);      -- Slave Select
        ss_o            : out std_logic_vector(0 downto 0);
        ss_t            : out std_logic
    );
end component;

component i2c_wrapper
    port( 
        CLK_50_MHz          : in    std_logic;
        CLK_40_MHz          : in    std_logic;
        CLK_125_MHz         : in    std_logic;
        new_IP_ready        : in    std_logic;
        module_busy         : out   std_logic;
        SDA                 : inout std_logic;
        SCL                 : out   std_logic;
        destIP              : in    std_logic_vector(31 downto 0);
        srcIP               : in    std_logic_vector(31 downto 0);
        mac                 : in    std_logic_vector(47 downto 0);
        destIP_returned     : out   std_logic_vector(31 downto 0);
        srcIP_returned      : out   std_logic_vector(31 downto 0);
        mac_returned        : out   std_logic_vector(47 downto 0);
        destIP_default_val  : in    std_logic_vector(31 downto 0);
        srcIP_default_val   : in    std_logic_vector(31 downto 0);
        mac_default_val     : in    std_logic_vector(47 downto 0)
     );
end component;

component clk_gen_wrapper
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_500             : in  std_logic;
        clk_160             : in  std_logic;
        clk_40              : in  std_logic;
        rst                 : in  std_logic;
        mmcm_locked         : in  std_logic;
        CKTP_raw            : out std_logic;
        ------------------------------------
        ----- Configuration Interface ------
        cktp_enable         : in  std_logic;
        cktp_primary        : in  std_logic;
        readout_mode        : in  std_logic;
        cktp_pulse_width    : in  std_logic_vector(4 downto 0);
        cktp_max_num        : in  std_logic_vector(15 downto 0);
        cktp_period         : in  std_logic_vector(15 downto 0);
        sr_offset           : in  std_logic_vector(11 downto 0);
        cktp_skew           : in  std_logic_vector(4 downto 0);        
        ckbc_freq           : in  std_logic_vector(5 downto 0);
        ckbc_max_num        : in  std_logic_vector(7 downto 0);
        ------------------------------------
        ---------- VMM Interface -----------
        VMM_ENA             : out std_logic;
        CKTP                : out std_logic
    );
end component;

component ila_overview
    port(
        clk     : in std_logic;
        probe0  : in std_logic_vector(63 downto 0)
    );
end component;

-------------------------------------------------------------------
-- Signal Declaration
-------------------------------------------------------------------

    -- clocks/MMCM
    signal clk_40           : std_logic := '0';
    signal clk_40_90        : std_logic := '0';
    signal clk_160          : std_logic := '0';
    signal clk_320          : std_logic := '0';

    signal clk_50           : std_logic := '0';
    signal clk_200          : std_logic := '0';
    signal clk_500          : std_logic := '0';
    signal master_locked    : std_logic := '0';
    signal aux_locked       : std_logic := '0';
    signal eth_locked       : std_logic := '0';
    signal all_pll_locked   : std_logic := '0';

    -- ethernet core
    signal clk_eth          : std_logic := '0';
    signal udp_tx_start     : std_logic := '0';
    signal udp_tx_dout_rdy  : std_logic := '0';
    signal udp_rx           : udp_rx_type;
    signal udp_tx           : udp_tx_type;

    -- vmm signals
    signal art_1_buf        : std_logic := '0';
    signal tki_buf          : std_logic := '0';
    signal tko_buf          : std_logic := '0';
    signal tki_mmfe8_buf    : std_logic := '0';
    signal art_in           : std_logic := '0';
    signal ext_trg_i        : std_logic := '0';
    signal data0_buf        : std_logic_vector(7 downto 0) := (others => '0');
    signal data1_buf        : std_logic_vector(7 downto 0) := (others => '0');
    signal sdo_buf          : std_logic_vector(7 downto 0) := (others => '0');
    signal sdi_buf          : std_logic_vector(7 downto 0) := (others => '0');
    signal sck_buf          : std_logic_vector(7 downto 0) := (others => '0');
    signal cs_buf           : std_logic_vector(7 downto 0) := (others => '0');
    signal ena_buf          : std_logic_vector(7 downto 0) := (others => '0');
    signal cktk_buf         : std_logic_vector(7 downto 0) := (others => '0');
    signal ckbc_buf         : std_logic_vector(7 downto 0) := (others => '0');
    signal ckdt_buf         : std_logic_vector(7 downto 0) := (others => '0');
    signal cktp_buf         : std_logic_vector(7 downto 0) := (others => '0');

    signal vmm_data0_i      : std_logic_vector(7 downto 0) := (others => '0');
    signal vmm_data1_i      : std_logic_vector(7 downto 0) := (others => '0');
    signal vmm_sdi_i        : std_logic_vector(7 downto 0) := (others => '0');
    signal vmm_sck_i        : std_logic_vector(7 downto 0) := (others => '0');
    signal vmm_cs_i         : std_logic_vector(7 downto 0) := (others => '0');
    signal vmm_ena_i        : std_logic_vector(7 downto 0) := (others => '0');
    signal vmm_cktk_i       : std_logic_vector(7 downto 0) := (others => '0');

    signal CKDT_glbl        : std_logic := '0';
    signal CKBC_glbl        : std_logic := '0';    
    signal vmm_tki_i        : std_logic := '0';
    signal vmm_tko_i        : std_logic := '0';
    signal ext_rst_i        : std_logic := '0';
    signal ext_inh_i        : std_logic := '0';
    signal vmm_ckbc_enable  : std_logic := '0';
    signal vmm_ckdt_enable  : std_logic_vector(7 downto 0) := (others => '0');
    signal ckart_buf        : std_logic_vector(8 downto 0) := (others => '0');

    -- flow fsm
    signal trig_mode_int    : std_logic := '0';
    signal vmm_conf         : std_logic := '0';
    signal vmm_id_rdy       : std_logic := '0';
    signal vmmConf_done     : std_logic := '0';
    signal newIP_rdy        : std_logic := '0';
    signal xadc_conf_rdy    : std_logic := '0';
    signal inhibit_final    : std_logic := '0';
    signal daq_on           : std_logic := '0';
    signal per_sr           : std_logic := '0';
    signal vmm_sck_all      : std_logic := '0';
    signal vmm_sdi_all      : std_logic := '0';
    signal reply_done       : std_logic := '0';
    signal reply_enable     : std_logic := '0';
    signal sel_reply        : std_logic := '0';
    signal vmm_confEnable   : std_logic := '0';
    signal xadc_busy        : std_logic := '0';
    signal flash_busy       : std_logic := '0';
    signal tki_analog       : std_logic := '0';
    signal vmm_ena_analog   : std_logic := '0';
    signal tr_raw           : std_logic := '0';
    signal cktp_enable      : std_logic := '0';
    signal cktp_primary     : std_logic := '0';
    signal tr_en            : std_logic := '0';
    signal daq_enable_i     : std_logic := '0';
    signal analog_on        : std_logic := '0';
    signal xadc_start       : std_logic := '0';
    signal newIP_start      : std_logic := '0';
    signal rst_readout      : std_logic := '0';
    signal inhibit_conf     : std_logic := '0';
    signal vmm_bitmask_8VMM : std_logic_vector(7 downto 0) := (others => '0');

    -- udp
    signal ckbcMode         : std_logic := '0';
    signal we_conf_int      : std_logic := '0';
    signal endPacket_conf_i : std_logic := '0';
    signal vmm_cs_udp       : std_logic := '0';
    signal user_data_out_i  : std_logic_vector(15 downto 0) := (others => '0');
    signal packet_len_conf  : std_logic_vector(11 downto 0) := (others => '0');
    signal latency_conf     : std_logic_vector(15 downto 0) := (others => '0');
    signal latency_extra    : std_logic_vector(15 downto 0) := (others => '0');
    signal serial_number    : std_logic_vector(31 downto 0) := (others => '0');
    signal art_timeout      : std_logic_vector(7 downto 0)  := (others => '0');
    signal ckbc_freq        : std_logic_vector(7 downto 0)  := (others => '0');
    signal cktk_max_num     : std_logic_vector(7 downto 0)  := (others => '0');
    signal cktp_max_num     : std_logic_vector(15 downto 0) := (others => '0');
    signal cktp_skew        : std_logic_vector(7 downto 0)  := (others => '0');
    signal cktp_period      : std_logic_vector(15 downto 0) := (others => '0');
    signal cktp_pulse_width : std_logic_vector(7 downto 0)  := (others => '0');
    signal ckbc_max_udp     : std_logic_vector(7 downto 0)  := (others => '0');
    signal vmm_id_xadc      : std_logic_vector(15 downto 0) := (others => '0');
    signal xadc_sample_size : std_logic_vector(10 downto 0) := (others => '0');
    signal xadc_delay       : std_logic_vector(17 downto 0) := (others => '0');

    -- fifo2udp
    signal UDPDone          : std_logic := '0';
    signal UDPBusy          : std_logic := '0';
    signal daqFIFO_wr_en_i  : std_logic := '0';
    signal end_packet_i     : std_logic := '0';
    signal daqFIFO_reset    : std_logic := '0';
    signal fifo_rst_conf    : std_logic := '0';
    signal daqFIFO_din_i    : std_logic_vector(15 downto 0) := (others => '0');
    signal packLen_i        : std_logic_vector(11 downto 0) := (others => '0');

    -- readout
    signal pf_trigVmmRo     : std_logic := '0';
    signal level_0          : std_logic := '0';
    signal wr_accept_pf     : std_logic := '0';
    signal daq_inh_health   : std_logic := '0';
    signal l0_reset_done    : std_logic := '0';
    signal analog_busy      : std_logic := '0';
    signal xadcStart_anlg   : std_logic := '0';
    signal wr_en_udp_anlg   : std_logic := '0';
    signal endPack_udp_anlg : std_logic := '0';
    signal xadc_read_tdo    : std_logic := '0';
    signal vmmWordReady_i   : std_logic := '0';
    signal vmmEventDone_i   : std_logic := '0';
    signal rd_ena_buff      : std_logic := '0';
    signal pf_newCycle      : std_logic := '0';
    signal daq_wr_en_i      : std_logic := '0';
    signal endPack_daq_i    : std_logic := '0';
    signal rst_l0_pf        : std_logic := '0';
    signal level0_idle      : std_logic := '0';
    signal level0_resetDone : std_logic := '0';
    signal trigger_pf       : std_logic := '0';
    signal tr_hold          : std_logic := '0';
    signal rst_vmm          : std_logic := '0';
    signal pf_rst_FIFO      : std_logic := '0';
    signal vmmArtReady      : std_logic := '0';
    signal request2ckbc     : std_logic := '0';
    signal rstFIFO_top      : std_logic := '0';
    signal dout_udp_analog  : std_logic_vector(15 downto 0) := (others => '0');
    signal art_in_vec       : std_logic_vector(7 downto 0)  := (others => '0');
    signal packLen_udp_anlg : std_logic_vector(11 downto 0) := (others => '0');
    signal vmmWord_i        : std_logic_vector(15 downto 0) := (others => '0');
    signal pf_vmmIdRo       : std_logic_vector(2 downto 0)  := (others => '0');
    signal linkHealth_bmsk  : std_logic_vector(7 downto 0)  := (others => '0');
    signal pf_packLen       : std_logic_vector(11 downto 0) := (others => '0');
    signal daq_data_out_i   : std_logic_vector(15 downto 0) := (others => '0');
    signal vmmArtData       : std_logic_vector(5 downto 0)  := (others => '0');
    signal art2trigger      : std_logic_vector(5 downto 0)  := (others => '0');

    -- xadc
    signal xadc_start_final : std_logic := '0';
    signal xadc_end_of_data : std_logic := '0';
    signal MuxAddr3_p_i     : std_logic := '0';
    signal MuxAddr3_n_i     : std_logic := '0';
    signal xadc_fifo_enable : std_logic := '0';
    signal xadc_packet_len  : std_logic_vector(11 downto 0) := (others => '0');
    signal xadc_fifo_bus    : std_logic_vector(15 downto 0) := (others => '0');

    -- CKTP gen
    signal CKTP_raw         : std_logic := '0';
    signal CKTP_glbl        : std_logic := '0';
    signal vmm_ena_sr       : std_logic := '0';
    signal ckbc_max_num     : std_logic_vector(7 downto 0) := (others => '0');

    -- ip
    signal myIP_set         : std_logic_vector(31 downto 0) := (others => '0');    
    signal myMAC_set        : std_logic_vector(47 downto 0) := (others => '0');    
    signal destIP_set       : std_logic_vector(31 downto 0) := (others => '0');
    signal myIP             : std_logic_vector(31 downto 0) := (others => '0');    
    signal myMAC            : std_logic_vector(47 downto 0) := (others => '0');    
    signal destIP           : std_logic_vector(31 downto 0) := (others => '0');
                     
    signal io0_i            : std_logic := '0';
    signal io0_o            : std_logic := '0';
    signal io0_t            : std_logic := '0';
    signal io1_i            : std_logic := '0';
    signal io1_o            : std_logic := '0';
    signal io1_t            : std_logic := '0'; 
    signal ss_t             : std_logic := '0';
    signal ss_i             : std_logic_vector(0 downto 0)  := (others => '0');  
    signal ss_o             : std_logic_vector(0 downto 0)  := (others => '0'); 

    -- common/aux processes
    signal lemo_trg_buf     : std_logic := '0';
    signal phy_rstn_i       : std_logic := '0';
    signal glbl_fifo_init   : std_logic := '0';
    signal glbl_fifo_init_s : std_logic := '0';
    signal glbl_fifo_init_i : std_logic := '0';
    signal glbl_rst_i       : std_logic := '0';
    signal rst_l0_buff      : std_logic := '0';
    signal rst_ro_final     : std_logic := '0';
    signal udp_rst_i        : std_logic := '0';
    signal eth_rst_i        : std_logic := '0';
    signal gpio_led_i       : std_logic_vector(5 downto 0)  := (others => '0');
    
    -- debug
    signal st_flow_dbg      : std_logic_vector(4 downto 0)  := (others => '0');
    signal state_pf         : std_logic_vector(4 downto 0)  := (others => '0');
    signal state_udp_din    : std_logic_vector(2 downto 0)  := (others => '0');
    signal dt_state_o       : std_logic_vector(3 downto 0)  := (others => '0');
    signal dt_cntr_st_o     : std_logic_vector(3 downto 0)  := (others => '0');
    signal state_buff_dbg   : std_logic_vector(1 downto 0)  := (others => '0');
    --signal cnt              : unsigned(15 downto 0)         := (others => '0');
    --signal trg_ila          : std_logic := '0';
    --signal cnt_start        : std_logic := '0';
    
    --attribute mark_debug    : string;
    --attribute mark_debug of cnt             : signal is "TRUE";
    --attribute mark_debug of trg_ila         : signal is "TRUE";
    --attribute mark_debug of tr_hold         : signal is "TRUE";
    --attribute mark_debug of CKTP_glbl       : signal is "TRUE";
    --attribute mark_debug of UDPBusy         : signal is "TRUE";
    --attribute mark_debug of UDPDone         : signal is "TRUE";
    --attribute mark_debug of vmm_cktk_i      : signal is "TRUE";
    --attribute mark_debug of vmmWordReady_i  : signal is "TRUE";
    --attribute mark_debug of vmmEventDone_i  : signal is "TRUE";
    --attribute mark_debug of state_pf        : signal is "TRUE";
    --attribute mark_debug of state_buff_dbg  : signal is "TRUE";

-- async_reg/keep/dont_touch
    attribute ASYNC_REG     : string;
    --attribute keep          : string;
    --attribute dont_touch    : string;

    attribute ASYNC_REG of glbl_fifo_init_i : signal is "TRUE";
    attribute ASYNC_REG of glbl_fifo_init_s : signal is "TRUE";
    attribute ASYNC_REG of eth_rst_i        : signal is "TRUE";
    attribute ASYNC_REG of glbl_rst_i       : signal is "TRUE";
    attribute ASYNC_REG of rst_l0_buff      : signal is "TRUE";
    attribute ASYNC_REG of rst_ro_final     : signal is "TRUE";
    
begin

-- Main MMCM
mmcm_master: clk_wiz_gen
    port map(
        -- Clock in ports
        clk_in1_p       => REFCLK_P,
        clk_in1_n       => REFCLK_N,
        -- Clock out ports  
        clk_out_40      => clk_40,
        clk_out_40_90   => clk_40_90,
        clk_out_160     => clk_160,
        clk_out_320     => clk_320,
        -- Status and control signals
        reset           => '0',
        gen_locked      => master_locked
    );


mmcm_aux: clk_wiz_aux
    port map(
        -- Clock in ports
        clk_in1         => clk_40,
        -- Clock out ports
        clk_out_50      => clk_50,
        clk_out_200     => clk_200,
        clk_out_500     => clk_500,
        -- Status and control signals
        reset           => '0',
        aux_locked      => aux_locked
    );

-- startup logic
startup_inst: startup
    port map(  
        clk_40          => clk_40,
        mmcm_locked     => all_pll_locked,
        tr_hold         => tr_hold,
        gpio_led        => gpio_led_i,
        phy_rstn_out    => phy_rstn_i,
        rst_init_out    => glbl_fifo_init
    );

vmm_intf_block_inst: vmm_intf_block
    generic map(
            is_mmfe8   => is_mmfe8,
            is_level0  => is_level0)
    port map(
        ------------------------------------
        ----- FPGA Logic Interface ---------
        clk_40              => clk_40,
        clk_160             => clk_160,
        clk_320             => clk_320,
        mmcm_locked         => master_locked,
        cktp                => CKTP_glbl,
        ckdt                => CKDT_glbl,
        ckbc                => CKBC_glbl,
        ckbc_mode           => ckbcMode,
        trg_en              => tr_en,
        req_ckbc            => request2ckbc,
        ckbc_max_num        => ckbc_max_num,
        ckbc_freq           => ckbc_freq(5 downto 0),
        vmm_tki_in          => vmm_tki_i,
        vmm_tko_out         => vmm_tko_i,
        ext_trg_out         => ext_trg_i,
        ext_rst_out         => ext_rst_i,
        ext_inh_out         => ext_inh_i,
        ext_busy_in         => tr_hold,
        vmm_ckdt_enable     => vmm_ckdt_enable,
        vmm_ckbc_enable     => vmm_ckbc_enable,
        vmm_cktp_enable     => '1',
        vmm_ena_in          => vmm_ena_i,
        vmm_cs_in           => vmm_cs_i,
        vmm_sdi_in          => vmm_sdi_i,
        vmm_sck_in          => vmm_sck_i,
        vmm_cktk_in         => vmm_cktk_i,
        vmm_data0_out       => vmm_data0_i,
        vmm_data1_out       => vmm_data1_i,
        ------------------------------------
        ------------ VMM Interface ---------
        vmm_ena_out         => ena_buf,
        vmm_cktk_out        => cktk_buf,
        vmm_ckbc_out        => ckbc_buf,
        vmm_ckdt_out        => ckdt_buf,
        vmm_cktp_out        => cktp_buf,
        vmm_ckart_out       => ckart_buf,
        vmm_sdi_out         => sdi_buf,
        vmm_sck_out         => sck_buf,
        vmm_cs_out          => cs_buf,
        vmm_tki_out         => tki_buf,
        vmm_tki_mmfe8_out   => tki_mmfe8_buf,
        vmm_tko_in          => tko_buf,
        vmm_data0_in        => data0_buf,
        vmm_data1_in        => data1_buf,
        ------------------------------------
        ------------ Ext Interface ---------
        ext_rst_in          => '0',
        ext_trg_in          => lemo_trg_buf,
        ext_inh_in          => '0',
        ext_busy_out        => open
    );

flow_fsm_inst: flow_fsm
    generic map(
            is_level0   => is_level0,
            is_vmm3a    => is_vmm3a,
            use_extSR   => use_extSR,
            use_inhib   => use_inhib)
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_40          => clk_40,
        rst             => glbl_rst_i,
        daq_inhib       => inhibit_final,
        trg_mode        => trig_mode_int,
        analog_mode     => is_analog,
        ext_sr          => '0',
        per_sr          => per_sr,
        mmcm_locked     => all_pll_locked,
        st_flow_dbg     => st_flow_dbg,
        ------------------------------------
        ----- UDP din/dout Interface -------
        vmm_bitmask     => vmm_bitmask_8VMM,
        vmm_conf        => vmm_conf,
        vmm_confRdy     => vmm_id_rdy,
        vmm_confEnd     => vmmConf_done,
        ip_conf         => newIP_rdy,
        xadc_conf       => xadc_conf_rdy,
        daq_on          => daq_on,
        cs_udp          => vmm_cs_udp,
        sck_udp         => vmm_sck_all,
        sdi_udp         => vmm_sdi_all,
        udp_done        => UDPDone,
        reply_done      => reply_done,
        reply_enable    => reply_enable,
        sel_reply       => sel_reply,
        vmm_confEnable  => vmm_confEnable,
        ------------------------------------
        -------- Submodules Interface ------
        xadc_busy       => xadc_busy,
        ip_busy         => flash_busy,
        tki_analog      => tki_analog,
        vmm_ena_analog  => vmm_ena_analog,
        trg_ext         => tr_raw,
        cktp_enable     => cktp_enable,
        cktp_primary    => cktp_primary,
        trg_enable      => tr_en,
        daq_enable      => daq_enable_i,
        analog_enable   => analog_on,
        xadc_start      => xadc_start,
        ip_start        => newIP_start,
        rst_readout     => rst_readout,
        inhib_conf      => inhibit_conf,
        ------------------------------------
        ------------ VMM Interface ---------
        ckbc_enable     => vmm_ckbc_enable,
        vmm_ena         => vmm_ena_i,
        vmm_cs          => vmm_cs_i,
        vmm_sdi         => vmm_sdi_i,
        vmm_sck         => vmm_sck_i,
        vmm_tki         => vmm_tki_i
     );

-- Ethernet wrapper
vmmFE_eth_wrapper_inst: vmmFE_eth_wrapper
    port map(
        ---------------------------------
        ------ General Interface --------
        clk_200         => clk_200,
        clk_40          => clk_40,
        fifo_init       => glbl_fifo_init_s,
        rst_eth         => eth_rst_i,
        FPGA_IP         => myIP,
        FPGA_MAC        => myMAC,
        userclk2        => clk_eth,
        TXRXmmcm_locked => eth_locked,
        ---------------------------------
        ---- UDP/ICMP Block Interface ---
        udp_tx_start    => udp_tx_start,
        udp_txi         => udp_tx,
        udp_rxo         => udp_rx,
        udp_tx_dout_rdy => udp_tx_dout_rdy,
        ---------------------------------
        ----- Transceiver Interface -----
        gtrefclk_p      => GTREFCLK_P, 
        gtrefclk_n      => GTREFCLK_N, 
        rxp             => RXP,
        rxn             => RXN,
        txp             => TXP,
        txn             => TXN
    );

udp_data_in_handler_inst: udp_data_in_handler
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_125             => clk_eth,
        clk_40              => clk_40,
        inhibit_conf        => inhibit_conf,
        rst                 => glbl_rst_i,
        rst_fifo_init       => glbl_fifo_init_s,
        state_o             => state_udp_din,
        valid_o             => open,
        ------------------------------------
        -------- FPGA Config Interface -----
        latency             => latency_conf,
        latency_extra       => latency_extra,
        serial_number       => serial_number,
        artTimeout          => art_timeout,
        daq_on              => daq_on,
        analog_on           => open, --is_analog
        ext_trigger         => trig_mode_int,
        ckbcMode            => ckbcMode,
        fpga_rst            => udp_rst_i,
        ------------------------------------
        -------- UDP Interface -------------
        udp_rx              => udp_rx,
        ------------------------------------
        ---------- AXI4SPI Interface -------
        flash_busy          => flash_busy,
        newIP_rdy           => newIP_rdy,
        myIP_set            => myIP_set,
        myMAC_set           => myMAC_set,
        destIP_set          => destIP_set,
        ------------------------------------
        -------- CKTP/CKBC Interface -------
        ckbc_freq           => ckbc_freq,
        cktk_max_num        => cktk_max_num,
        cktp_max_num        => cktp_max_num,
        cktp_skew           => cktp_skew,
        cktp_period         => cktp_period,
        cktp_width          => cktp_pulse_width,
        ckbc_max_num        => ckbc_max_udp,
        ------------------------------------
        ------ VMM Config Interface --------
        vmm_bitmask         => vmm_bitmask_8VMM,
        vmmConf_came        => vmm_conf,
        vmmConf_rdy         => vmm_id_rdy,
        vmmConf_done        => vmmConf_done,
        vmm_sck             => vmm_sck_all,
        vmm_cs              => vmm_cs_udp,
        vmm_cfg_bit         => vmm_sdi_all,
        top_rdy             => vmm_confEnable,
        ------------------------------------
        ---------- XADC Interface ----------
        xadc_busy           => xadc_busy,
        xadc_rdy            => xadc_conf_rdy,
        vmm_id_xadc         => vmm_id_xadc,
        xadc_sample_size    => xadc_sample_size,
        xadc_delay          => xadc_delay
    );

udp_reply_handler_inst: udp_reply_handler
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk             => clk_160,
        enable          => reply_enable,
        serial_number   => serial_number,
        UDP_done        => UDPDone,
        reply_done      => reply_done,
        ------------------------------------
        ---- FIFO Data Select Interface ----
        rst_fifo        => fifo_rst_conf,
        wr_en_conf      => we_conf_int,
        dout_conf       => user_data_out_i,
        packet_len_conf => packet_len_conf,
        end_conf        => endPacket_conf_i
    );

FIFO2UDP_inst: FIFO2UDP
    port map( 
        clk_125                 => clk_eth,
        clk_160                 => clk_160,
        destinationIP           => destIP,
        daq_data_in             => daqFIFO_din_i,
        fifo_data_out           => open,
        udp_txi                 => udp_tx,
        udp_tx_start            => udp_tx_start,
        UDPDone                 => UDPDone,
        UDPBusy                 => UDPBusy,
        xadc_busy               => xadc_busy,
        analog_mode             => analog_on,
        udp_tx_data_out_ready   => udp_tx_dout_rdy,
        wr_en                   => daqFIFO_wr_en_i,
        end_packet              => end_packet_i,
        global_reset            => glbl_rst_i,
        packet_length_in        => packLen_i,
        reset_DAQ_FIFO          => daqFIFO_reset,
        confReply_packet        => reply_enable
    );    

vmm_readout_wrapper_inst: vmm_readout_wrapper
    generic map(is_vmm3a => is_vmm3a, is_mmfe8 => is_mmfe8, is_level0 => is_level0, is_analog => is_analog)
    port map(
        ------------------------------------
        --- Continuous Readout Interface ---
        clkTkProc       => clk_40,
        clkDtProc       => clk_50,
        clk             => clk_160,
        --
        daq_enable      => daq_enable_i,
        trigger_pulse   => pf_trigVmmRo,
        cktk_max        => cktk_max_num,
        --
        dt_state_o      => dt_state_o,
        dt_cntr_st_o    => dt_cntr_st_o,
        ------------------------------------
        ---- Level-0 Readout Interface -----
        clk_ckdt        => clk_160,
        clk_ckbc        => clk_40, --  put shifted CKBC here (clk_40_135)
        rst_buff        => rst_l0_buff,
        rst_intf_proc   => rst_l0_pf,
        --
        level_0         => level_0,
        wr_accept       => wr_accept_pf,
        --
        vmm_conf        => vmm_confEnable,
        daq_on_inhib    => daq_inh_health, -- synced to flow_fsm's clock
        level0_idle     => level0_idle,
        l0_sreg_out     => open,
        --
        state_buff_dbg  => state_buff_dbg,
        fifo_full_dbg   => open,
        commas_dbg      => open,
        reset_done      => l0_reset_done,    
        ------------------------------------
        -- 2phaseAnalog Readout Interface --
        fifo_init       => glbl_fifo_init_s,
        analog_on       => analog_on,
        analog_busy     => analog_busy,
        trint           => pf_trigVmmRo,
        xadc_busy       => xadc_busy,
        xadc_start      => xadcStart_anlg,
        xadc_read_tdo   => xadc_read_tdo,
        UDPDone         => UDPDone,
        wr_en_udp       => wr_en_udp_anlg,
        packLen_udp     => packLen_udp_anlg,
        endPacket_udp   => endPack_udp_anlg,
        dout_udp        => dout_udp_analog,
        vmm_ena         => vmm_ena_analog,
        vmm_tki         => tki_analog,
        ------------------------------------
        ---- Packet Formation Interface ----
        vmmWordReady    => vmmWordReady_i,
        vmmWord         => vmmWord_i,
        rd_ena_buff     => rd_ena_buff,
        vmmEventDone    => vmmEventDone_i,
        vmmId           => pf_vmmIdRo,
        linkHealth_bmsk => linkHealth_bmsk,
        ------------------------------------
        ---------- VMM3 Interface ----------
        vmm_data0_vec   => vmm_data0_i,
        vmm_data1_vec   => vmm_data1_i,
        vmm_ckdt_glbl   => CKDT_glbl,
        vmm_ckdt_enable => vmm_ckdt_enable,
        vmm_cktk_vec    => vmm_cktk_i
    );


packet_formation_inst: packet_formation
    generic map(is_mmfe8        => is_mmfe8,
                is_level0       => is_level0,
                artEnabled      => artEnabled)
    port map(
        clk_art         => clk_160,
        bc_clk          => clk_40,
        fifo_init       => '0',
        reset           => rst_ro_final,
        vmm_ena         => vmm_ena_sr,
        ckbc_enable     => vmm_ckbc_enable,
        CKBCstrobe      => ckbcMode,
        
        newCycle        => pf_newCycle,
        
        trigVmmRo       => pf_trigVmmRo,
        vmmId           => pf_vmmIdRo,
        vmmWord         => vmmWord_i,
        vmmWordReady    => vmmWordReady_i,
        vmmEventDone    => vmmEventDone_i,
        event_cnt_o     => open,
        
        UDPBusy         => UDPBusy,
        UDPDone         => UDPDone,
        inhibit         => inhibit_final,
        pfBusy          => open,
        glBCID          => (others => '0'),
        
        packLen         => pf_packLen,
        dataout         => daq_data_out_i,
        wrenable        => daq_wr_en_i,
        end_packet      => endPack_daq_i,
        wr_accept       => wr_accept_pf,
        
        rd_ena_buff     => rd_ena_buff,
        rst_l0          => rst_l0_pf,
        level0_idle     => level0_idle,
        trig_l0         => '0',
        latency_done    => trigger_pf,
        level0_resetDone=> l0_reset_done,
        
        tr_hold         => tr_hold,
        
        rst_vmm         => rst_vmm,
        linkHealth_bmsk => linkHealth_bmsk,
        rst_FIFO        => pf_rst_FIFO,

        latency         => latency_conf,
        dbg_st_o        => state_pf,
        tr_raw          => tr_raw,
        art2trigger     => art2trigger,
        
        vmmArtData      => vmmArtData,
        vmmArtReady     => vmmArtReady
    );

trigger_inst: trigger
    generic map(is_level0 => is_level0)
    port map(
        ckbc                => clk_40,
        clk_art             => clk_160,
        clk_art2            => clk_320,
        rst_trig            => rst_ro_final,
        state_trg           => open,
        
        ckbcMode            => ckbcMode,
        cktp_enable         => cktp_enable,
        cktp_pulse_width    => cktp_pulse_width(4 downto 0),
        CKTP_raw            => CKTP_raw,
        request2ckbc        => request2ckbc,
        
        tr_en               => tr_en,
        tr_hold             => tr_hold,
        tr_mode             => trig_mode_int,
        tr_ext              => ext_trg_i,
        level_0             => level_0,
        busy_l0             => open,
        trigger_pf          => trigger_pf,

        ckbc_max_conf       => ckbc_max_udp,
        ckbc_max_num        => ckbc_max_num,
        latency_extra       => latency_extra,
        latency             => latency_conf,

        tr_out              => pf_newCycle,
        tr_raw              => tr_raw
    );

art_inst_gen: if is_mmfe8 = '0' and artEnabled = '1' generate
artReadout_inst: artReadout
    generic map(is_mmfe8    => is_mmfe8,
                artEnabled  => artEnabled,
                slowTrigger => slowTrigger)
    port map(
        clk_art         => clk_160,
        trigger160      => pf_newCycle,
        tr_hold         => tr_hold,
        artData         => art_in_vec,
        art2trigger     => art2trigger,
        vmmArtData160   => vmmArtData,
        vmmArtReady     => vmmArtReady,
        artTimeout      => art_timeout,
        request2ckbc    => request2ckbc
    );
end generate art_inst_gen;

select_data_inst:  select_data
    port map(
        configuring             => sel_reply,
        analog                  => analog_on,
        xadc                    => xadc_busy,
        data_acq                => daq_enable_i,
        we_analog               => wr_en_udp_anlg,
        we_data                 => daq_wr_en_i,
        we_conf                 => we_conf_int,
        we_xadc                 => xadc_fifo_enable,
        analog_data_in          => dout_udp_analog,
        daq_data_in             => daq_data_out_i,
        conf_data_in            => user_data_out_i,
        xadc_data_in            => xadc_fifo_bus,
        analog_packet_length    => packLen_udp_anlg,
        data_packet_length      => pf_packLen,
        xadc_packet_length      => xadc_packet_len,
        conf_packet_length      => packet_len_conf,
        end_packet_analog       => endPack_udp_anlg,
        end_packet_conf         => endPacket_conf_i,
        end_packet_daq          => endPack_daq_i,
        end_packet_xadc         => xadc_end_of_data,
        fifo_rst_daq            => pf_rst_FIFO,
        fifo_rst_xadc           => '0',
        fifo_rst_conf           => fifo_rst_conf,
        rstFIFO_top             => glbl_fifo_init_s,

        data_out                => daqFIFO_din_i,
        packet_length           => packLen_i,
        we                      => daqFIFO_wr_en_i,
        end_packet              => end_packet_i,
        fifo_rst                => daqFIFO_reset
    );

xadc_inst: xadcModule
    port map(
        clk                 => clk_160,
        rst                 => glbl_rst_i,
        
        VP_0                => VP0,
        VN_0                => VN0,
        Vaux0_v_n           => VAUX0_V_N,
        Vaux0_v_p           => VAUX0_V_P,
        Vaux1_v_n           => VAUX1_V_N,
        Vaux1_v_p           => VAUX1_V_P,
        Vaux2_v_n           => VAUX2_V_N,
        Vaux2_v_p           => VAUX2_V_P,
        Vaux3_v_n           => VAUX3_V_N,
        Vaux3_v_p           => VAUX3_V_P,
        Vaux8_v_n           => VAUX8_V_N,
        Vaux8_v_p           => VAUX8_V_P,
        Vaux9_v_n           => VAUX9_V_N,
        Vaux9_v_p           => VAUX9_V_P,
        Vaux10_v_n          => VAUX10_V_N,
        Vaux10_v_p          => VAUX10_V_P,
        Vaux11_v_n          => VAUX11_V_N,
        Vaux11_v_p          => VAUX11_V_P,
        data_in_rdy         => xadc_start_final,
        read_tdo            => xadc_read_tdo,
        vmm_id              => vmm_id_xadc,
        sample_size         => xadc_sample_size,
        delay_in            => xadc_delay,
        UDPDone             => UDPDone,
    
        MuxAddr0            => MUXADDR(0),
        MuxAddr1            => MUXADDR(1),
        MuxAddr2            => MUXADDR(2),
        MuxAddr3_p          => MuxAddr3_p_i,
        MuxAddr3_n          => MuxAddr3_n_i,
        end_of_data         => xadc_end_of_data,
        fifo_bus            => xadc_fifo_bus,
        data_fifo_enable    => xadc_fifo_enable,
        packet_len          => xadc_packet_len,
        xadc_busy           => xadc_busy          -- synced to 125 Mhz
    );

axi4_spi_generate : if (is_mmfe8 = '1' or is_old_mdt = '1') generate
begin
axi4_spi_inst: AXI4_SPI  
    port map(
        clk_125         => clk_eth,
        clk_50          => clk_50,
        clk_40          => clk_40,
        
        myIP            => myIP,             -- synced to 125 Mhz
        myMAC           => myMAC,            -- synced to 125 Mhz
        destIP          => destIP,           -- synced to 125 Mhz

        default_IP      => default_IP,
        default_MAC     => default_MAC,
        default_destIP  => default_destIP,
        
        myIP_set        => myIP_set,         -- synced internally to 50 Mhz
        myMAC_set       => myMAC_set,        -- synced internally to 50 Mhz
        destIP_set      => destIP_set,       -- synced internally to 50 Mhz

        newip_start     => newIP_start,      -- synced internally to 50 Mhz
        flash_busy      => flash_busy,       -- synced to 125 Mhz

        io0_i           => io0_i,
        io0_o           => io0_o,
        io0_t           => io0_t,
        io1_i           => io1_i,
        io1_o           => io1_o,
        io1_t           => io1_t,
        ss_i            => ss_i,
        ss_o            => ss_o,
        ss_t            => ss_t
    );

QSPI_IO0_0: IOBUF    
   port map (
      O  => io0_i,
      IO => IO0_IO,
      I  => io0_o,
      T  => io0_t
   );
           
QSPI_IO1_0: IOBUF    
   port map (
      O  => io1_i,
      IO => IO1_IO,
      I  => io1_o,
      T  => io1_t
   );
    
QSPI_SS_0: IOBUF     
   port map (
      O  => ss_i(0),
      IO => SS_IO,
      I  => ss_o(0),
      T  => ss_t
   );

end generate;

i2c_wrapper_generate : if (is_mmfe8 = '0' and is_old_mdt = '0') generate
begin
i2c_wrapper_inst: i2c_wrapper
    port map (
        CLK_50_MHz          => clk_50,
        CLK_40_MHz          => clk_40,
        CLK_125_MHz         => clk_eth,
        new_IP_ready        => newIP_start,
        module_busy         => flash_busy,
        SDA                 => SDA,
        SCL                 => SCL,
        destIP              => destIP_set,
        srcIP               => myIP_set,
        mac                 => myMAC_set,
        destIP_returned     => destIP,
        srcIP_returned      => myIP,
        mac_returned        => myMAC,
        destIP_default_val  => default_destIP,
        srcIP_default_val   => default_IP,
        mac_default_val     => default_MAC
    );
end generate;

clk_gen_wrapper_inst: clk_gen_wrapper
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_500             => clk_500,
        clk_160             => clk_160,
        clk_40              => clk_40,
        rst                 => glbl_rst_i,
        mmcm_locked         => all_pll_locked,
        CKTP_raw            => CKTP_raw,
        ------------------------------------
        ----- Configuration Interface ------
        cktp_enable         => cktp_enable,
        cktp_primary        => cktp_primary, -- from flow_fsm
        readout_mode        => ckbcMode,
        cktp_pulse_width    => cktp_pulse_width(4 downto 0),
        cktp_max_num        => cktp_max_num,
        cktp_period         => cktp_period,
        sr_offset           => sr_offset,
        cktp_skew           => cktp_skew(4 downto 0),
        ckbc_freq           => ckbc_freq(5 downto 0),
        ckbc_max_num        => ckbc_max_num,
        ------------------------------------
        ---------- VMM Interface -----------
        VMM_ENA             => vmm_ena_sr,
        CKTP                => CKTP_glbl
    );

    -- i/o buffers
RSTN_OBUF:      OBUF    port map (O => PHY_RSTN,        I => phy_rstn_i);
PHY_INT_OBUF:   OBUF    port map (O => PHY_INT,         I => '1');
LEMO_TRG_IBUF:  IBUF    port map (O => lemo_trg_buf,    I => LEMO_TRG);
TRG_OUT_P_OBUF: OBUF    port map (O => TRG_OUT_P,       I => '0');
TRG_OUT_N_OBUF: OBUF    port map (O => TRG_OUT_N,       I => '1');
ART_1_IBUFDS:   IBUFDS  generic map (DIFF_TERM   => TRUE, IBUF_LOW_PWR => FALSE) port map (O => art_in, I => ART_1_P, IB => ART_1_N);
TKI_OBUFDS:     OBUFDS  generic map (IOSTANDARD  => "DEFAULT" , SLEW => "FAST") port map (O => TKI_P,  OB => TKI_N,  I => tki_buf);
TKO_IBUFDS:     IBUFDS  generic map (DIFF_TERM   => TRUE, IBUF_LOW_PWR => FALSE) port map (O => tko_buf, I => TKO_P, IB => TKO_N);
TKIMMFE8_OBUFDS:OBUFDS  generic map (IOSTANDARD  => "DEFAULT" , SLEW => "FAST") port map (O => TKI_MMFE8_P,  OB => TKI_MMFE8_N,  I => tki_mmfe8_buf);
XADCMUX_OBUFDS: OBUFDS  port map  (O => MUXADDR_P, OB => MUXADDR_N, I => MuxAddr3_p_i);

gen_buffers_led: for I in 0 to 5 generate LED_OBUF: OBUF  port map (O => GPIO_LED(I), I => gpio_led_i(I)); end generate gen_buffers_led;

gen_buffers_vmm: for I in 0 to 7 generate 
DATA0_IBUFDS:   IBUFDS generic map (DIFF_TERM => TRUE, IBUF_LOW_PWR => FALSE) port map (O => data0_buf(I), I => DATA0_P(I), IB => DATA0_N(I));
DATA1_IBUFDS:   IBUFDS generic map (DIFF_TERM => TRUE, IBUF_LOW_PWR => FALSE) port map (O => data1_buf(I), I => DATA1_P(I), IB => DATA1_N(I));
SDO_IBUF:       IBUF   port map (O => sdo_buf(I),   I => SDO(I));
SDI_OBUF:       OBUF   port map (O => SDI(I),       I => sdi_buf(I));
SCK_OBUF:       OBUF   port map (O => SCK(I),       I => sck_buf(I));
CS_OBUF:        OBUF   port map (O => CS(I),        I => cs_buf(I));
ENA_OBUFDS:     OBUFDS generic map (IOSTANDARD => "DEFAULT" , SLEW => "FAST") port map (O => ENA_P(I),  OB => ENA_N(I),  I => ena_buf(I));
CKTK_OBUFDS:    OBUFDS generic map (IOSTANDARD => "DEFAULT" , SLEW => "FAST") port map (O => CKTK_P(I), OB => CKTK_N(I), I => cktk_buf(I));
CKBC_OBUFDS:    OBUFDS generic map (IOSTANDARD => "DEFAULT" , SLEW => "FAST") port map (O => CKBC_P(I), OB => CKBC_N(I), I => ckbc_buf(I));
CKDT_OBUFDS:    OBUFDS generic map (IOSTANDARD => "DEFAULT" , SLEW => "FAST") port map (O => CKDT_P(I), OB => CKDT_N(I), I => ckdt_buf(I));
CKTP_OBUFDS:    OBUFDS generic map (IOSTANDARD => "DEFAULT" , SLEW => "FAST") port map (O => CKTP_P(I), OB => CKTP_N(I), I => cktp_buf(I));
end generate gen_buffers_vmm;

gen_buffers_art: for I in 0 to 8 generate 
CKART_OBUFDS: OBUFDS generic map (IOSTANDARD => "DEFAULT" , SLEW => "FAST") port map (O => CKART_P(I), OB => CKART_N(I), I => ckart_buf(I)); 
end generate gen_buffers_art;

    -- async assertion
    all_pll_locked      <= master_locked and eth_locked and aux_locked;
    art_in_vec(0)       <= art_in;
    MO                  <= 'Z';
    inhibit_final       <= (ext_inh_i and use_inhib) or daq_inh_health;
    xadc_start_final    <= xadc_start or xadcStart_anlg;
    per_sr              <= not vmm_ena_sr;
    CKBC_glbl           <= clk_40_90;

    -------------------------------------------------------------------
    -- Auxilliary Processes
    -------------------------------------------------------------------
rstFanout_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        glbl_fifo_init_i    <= glbl_fifo_init;
        glbl_fifo_init_s    <= glbl_fifo_init_i;
        eth_rst_i           <= udp_rst_i;
        glbl_rst_i          <= udp_rst_i or glbl_fifo_init_i;
        rst_l0_buff         <= udp_rst_i or glbl_fifo_init_i or rst_l0_pf;
        rst_ro_final        <= udp_rst_i or glbl_fifo_init_i or rst_readout;
    end if;
end process;

-- dbg/ila

-- monitor process
--monProc: process(clk_160)
--begin
--    if(rising_edge(clk_160))then
--        if(cnt_start = '1')then
--            if(tr_hold = '1')then
--                cnt_start   <= '1';
--                trg_ila     <= '0';
--            else
--                cnt_start   <= '0';
--                trg_ila     <= '1';
--            end if;
--        else
--            trg_ila <= '0';

--            if(tr_hold = '1')then
--                cnt_start <= '1';
--            else
--                cnt_start <= '0';
--            end if;
--        end if;

--        if(cnt_start = '1')then
--            cnt <= cnt + 1;
--        else
--            cnt <= (others => '0');
--        end if;

--    end if;
--end process;

---- ila
--ila_top_inst: ila_overview
--    port map(
--        clk                    => clk_160,
--        probe0(15 downto 0)    => std_logic_vector(cnt),
--        probe0(16)             => trg_ila,
--        probe0(17)             => tr_hold,
--        probe0(18)             => CKTP_glbl,
--        probe0(19)             => UDPBusy,
--        probe0(20)             => vmm_cktk_i(0),
--        probe0(21)             => vmmWordReady_i,
--        probe0(22)             => vmmEventDone_i,
--        probe0(27 downto 23)   => state_pf,
--        probe0(29 downto 28)   => state_buff_dbg,
--        probe0(30)             => UDPDone,
--        probe0(63 downto 31)   => (others => '0')
--    );

end RTL;
