--------------------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
--           Panagiotis Gkountoumis (panagiotis.gkountoumis@cern.ch)
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis, Panagiotis Gkountoumis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 23.11.2017 17:20:12
-- Design Name: vmm-front-end Ethernet Wrapper
-- Module Name: vmmFE_eth_wrapper - RTL
-- Project Name: MMFE8 - NTUA
-- Target Devices: Artix7 xc7a200t-2fbg484 & xc7a200t-3fbg484 
-- Tool Versions: Vivado 2018.2
-- Description: VMM Front-End Ethernet Interfacing Wrapper
--
-- Dependencies: 
-- 
-- Changelog:
--
--------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;

entity vmmFE_eth_wrapper is
    port(
        ---------------------------------
        ------ General Interface --------
        clk_200         : in  std_logic;    -- Independent clock (from BUFG)
        clk_40          : in  std_logic;    -- For CDCC
        fifo_init       : in  std_logic;    -- Initialize the ICMP FIFO
        rst_eth         : in  std_logic;    -- Reset the ethernet cores
        FPGA_IP         : in  std_logic_vector(31 downto 0);
        FPGA_MAC        : in  std_logic_vector(47 downto 0);
        userclk2        : out std_logic;    -- 125 Mhz for user logic
        TXRXmmcm_locked : out std_logic;    -- Transceiver MMCM lock signal
        ---------------------------------
        ---- UDP/ICMP Block Interface ---
        udp_tx_start    : in  std_logic;
        udp_txi         : in  udp_tx_type;
        udp_rxo         : out udp_rx_type;
        udp_tx_dout_rdy : out std_logic;
        ---------------------------------
        ----- Transceiver Interface -----
        gtrefclk_p      : in  std_logic;    -- Differential +ve of reference clock for transceiver: 125MHz, very high quality
        gtrefclk_n      : in  std_logic;    -- Differential -ve of reference clock for transceiver: 125MHz, very high quality
        rxp             : in  std_logic;    -- Differential +ve for serial reception from PMD to PMA.
        rxn             : in  std_logic;    -- Differential -ve for serial reception from PMD to PMA.
        txp             : out std_logic;    -- Differential +ve of serial transmission from PMA to PMD.
        txn             : out std_logic     -- Differential -ve of serial transmission from PMA to PMD.
    );
end vmmFE_eth_wrapper;

architecture RTL of vmmFE_eth_wrapper is

    component CDCC is
    generic(
        NUMBER_OF_BITS : integer := 8); -- number of signals to be synced
    port(
        clk_src     : in  std_logic;                                        -- input clk (source clock)
        clk_dst     : in  std_logic;                                        -- input clk (dest clock)
        data_in     : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0);    -- data to be synced
        data_out_s  : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0)     -- synced data to clk_dst
        );
    end component;

    component gig_ethernet_pcs_pma_0
        port(
            -- Transceiver Interface
            ---------------------
            gtrefclk_p               : in  std_logic;                          
            gtrefclk_n               : in  std_logic;                         
            
            gtrefclk_out             : out std_logic;                   -- Very high quality clock for GT transceiver.
            gtrefclk_bufg_out        : out std_logic;                           
                
            txp                      : out std_logic;                   -- Differential +ve of serial transmission from PMA to PMD.
            txn                      : out std_logic;                   -- Differential -ve of serial transmission from PMA to PMD.
            rxp                      : in  std_logic;                   -- Differential +ve for serial reception from PMD to PMA.
            rxn                      : in  std_logic;                   -- Differential -ve for serial reception from PMD to PMA.
            resetdone                : out std_logic;                   -- The GT transceiver has completed its reset cycle
            userclk_out              : out std_logic;                           
            userclk2_out             : out std_logic;                           
            rxuserclk_out            : out std_logic;                         
            rxuserclk2_out           : out std_logic;                         
            pma_reset_out            : out std_logic;                   -- transceiver PMA reset signal
            mmcm_locked_out          : out std_logic;                   -- MMCM Locked
            independent_clock_bufg   : in  std_logic;                   
            
            -- GMII Interface
            -----------------
            sgmii_clk_r             : out std_logic;              
            sgmii_clk_f             : out std_logic;              
            sgmii_clk_en            : out std_logic;                    -- Clock enable for client MAC
            gmii_txd                : in std_logic_vector(7 downto 0);  -- Transmit data from client MAC.
            gmii_tx_en              : in std_logic;                     -- Transmit control signal from client MAC.
            gmii_tx_er              : in std_logic;                     -- Transmit control signal from client MAC.
            gmii_rxd                : out std_logic_vector(7 downto 0); -- Received Data to client MAC.
            gmii_rx_dv              : out std_logic;                    -- Received control signal to client MAC.
            gmii_rx_er              : out std_logic;                    -- Received control signal to client MAC.
            gmii_isolate            : out std_logic;                    -- Tristate control to electrically isolate GMII.
            
            -- Management: Alternative to MDIO Interface
            --------------------------------------------
            
            configuration_vector    : in std_logic_vector(4 downto 0);  -- Alternative to MDIO interface.
            
            an_interrupt            : out std_logic;                    -- Interrupt to processor to signal that Auto-Negotiation has completed
            an_adv_config_vector    : in std_logic_vector(15 downto 0); -- Alternate interface to program REG4 (AN ADV)
            an_restart_config       : in std_logic;                     -- Alternate signal to modify AN restart bit in REG0
            
            -- Speed Control
            ----------------
            speed_is_10_100         : in std_logic;                             -- Core should operate at either 10Mbps or 100Mbps speeds
            speed_is_100            : in std_logic;                             -- Core should operate at 100Mbps speed
            
            -- General IO's
            ---------------
            status_vector           : out std_logic_vector(15 downto 0);        -- Core status.
            reset                   : in std_logic;                             -- Asynchronous reset for entire core.
            
            signal_detect           : in std_logic;                             -- Input from PMD to indicate presence of optical input.
            gt0_pll0outclk_out      : out std_logic;
            gt0_pll0outrefclk_out   : out std_logic;
            gt0_pll1outclk_out      : out std_logic;
            gt0_pll1outrefclk_out   : out std_logic;
            gt0_pll0refclklost_out  : out std_logic;
            gt0_pll0lock_out        : out std_logic
        );
        end component;

    component UDP_ICMP_Complete_nomac
        Port (
            -- UDP TX signals
            udp_tx_start            : in std_logic;                             -- indicates req to tx UDP
            udp_txi                 : in udp_tx_type;                           -- UDP tx cxns
            udp_tx_result           : out std_logic_vector (1 downto 0);        -- tx status (changes during transmission)
            udp_tx_data_out_ready   : out std_logic;                            -- indicates udp_tx is ready to take data
            -- UDP RX signals
            udp_rx_start            : out std_logic;                            -- indicates receipt of udp header
            udp_rxo                 : out udp_rx_type;
            -- ICMP RX signals
            icmp_rx_start           : out std_logic;
            icmp_rxo                : out icmp_rx_type;
            -- IP RX signals
            ip_rx_hdr               : out ipv4_rx_header_type;
            -- system signals
            rx_clk                  : in  std_logic;
            tx_clk                  : in  std_logic;
            reset                   : in  std_logic;
            fifo_init               : in  std_logic;
            our_ip_address          : in std_logic_VECTOR (31 downto 0);
            our_mac_address         : in std_logic_vector (47 downto 0);
            control                 : in udp_control_type;
            -- status signals
            arp_pkt_count           : out std_logic_vector(7 downto 0);         -- count of arp pkts received
            ip_pkt_count            : out std_logic_vector(7 downto 0);         -- number of IP pkts received for us
            -- MAC Transmitter
            mac_tx_tdata            : out  std_logic_vector(7 downto 0);        -- data byte to tx
            mac_tx_tvalid           : out  std_logic;                           -- tdata is valid
            mac_tx_tready           : in   std_logic;                             -- mac is ready to accept data
            mac_tx_tfirst           : out  std_logic;                           -- indicates first byte of frame
            mac_tx_tlast            : out  std_logic;                           -- indicates last byte of frame
            -- MAC Receiver
            mac_rx_tdata            : in std_logic_vector(7 downto 0);          -- data byte received
            mac_rx_tvalid           : in std_logic;                             -- indicates tdata is valid
            mac_rx_tready           : out  std_logic;                           -- tells mac that we are ready to take data
            mac_rx_tlast            : in std_logic);                            -- indicates last byte of the trame
    end component;

    component temac_10_100_1000_fifo_block
        port(
            gtx_clk                    : in  std_logic;
            -- asynchronous reset
            glbl_rstn                  : in  std_logic;
            rx_axi_rstn                : in  std_logic;
            tx_axi_rstn                : in  std_logic;
            -- Receiver Statistics Interface
            -----------------------------------------
            rx_reset                   : out std_logic;
            rx_statistics_vector       : out std_logic_vector(27 downto 0);
            rx_statistics_valid        : out std_logic;
            -- Receiver (AXI-S) Interface
            ------------------------------------------
            rx_fifo_clock              : in  std_logic;
            rx_fifo_resetn             : in  std_logic;
            rx_axis_fifo_tdata         : out std_logic_vector(7 downto 0);
            rx_axis_fifo_tvalid        : out std_logic;
            rx_axis_fifo_tready        : in  std_logic;
            rx_axis_fifo_tlast         : out std_logic;
            -- Transmitter Statistics Interface
            --------------------------------------------
            tx_reset                   : out std_logic;
            tx_ifg_delay               : in  std_logic_vector(7 downto 0);
            tx_statistics_vector       : out std_logic_vector(31 downto 0);
            tx_statistics_valid        : out std_logic;
            -- Transmitter (AXI-S) Interface
            ---------------------------------------------
            tx_fifo_clock              : in  std_logic;
            tx_fifo_resetn             : in  std_logic;
            tx_axis_fifo_tdata         : in  std_logic_vector(7 downto 0);
            tx_axis_fifo_tvalid        : in  std_logic;
            tx_axis_fifo_tready        : out std_logic;
            tx_axis_fifo_tlast         : in  std_logic;
            -- MAC Control Interface
            --------------------------
            pause_req                  : in  std_logic;
            pause_val                  : in  std_logic_vector(15 downto 0);
            -- GMII Interface
            -------------------
            gmii_txd                  : out std_logic_vector(7 downto 0);
            gmii_tx_en                : out std_logic;
            gmii_tx_er                : out std_logic;
            gmii_rxd                  : in  std_logic_vector(7 downto 0);
            gmii_rx_dv                : in  std_logic;
            gmii_rx_er                : in  std_logic;
            clk_enable                : in  std_logic;
            speedis100                : out std_logic;
            speedis10100              : out std_logic;
            -- Configuration Vector
            -------------------------
            rx_configuration_vector   : in  std_logic_vector(79 downto 0);
            tx_configuration_vector   : in  std_logic_vector(79 downto 0));
    end component;

    component temac_10_100_1000_reset_sync
        port ( 
            reset_in    : in  std_logic;    -- Active high asynchronous reset
            enable      : in  std_logic;
            clk         : in  std_logic;    -- clock to be sync'ed to
            reset_out   : out std_logic);     -- "Synchronised" reset signal
    end component;

    component temac_10_100_1000_config_vector_sm is
    port(
        gtx_clk                 : in  std_logic;
        gtx_resetn              : in  std_logic;
        mac_speed               : in  std_logic_vector(1 downto 0);
        update_speed            : in  std_logic;
        rx_configuration_vector : out std_logic_vector(79 downto 0);
        tx_configuration_vector : out std_logic_vector(79 downto 0));
    end component;

    -------------------------------------------------------------------
    -- Transceiver, TEMAC, UDP_ICMP block
    ------------------------------------------------------------------- 

    -- clock generation signals for transceiver
    signal gtrefclkp, gtrefclkn             : std_logic;                    -- Route gtrefclk through an IBUFG.
    signal txoutclk                         : std_logic;                    -- txoutclk from GT transceiver
    signal resetdone                        : std_logic;                    -- To indicate that the GT transceiver has completed its reset cycle
    signal clkfbout                         : std_logic;                    -- MMCM feedback clock
    signal userclk                          : std_logic;                    -- 62.5MHz clock for GT transceiver Tx/Rx user clocks
    signal userclk2_i                       : std_logic;                    -- 125MHz clock for core reference clock.
    -- PMA reset generation signals for tranceiver
    signal pma_reset_pipe                   : std_logic_vector(3 downto 0); -- flip-flop pipeline for reset duration stretch
    signal pma_reset                        : std_logic;                    -- Synchronous transcevier PMA reset
    -- An independent clock source used as the reference clock for an
    -- IDELAYCTRL (if present) and for the main GT transceiver reset logic.
    signal independent_clock_bufg           : std_logic;
    -- clock generation signals for SGMII clock
    signal sgmii_clk_r                      : std_logic;                    -- Clock to client MAC (125MHz, 12.5MHz or 1.25MHz) (to rising edge DDR).
    signal sgmii_clk_f                      : std_logic;                    -- Clock to client MAC (125MHz, 12.5MHz or 1.25MHz) (to falling edge DDR).
    -- GMII signals
    signal gmii_isolate                     : std_logic;                    -- Internal gmii_isolate signal.
    signal gmii_txd_int                     : std_logic_vector(7 downto 0); -- Internal gmii_txd signal (between core and SGMII adaptation module).
    signal gmii_tx_en_int                   : std_logic;                    -- Internal gmii_tx_en signal (between core and SGMII adaptation module).
    signal gmii_tx_er_int                   : std_logic;                    -- Internal gmii_tx_er signal (between core and SGMII adaptation module).
    signal gmii_rxd_int                     : std_logic_vector(7 downto 0); -- Internal gmii_rxd signal (between core and SGMII adaptation module).
    signal gmii_rx_dv_int                   : std_logic;                    -- Internal gmii_rx_dv signal (between core and SGMII adaptation module).
    signal gmii_rx_er_int                   : std_logic;                    -- Internal gmii_rx_er signal (between core and SGMII adaptation module).
    signal phy_rstn_i                       : std_logic := '0';
    
    -- Extra registers to ease IOB placement
    signal status_vector_int                : std_logic_vector(15 downto 0);
    signal gmii_txd_emac                    : std_logic_vector(7 downto 0);
    signal gmii_tx_en_emac                  : std_logic; 
    signal gmii_tx_er_emac                  : std_logic; 
    signal gmii_rxd_emac                    : std_logic_vector(7 downto 0);
    signal gmii_rx_dv_emac                  : std_logic; 
    signal gmii_rx_er_emac                  : std_logic; 
    signal speed_is_10_100                  : std_logic;
    signal speed_is_100                     : std_logic;
    signal tx_axis_mac_tready_int           : std_logic;
    signal rx_axis_mac_tuser_int            : std_logic;
    signal rx_axis_mac_tlast_int            : std_logic;
    signal rx_axis_mac_tdata_int            : std_logic_vector(7 downto 0);
    signal rx_axis_mac_tvalid_int           : std_logic;
    signal local_gtx_reset                  : std_logic;
    signal rx_reset                         : std_logic;
    signal tx_reset                         : std_logic;
    signal gtx_pre_resetn                   : std_logic := '0';
    signal tx_axis_mac_tdata_int            : std_logic_vector(7 downto 0);    
    signal tx_axis_mac_tvalid_int           : std_logic;
    signal tx_axis_mac_tlast_int            : std_logic;  
    signal gtx_resetn                       : std_logic;
    signal glbl_rstn                        : std_logic := '1';
    signal glbl_rst_i                       : std_logic := '0';
    signal gtx_clk_reset_int                : std_logic;
    signal an_restart_config_int            : std_logic;
    signal rx_axis_mac_tready_int           : std_logic;
    signal rx_configuration_vector_int      : std_logic_vector(79 downto 0);
    signal tx_configuration_vector_int      : std_logic_vector(79 downto 0);
    signal vector_resetn                    : std_logic := '0';
    signal vector_pre_resetn                : std_logic := '0';
    signal vector_reset_int                 : std_logic;
    signal clk_enable_int                   : std_logic;
    signal control                          : udp_control_type;
    signal ip_rx_hdr_int                    : ipv4_rx_header_type;
    signal icmp_rx_start                    : std_logic;
    signal icmp_rxo                         : icmp_rx_type;
    signal local_gtx_reset_i                : std_logic := '0';
    signal an_rst_i                         : std_logic := '0';

    attribute ASYNC_REG                     : string;
    attribute ASYNC_REG of pma_reset_pipe   : signal is "TRUE";

begin

-----------------------------------------------------------------------------
-- Transceiver PMA reset circuitry
-----------------------------------------------------------------------------

core_wrapper: gig_ethernet_pcs_pma_0
    port map (
      gtrefclk_p           => gtrefclk_p,
      gtrefclk_n           => gtrefclk_n,
      txp                  => txp,
      txn                  => txn,
      rxp                  => rxp,
      rxn                  => rxn,
      gtrefclk_out         => open,
      gtrefclk_bufg_out    => txoutclk,
      rxuserclk_out        => open,
      rxuserclk2_out       => open,
      resetdone            => resetdone,
      mmcm_locked_out      => TXRXmmcm_locked,
      userclk_out          => userclk,
      userclk2_out         => userclk2_i,
      independent_clock_bufg => clk_200,
      pma_reset_out        => pma_reset,
      sgmii_clk_r          => sgmii_clk_r,
      sgmii_clk_f          => sgmii_clk_f,
      sgmii_clk_en         => clk_enable_int,
      gmii_txd             => gmii_txd_int,
      gmii_tx_en           => gmii_tx_en_int,
      gmii_tx_er           => gmii_tx_er_int,
      gmii_rxd             => gmii_rxd_int,
      gmii_rx_dv           => gmii_rx_dv_int,
      gmii_rx_er           => gmii_rx_er_int,
      gmii_isolate         => gmii_isolate,
      configuration_vector => "10000", -- configuration_vector,
      status_vector        => status_vector_int, -- status_vector_int,
      reset                => local_gtx_reset, -- was grounded
      signal_detect        => '1', -- signal_detect
      speed_is_10_100      => speed_is_10_100,
      speed_is_100         => speed_is_100,
      an_interrupt         => open,                    -- Interrupt to processor to signal that Auto-Negotiation has completed
      an_adv_config_vector =>  "1111111000000001",-- Alternate interface to program REG4 (AN ADV)
      an_restart_config    => an_restart_config_int,                     -- Alternate signal to modify AN restart bit in REG0

      gt0_pll0outclk_out     => open,
      gt0_pll0outrefclk_out  => open,
      gt0_pll1outclk_out     => open,
      gt0_pll1outrefclk_out  => open,
      gt0_pll0refclklost_out => open,
      gt0_pll0lock_out       => open
      );

gen_vector_reset_proc: process (userclk2_i)
    begin
     if userclk2_i'event and userclk2_i = '1' then
       if vector_reset_int = '1' then
         vector_pre_resetn  <= '0';
         vector_resetn      <= '0';
       else
         vector_pre_resetn  <= '1';
         vector_resetn      <= vector_pre_resetn;
       end if;
     end if;
end process;

an_restart_config_proc: process(local_gtx_reset)
    begin
        if (local_gtx_reset = '1') then
            an_restart_config_int <= '1';
        else
            an_restart_config_int <= '0';
        end if;
end process;

tri_fifo: temac_10_100_1000_fifo_block
    port map(
      gtx_clk                    => userclk2_i, --sgmii_clk_int, --userclk2_i,
      -- asynchronous reset
      glbl_rstn                  => glbl_rstn,
      rx_axi_rstn                => '1',
      tx_axi_rstn                => '1',
      -- Receiver Statistics Interface
      -----------------------------------------
      rx_reset                   => rx_reset,
      rx_statistics_vector       => open,
      rx_statistics_valid        => open,
      -- Receiver (AXI-S) Interface
      ------------------------------------------
      rx_fifo_clock              => userclk2_i,
      rx_fifo_resetn             => gtx_resetn,
      rx_axis_fifo_tdata         => rx_axis_mac_tdata_int,
      rx_axis_fifo_tvalid        => rx_axis_mac_tvalid_int,
      rx_axis_fifo_tready        => rx_axis_mac_tready_int,
      rx_axis_fifo_tlast         => rx_axis_mac_tlast_int,
      -- Transmitter Statistics Interface
      --------------------------------------------
      tx_reset                   => tx_reset,
      tx_ifg_delay               => x"00",
      tx_statistics_vector       => open,
      tx_statistics_valid        => open,
      -- Transmitter (AXI-S) Interface
      ---------------------------------------------
      tx_fifo_clock              => userclk2_i,
      tx_fifo_resetn             => gtx_resetn,
      tx_axis_fifo_tdata         => tx_axis_mac_tdata_int,
      tx_axis_fifo_tvalid        => tx_axis_mac_tvalid_int,
      tx_axis_fifo_tready        => tx_axis_mac_tready_int,
      tx_axis_fifo_tlast         => tx_axis_mac_tlast_int,
      -- MAC Control Interface
      --------------------------
      pause_req                  => '0',
      pause_val                  => x"0000",
      -- GMII Interface
      -------------------
      gmii_txd                  => gmii_txd_emac,
      gmii_tx_en                => gmii_tx_en_emac,
      gmii_tx_er                => gmii_tx_er_emac,
      gmii_rxd                  => gmii_rxd_emac,
      gmii_rx_dv                => gmii_rx_dv_emac,
      gmii_rx_er                => gmii_rx_er_emac,
      clk_enable                => clk_enable_int,
      speedis100                => speed_is_100,
      speedis10100              => speed_is_10_100,
      -- Configuration Vector
      -------------------------
      rx_configuration_vector   => rx_configuration_vector_int, -- x"0605_0403_02da_0000_2022",
      tx_configuration_vector   => tx_configuration_vector_int);  -- x"0605_0403_02da_0000_2022"
    
    -- Control vector reset
axi_lite_reset_gen: temac_10_100_1000_reset_sync
   port map (
       clk                      => userclk2_i,
       enable                   => '1',
       reset_in                 => local_gtx_reset_i,
       reset_out                => vector_reset_int);   
        
config_vector: temac_10_100_1000_config_vector_sm
    port map(
      gtx_clk                   => userclk2_i, --sgmii_clk_int, --userclk2_i,
      gtx_resetn                => vector_resetn,    
      mac_speed                 => status_vector_int(11 downto 10), -- "10",
      update_speed              => '1',    
      rx_configuration_vector   => rx_configuration_vector_int,
      tx_configuration_vector   => tx_configuration_vector_int);

-----------------------------------------------------------------------------
-- GMII transmitter data logic
-----------------------------------------------------------------------------

-- Drive input GMII signals through IOB input flip-flops (inferred).
gmii_tx_reg: process (userclk2_i)
begin
  if userclk2_i'event and userclk2_i = '1' then
     gmii_txd_int    <= gmii_txd_emac;
     gmii_tx_en_int  <= gmii_tx_en_emac;
     gmii_tx_er_int  <= gmii_tx_er_emac;
  end if;
end process;
    
gtx_reset_gen: temac_10_100_1000_reset_sync
    port map (
       clk              => userclk2_i,
       enable           => '1',
       reset_in         => local_gtx_reset,
       reset_out        => gtx_clk_reset_int);

gen_gtx_reset_proc: process (userclk2_i)
   begin
     if userclk2_i'event and userclk2_i = '1' then
       if gtx_clk_reset_int = '1' then
         gtx_pre_resetn   <= '0';
         gtx_resetn       <= '0';
       else
         gtx_pre_resetn   <= '1';
         gtx_resetn       <= gtx_pre_resetn;
       end if;
     end if;
end process;

-- Drive input GMII signals through IOB output flip-flops (inferred).
gmii_rx_reg: process (userclk2_i)
begin
      if userclk2_i'event and userclk2_i = '1' then
         gmii_rxd_emac          <= gmii_rxd_int;
         gmii_rx_dv_emac        <= gmii_rx_dv_int;
         gmii_rx_er_emac        <= gmii_rx_er_int;
      end if;
end process;

UDP_ICMP_block: UDP_ICMP_Complete_nomac
    Port map(
            udp_tx_start                => udp_tx_start,
            udp_txi                     => udp_txi, 
            udp_tx_result               => open,
            udp_tx_data_out_ready       => udp_tx_dout_rdy, 
            udp_rx_start                => open,
            udp_rxo                     => udp_rxo,
            icmp_rx_start               => icmp_rx_start,
            icmp_rxo                    => icmp_rxo,
            ip_rx_hdr                   => ip_rx_hdr_int,   
            rx_clk                      => userclk2_i,
            tx_clk                      => userclk2_i,
            reset                       => local_gtx_reset_i,
            fifo_init                   => fifo_init,
            our_ip_address              => FPGA_IP,
            our_mac_address             => FPGA_MAC,
            control                     => control,
            arp_pkt_count               => open,
            ip_pkt_count                => open,
            mac_tx_tdata                => tx_axis_mac_tdata_int,
            mac_tx_tvalid               => tx_axis_mac_tvalid_int,
            mac_tx_tready               => tx_axis_mac_tready_int,
            mac_tx_tfirst               => open,
            mac_tx_tlast                => tx_axis_mac_tlast_int,
            mac_rx_tdata                => rx_axis_mac_tdata_int,
            mac_rx_tvalid               => rx_axis_mac_tvalid_int,
            mac_rx_tready               => rx_axis_mac_tready_int,
            mac_rx_tlast                => rx_axis_mac_tlast_int
    );
    
sync_50to125: CDCC
        generic map(
            NUMBER_OF_BITS => 1) -- number of signals to be synced
        port map(
            clk_src         => clk_40,
            clk_dst         => userclk2_i,
            data_in(0)      => rst_eth,
            data_out_s(0)   => local_gtx_reset_i
            );

    userclk2        <= userclk2_i;
    local_gtx_reset <= local_gtx_reset_i or rx_reset or tx_reset;
    
end RTL;
