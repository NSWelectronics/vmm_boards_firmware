-----------------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 14.08.2018
-- Design Name: Startup Logic
-- Module Name: startup_logic - RTL
-- Project Name: MMFE8 - NTUA-BNL_VMM_firmware
-- Target Devices: Artix7 xc7a200t-2fbg484 and xc7a200t-3fbg484
-- Tool Versions: Vivado 2018.2
-- Description: Simple FSM that controls an LED pulse initialization pattern

-- Dependencies: MMFE8 NTUA/BNL Project
-- 
-- Changelog:
--
-----------------------------------------------------------------------------------------
library IEEE;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_1164.ALL;

entity startup is
    port(
        clk_40          : in  std_logic;
        mmcm_locked     : in  std_logic;
        tr_hold         : in  std_logic;
        gpio_led        : out std_logic_vector(5 downto 0);
        phy_rstn_out    : out std_logic;
        rst_init_out    : out std_logic
    );
end startup;

architecture RTL of startup is

    signal startup_cnt  : unsigned(22 downto 0)         := (others => '0');
    signal pulse_cnt    : unsigned(22 downto 0)         := (others => '0');
    signal sel_pat      : unsigned(2 downto 0)          := (others => '0');
    signal gpio_led_i   : std_logic_vector(5 downto 0)  := (others => '0');
    signal sel_led      : std_logic := '0';
    signal trHold_led   : std_logic := '0';
    signal gpio_led_log : std_logic_vector(5 downto 0)  := (others => '0');

    signal phy_resetn       : std_logic := '0';
    signal rst_init_i       : std_logic := '0';
    constant cnt_1ms        : natural := 50_000;

    type stateType is (ST_INIT, ST_0, ST_1, ST_2, ST_3, ST_4, ST_5, ST_SWITCH);
    signal state   : stateType := ST_INIT;

    type stateType_pulse is (ST_PULSE_0, ST_PULSE_1, ST_PULSE_2, ST_PULSE_3, ST_PULSE_4);
    signal state_pulse : stateType_pulse := ST_PULSE_0;

begin

-- startup FSM
startup_FSM_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        case state is
        when ST_INIT =>
            if(mmcm_locked = '1')then
                state <= ST_0;
            else
                state <= ST_INIT;
            end if;

        when ST_0 =>

            case sel_pat is
            when "000"  =>
                gpio_led_i(0) <= '1';
                gpio_led_i(1) <= '0';
                gpio_led_i(2) <= '0';
                gpio_led_i(3) <= '0';
                gpio_led_i(4) <= '0';
                gpio_led_i(5) <= '0';
            when "001"  =>
                gpio_led_i(0) <= '0';
                gpio_led_i(1) <= '0';
                gpio_led_i(2) <= '0';
                gpio_led_i(3) <= '0';
                gpio_led_i(4) <= '0';
                gpio_led_i(5) <= '1';
            when "010"  => gpio_led_i <= "111111";
            when "011"  => gpio_led_i <= "111111";
            when "100"  => gpio_led_i <= "111111";
            when "101"  => gpio_led_i <= "111111";
            when others => gpio_led_i <= "000000";
            end case;

            startup_cnt <= startup_cnt + 1;

            if(startup_cnt = "1111111111111111111111")then
                state <= ST_1;
            else
                state <= ST_0;
            end if;

        when ST_1 =>
            case sel_pat is
            when "000"  =>
                gpio_led_i(0) <= '0';
                gpio_led_i(1) <= '1';
                gpio_led_i(2) <= '0';
                gpio_led_i(3) <= '0';
                gpio_led_i(4) <= '0';
                gpio_led_i(5) <= '0';
            when "001"  =>
                gpio_led_i(0) <= '0';
                gpio_led_i(1) <= '0';
                gpio_led_i(2) <= '0';
                gpio_led_i(3) <= '0';
                gpio_led_i(4) <= '1';
                gpio_led_i(5) <= '0';
            when "010"  => gpio_led_i <= "000000";
            when "011"  => gpio_led_i <= "111111";
            when "100"  => gpio_led_i <= "111111";
            when "101"  => gpio_led_i <= "111111";
            when others => gpio_led_i <= "000000";
            end case;

            startup_cnt <= startup_cnt + 1;

            if(startup_cnt = "1111111111111111111111")then
                state <= ST_2;
            else
                state <= ST_1;
            end if;

        when ST_2 =>

            case sel_pat is
            when "000"  =>
                gpio_led_i(0) <= '0';
                gpio_led_i(1) <= '0';
                gpio_led_i(2) <= '1';
                gpio_led_i(3) <= '0';
                gpio_led_i(4) <= '0';
                gpio_led_i(5) <= '0';
            when "001"  =>
                gpio_led_i(0) <= '0';
                gpio_led_i(1) <= '0';
                gpio_led_i(2) <= '0';
                gpio_led_i(3) <= '1';
                gpio_led_i(4) <= '0';
                gpio_led_i(5) <= '0';
            when "010"  => gpio_led_i <= "111111";
            when "011"  => gpio_led_i <= "000000";
            when "100"  => gpio_led_i <= "111111";
            when "101"  => gpio_led_i <= "111111";
            when others => gpio_led_i <= "000000";
            end case;

            startup_cnt <= startup_cnt + 1;

            if(startup_cnt = "1111111111111111111111")then
                state <= ST_3;
            else
                state <= ST_2;
            end if;

        when ST_3 =>

            case sel_pat is
            when "000"  =>
                gpio_led_i(0) <= '0';
                gpio_led_i(1) <= '0';
                gpio_led_i(2) <= '0';
                gpio_led_i(3) <= '1';
                gpio_led_i(4) <= '0';
                gpio_led_i(5) <= '0';
            when "001"  =>
                gpio_led_i(0) <= '0';
                gpio_led_i(1) <= '0';
                gpio_led_i(2) <= '1';
                gpio_led_i(3) <= '0';
                gpio_led_i(4) <= '0';
                gpio_led_i(5) <= '0';
            when "010"  => gpio_led_i <= "000000";
            when "011"  => gpio_led_i <= "000000";
            when "100"  => gpio_led_i <= "000000";
            when "101"  => gpio_led_i <= "111111";
            when others => gpio_led_i <= "000000";
            end case;

            startup_cnt <= startup_cnt + 1;

            if(startup_cnt = "1111111111111111111111")then
                state <= ST_4;
            else
                state <= ST_3;
            end if;

        when ST_4 =>

            case sel_pat is
            when "000"  =>
                gpio_led_i(0) <= '0';
                gpio_led_i(1) <= '0';
                gpio_led_i(2) <= '0';
                gpio_led_i(3) <= '0';
                gpio_led_i(4) <= '1';
                gpio_led_i(5) <= '0';
            when "001"  =>
                gpio_led_i(0) <= '0';
                gpio_led_i(1) <= '1';
                gpio_led_i(2) <= '0';
                gpio_led_i(3) <= '0';
                gpio_led_i(4) <= '0';
                gpio_led_i(5) <= '0';
            when "010"  => gpio_led_i <= "111111";
            when "011"  => gpio_led_i <= "111111";
            when "100"  => gpio_led_i <= "000000";
            when "101"  => gpio_led_i <= "111111";
            when others => gpio_led_i <= "000000";
            end case;

            startup_cnt <= startup_cnt + 1;

            if(startup_cnt = "1111111111111111111111")then
                state <= ST_5;
            else
                state <= ST_4;
            end if;

        when ST_5 =>

            case sel_pat is
            when "000"  =>
                gpio_led_i(0) <= '0';
                gpio_led_i(1) <= '0';
                gpio_led_i(2) <= '0';
                gpio_led_i(3) <= '0';
                gpio_led_i(4) <= '0';
                gpio_led_i(5) <= '1';
            when "001"  =>
                gpio_led_i(0) <= '1';
                gpio_led_i(1) <= '0';
                gpio_led_i(2) <= '0';
                gpio_led_i(3) <= '0';
                gpio_led_i(4) <= '0';
                gpio_led_i(5) <= '0';
            when "010"  => gpio_led_i <= "000000";
            when "011"  => gpio_led_i <= "111111";
            when "100"  => gpio_led_i <= "000000";
            when "101"  => gpio_led_i <= "111111";
            when others => gpio_led_i <= "000000";
            end case;

            startup_cnt <= startup_cnt + 1;

            if(startup_cnt = "1111111111111111111111")then
                state <= ST_SWITCH;
            else
                state <= ST_5;
            end if;

        when ST_SWITCH =>
            gpio_led_i  <= "000000";
            if(sel_pat = "101")then
                sel_led <= '1';
                state   <= ST_SWITCH; -- stay
            else
                sel_pat <= sel_pat + 1;
                state   <= ST_0;
            end if;


        when others => state <= ST_INIT;
        end case;
    end if;
end process;

-- pulse led when pf is busy
pulse_pfBusy_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        case state_pulse is

        when ST_PULSE_0 =>
            trHold_led <= '0';

            if(tr_hold = '1')then
                state_pulse <= ST_PULSE_1;
            else
                state_pulse <= ST_PULSE_0;
            end if;

        when ST_PULSE_1 =>
            trHold_led  <= '1';
            pulse_cnt   <= pulse_cnt + 1;

            if(pulse_cnt = "1111111111111111111111")then
                state_pulse <= ST_PULSE_2;
            else
                state_pulse <= ST_PULSE_1;
            end if;

        when ST_PULSE_2 =>
            trHold_led  <= '1';
            pulse_cnt   <= pulse_cnt + 1;

            if(pulse_cnt = "1111111111111111111111")then
                state_pulse <= ST_PULSE_3;
            else
                state_pulse <= ST_PULSE_2;
            end if;

        when ST_PULSE_3 =>
            trHold_led  <= '0';
            pulse_cnt   <= pulse_cnt + 1;

            if(pulse_cnt = "1111111111111111111111")then
                state_pulse <= ST_PULSE_4;
            else
                state_pulse <= ST_PULSE_3;
            end if;

        when ST_PULSE_4 =>
            trHold_led  <= '0';
            pulse_cnt   <= pulse_cnt + 1;
            if(pulse_cnt = "1111111111111111111111")then
                state_pulse <= ST_PULSE_0;
            else
                state_pulse <= ST_PULSE_4;
            end if;

        when others => state_pulse <= ST_PULSE_0;
        end case;
    end if;
end process;

    gpio_led_log(0)       <= mmcm_locked;
    gpio_led_log(1)       <= '0';
    gpio_led_log(2)       <= '0';
    gpio_led_log(3)       <= trHold_led;
    gpio_led_log(4)       <= '0';
    gpio_led_log(5)       <= '0';

sel_led_proc: process(sel_led, gpio_led_i, gpio_led_log)
begin
    case sel_led is 
    when '0'    => gpio_led <= gpio_led_i;
    when '1'    => gpio_led <= gpio_led_log;
    when others => gpio_led <= (others => '0');
    end case;
end process;


-----------------------

-- old logic
    phy_rstn_out     <= phy_resetn;
    rst_init_out     <= rst_init_i;


phy_resetn_process: process(clk_40)

    variable cnt : natural range 0 to cnt_1ms := 0; --1ms

begin
    if(rising_edge(clk_40)) then
        if phy_resetn = '0' then --resetn
            if(cnt < cnt_1ms)then --cnt
               cnt := cnt + 1;
               rst_init_i <= '1';
            elsif(cnt = cnt_1ms)then
               cnt := 0;
               phy_resetn <= '1';
               rst_init_i <= '0';
            else null;
            end if; --cnt
        else
            rst_init_i <= rst_init_i;
            phy_resetn  <= phy_resetn;
        end if; --resetn check
    end if; --clk               
end process;



end RTL;