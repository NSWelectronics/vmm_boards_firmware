library IEEE;
library UNISIM;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use UNISIM.VComponents.all;

entity I2C_controller IS

	generic( 
        clock_rate : integer := 50_000_000; -- 50 Mhz clock
        bps        : integer := 100_000;    -- 100 kbps (the I2C standard)
        max_index  : integer := 30);       -- 30 internal clock cycles at some final FSM states (was 500)
					
    port (
        CLK_50_MHz  : in    std_logic; 
        ResetN      : in    std_logic;
        --RTC/I2C control signals
        SCL         : out   std_logic;
        SDA         : inout std_logic;
        --User interface data signals
        Read_WriteN : in    std_logic;
        DataIn      : in    std_logic_vector(7 DOWNTO 0);
        AddressIn   : in    std_logic_vector(7 DOWNTO 0);
        DataOut     : out   std_logic_vector(7 DOWNTO 0);
        Done        : out   std_logic;
        Start       : in    std_logic;
        i2cClockOut : out   std_logic);
end I2C_controller;

architecture FSMD of I2C_controller is

	signal state       : std_logic_vector(7 DOWNTO 0) := x"00";
	signal Data        : std_logic_vector(7 DOWNTO 0);
	signal SDA01       : std_logic;	-- internal SDA having values of 0 and 1
	signal finished    : std_logic := '0';
	signal start_latch : std_logic := '0';
	signal error       : std_logic := '0';
	
	signal bitcount    : integer RANGE 0 TO 7;
	signal index       : integer range 0 to max_index;
	
	constant SlaveAddress_Read     : std_logic_vector(7 DOWNTO 0)   := "1010000"&'1';	-- I2C address of the slave + read
    constant SlaveAddress_Write    : std_logic_vector(7 DOWNTO 0)   := "1010000"&'0';	-- I2C address of the slave + write'
    constant UpperAddressByte      : std_logic_vector(7 downto 0)   := x"00";
	
	-- constants for 200kHz clock divider
	constant max200k: integer := clock_rate/(bps*2);	-- to get 100kHz for I2C standard; every 2 cycles = 1 I2C cycle 
--	CONSTANT max200k: INTEGER := 50000000/4;	-- for some I2C slaves (Maxim DS1375 RTC) the actual clock speed can be any speed less than,
-- the max I2C clock speed  ,,,,,,,50000000/(100000*2)
	-- some I2C slaves (Maxim DS3232 RTC) requires a minimum SCL rate, so when this clock is too slow, the slave will reset the I2C interface
	signal clockticks200k  : integer range 0 TO max200k;
	signal CLK_200k_Hz     : std_logic;
	signal CLK_200k_Hz_ub  : std_logic;
	
begin

	BUFG_clk200khz : BUFG
    port map (
        O => CLK_200k_Hz, -- 1-bit output: Clock output
        I => CLK_200k_Hz_ub -- 1-bit input: Clock input
    ); -- End of BUFG_inst instantiation
	
	-- SDA line is open drain that is pulled up with a resistor
	-- therefore, to set SDA to a 1 we need to output a Z value
	-- SDA is changed only when SCL is low except for sending the start and stop bits
	-- i.e. SDA is valid at the rising edge of the SCL (clock)
	SDA            <= 'Z' when SDA01 = '1' else '0';	-- convert SDA 0/1 to 0/Z
	i2cClockOut    <= CLK_200k_Hz;
	
	output: process(CLK_200k_Hz, ResetN)
	begin
		if (ResetN = '0') then
			-- when idle, both SDA and SCL = 1
			SCL      <= '1';
			SDA01    <= '1';
			DataOut  <= "00000000";
			state    <= x"00";
			error    <= '0';
		elsif (CLK_200k_Hz'event and CLK_200k_Hz = '1') then
			case state is
			when x"00" =>	-- Idle
				-- when idle, both SDA and SCL = 1
				SCL     <= '1';		-- SCL = 1
				SDA01   <= '1';	-- SDA = 1
				DataOut <= "00000000";
				error   <= '0';
				if (start_latch = '1') then
				    state <= x"01";
				end if;
----------------------------------------------------------------				
-- send start condition and slave address
			when x"01" =>	-- Start
				SCL         <= '1';		-- SCL stays at 1 while
				SDA01       <= '0';	-- SDA changes from 1 to 0
				bitcount    <= 7;	-- starting bit count
				state <= x"02";
			-- send 7-bit slave address followed by R/W' bit, MSB first
			when x"02" =>					
				SCL     <= '0';
				SDA01   <= SlaveAddress_Write(bitcount);
				state   <= x"03";
			when x"03" =>
				SCL <= '1';
				if (bitcount - 1) >= 0 then
					bitcount <= bitcount - 1;
					state <= x"02";
				else
					bitcount <= 7;
					state <= x"12";
				end if;
			-- get acknowledgment' from slave
			when x"12" =>
				SCL <= '0';
				SDA01 <= '1';
				state <= x"13";
			when x"13" =>
				SCL <= '1'; --acknowledge'; error if it is a 1
				if SDA = '1' then
					state <= x"EE";	-- acknowledge error
				else
					state <= x"20";	-- send register address
				end if;

----------------------------------------------------------------
			-- send the first 8-bit register address to slave
			when x"20" =>
				SCL     <= '0';
				SDA01   <= UpperAddressByte(bitcount);	-- register address MSB first
				state   <= x"21";	-- continue
			when x"21" =>
				SCL <= '1';
				if (bitcount - 1) >= 0 then
					bitcount <= bitcount - 1;
					state <= x"20";
				else
					bitcount <= 7;
					state <= x"22";
				end if;
		    -- get acknowledgment'
            when x"22" =>
                SCL     <= '0';
                SDA01   <= '1';
                state   <= x"23";
            when x"23" =>
                SCL <= '1';    -- acknowledge'; error if it is a 1
                if SDA = '1' then
                    state <= x"EE"; -- acknowledge error
                else
                    state <= x"24";
                end if;
            -- send the next 8-bit register address to slave
            when x"24" =>
                SCL     <= '0';
                SDA01   <= AddressIn(bitcount);    -- register address MSB first
                state   <= x"25";    -- continue
            when x"25" =>
                SCL <= '1';
                if (bitcount - 1) >= 0 then
                    bitcount <= bitcount - 1;
                    state <= x"24";
                else
                    bitcount <= 7;
                    state <= x"30";
                end if;
			-- get acknowledgment'
			when x"30" =>
				SCL <= '0';
				SDA01 <= '1';
				state <= x"31";
			when x"31" =>
				SCL <= '1';	-- acknowledge'; error if it is a 1
				if SDA = '1' then
					state <= x"EE"; -- acknowledge error
				elsif Read_WriteN = '0' then
					state <= x"40";	-- go do a write --state <= x"40";
				else
					state <= x"70";	-- send repeated start and then go do a read
				end if;


----------------------------------------------------------------				
-- going to write data to slave
			-- send 8-bit data to slave
			when x"40" =>
				SCL <= '0';
				SDA01 <= DataIn(bitcount); 	-- Data MSB first
				state <= x"41";
			when x"41" =>
				SCL <= '1';
				if (bitcount - 1) >= 0 then
					bitcount <= bitcount - 1;
					state <= x"40";
				else
					bitcount <= 7;
					state <= x"50";
				end if;
			-- get acknowledgment'
			when x"50" =>
				SCL     <= '0';
				SDA01   <= '1';
				DataOut <= DataIn;
				state   <= x"51";
			when x"51" =>
				SCL <= '1'; --acknowledge'; error if it is a 1
				if SDA = '1' then
					state <= x"EE";
				else	-- send stop
					state <= x"52";
				end if;
			-- send stop
			-- SDA goes from 0 to 1 while SCL is 1
			when x"52" =>
				SCL     <= '0';
				SDA01   <= '0';	-- SDA starts at 0 to prepare for the 0 to 1 transition
				state   <= x"53";
			when x"53" =>
				SCL     <= '1';		-- SCL goes to 1
				SDA01   <= '0';	-- SDA starts from 0
				state   <= x"54";
			when x"54" =>	-- idle state
				SCL     <= '1';		-- SCL stays at 1 while
				SDA01   <= '1';	-- SDA goes to 1
			                  -- 	state <= x"54";
				
				if(index < max_index) then     -- delete
					index <= index + 1;  -- delete
			   	state <= x"54"; 
				elsif(index = max_index) then -- delete
					index <= 0;   -- delete
					state <= x"D6";    -- delete, back to idle
				else null;
				end if;


----------------------------------------------------------------
----------------------------------------------------------------
-- going to receive data from slave
			-- send repeated start condition to do a read after a write
			-- SDA goes from 1 to 0 while SCL is 1
			when x"70" =>
				SCL     <= '0';
				SDA01   <= '1';	-- SDA starts at 1 to prepare for the 1 to 0 transition
				if SDA = '1' then	-- SDA is still a 0 here from acknowledgement
					state <= x"EE";	-- acknowledge error
				else
					state <= x"71";	-- continue
				end if;
			when x"71" =>	-- idle state with both SCL and SDA at 1
				SCL     <= '1';		-- SCL goes to 1
				SDA01   <= '1';	-- SDA starts from 1
				state   <= x"72";
			when x"72" =>	-- Start
				SCL         <= '1';		-- SCL stays at 1 while
				SDA01       <= '0';	-- SDA changes from 1 to 0
				bitcount    <= 7;
				state       <= x"82";
			-- send 7-bit slave address followed by R/W' bit, MSB first
			-- slave address for RTC chip is 1101000
			when x"82" =>					
				SCL <= '0';
				SDA01 <= SlaveAddress_Read(bitcount);
				state <= x"83";
			when x"83" =>
				SCL <= '1';
				if (bitcount - 1) >= 0 then
					bitcount   <= bitcount - 1;
					state      <= x"82";
				else
					bitcount   <= 7;
					state      <= x"92";
				end if;
			-- get acknowledgment' from slave
			when x"92" =>
				SCL     <= '0';
				SDA01   <= '1';
				state   <= x"93";
			when x"93" =>
				SCL <= '1';  --acknowledge'; error if it is a 1
				if SDA = '1' then
					state <= x"EE";	-- acknowledge error
				else
					bitcount <= 7;
					state <= x"C0";	-- go to receive data byte
				end if;


----------------------------------------------------------------
			-- receive bytes from RTC
			when x"C0" =>
				SCL             <= '0';
				SDA01           <= '1';
				state           <= x"C1";
			when x"C1" =>
				SCL             <= '1';
				Data(bitcount)  <= SDA;	-- MSB of data read in
				if (bitcount - 1) >= 0 then
					bitcount    <= bitcount - 1;
					state       <= x"C0";
				else
					bitcount <= 7;
					state      <= x"D0";
				end if;
			-- if more bytes to read then send an acknowledge' (0) signal
			-- if no more bytes to read then send a not acknowledge' (1) signal
			when x"D0" =>
				SCL <= '0';
                SDA01   <= '1';	-- send a not acknowledge' (1) signal
                state   <= x"D2";	-- stop reading
				DataOut <= Data;
			when x"D2" =>
				SCL <= '1';
				state <= x"D3";
			-- send stop condition
			-- SDA goes from 0 to 1 while SCL is 1
			when x"D3" =>
				SCL <= '0';
				SDA01   <= '0';	-- SDA starts at 0 to prepare for the 0 to 1 transition
				state   <= x"D4";
			when x"D4" =>
				SCL     <= '1';		-- SCL goes to 1
				SDA01   <= '0';	-- SDA starts from 0
				state   <= x"D5";
			when x"D5" =>
				SCL     <= '1';		-- SCL stays at 1 while
				SDA01   <= '1';	-- SDA goes to 1
				if(index < max_index) then     -- begin loop
					index  <= index + 1;  
                state   <= x"D5"; 
				elsif(index = max_index) then 
					index  <= 0;   
					state  <= x"D6";   -- go back to idle
				else null;
				end if;


            when x"D6" =>
                state <= x"00";   -- go back to idle

-----------------------------------------------------------
			-- error - did not recieve the acknowledge
			when x"EE" =>
				--gets here only if ack is error
				SCL <= '1';
				SDA01 <= '1';
				error <= '1';
				if (index < max_index) then     -- delete
					index <= index + 1;  -- delete
			   	state <= x"EE"; 
				elsif (index = max_index) then -- delete
					index <= 0;   -- delete
					state <= x"00";    -- delete, back to idle
				else null;
				end if;
				
			when others => null;
				SCL <= '1';
				SDA01 <= '1';
			end CASE;
		end IF;
	end PROCESS;

	clockdivider200k: process(CLK_50_MHz)
	begin
		if CLK_50_MHz'event and CLK_50_MHz = '1' then
            if clockticks200k < max200k then
                clockticks200k <= clockticks200k + 1;
            else
                clockticks200k <= 0;
            end if;
            if clockticks200k < max200k/2 then
                CLK_200k_Hz_ub <= '0';
            else
                CLK_200k_Hz_ub <= '1';
            end if;
		end if;
	end process;
	
	i2c_trigger_proc: process(CLK_50_MHz)
    begin
        if CLK_50_MHz'event and CLK_50_MHz = '1' then
            if (error = '1') then
                finished        <= '0';
                start_latch     <= '0';
            end if;
            --if ((state = x"D5" or state = x"54") and index = max_index and finished = '0') then
            if (state = x"D6" and finished = '0') then
                 start_latch    <= '0';
                 done           <= '1';
                 finished       <= '1';
            elsif(state = x"01") then
                finished        <= '0';
            elsif Start = '1' then
                 start_latch    <= '1';
                 done           <= '0';
            end if;
        end if;
    end process;
	
end FSMD;