----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch)
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 14.03.2018 13:12:23
-- Design Name: 
-- Module Name: softReset_gen - RTL
-- Project Name: Soft Reset Generator
-- Target Devices: 
-- Tool Versions: 
-- Description: Module that generates a soft-reset pulse right before the CKTP
-- assertion
-- 
-- Dependencies: 
-- 
-- Changelog: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity softReset_gen is
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_160         : in  std_logic;
        clk_ckbc        : in  std_logic;
        cktp_enable     : in  std_logic;
        sr_enable       : in  std_logic;
        CKTP            : in  std_logic;
        ------------------------------------
        ----- Configuration Interface ------
        sr_offset       : in  std_logic_vector(11 downto 0);
        ------------------------------------
        ---------- VMM Interface -----------
        vmm_ena         : out std_logic
    );
end softReset_gen;

architecture RTL of softReset_gen is

component CDCC
    generic(
        NUMBER_OF_BITS : integer := 8); -- number of signals to be synced
    port(
        clk_src     : in  std_logic;                                        -- input clk (source clock)
        clk_dst     : in  std_logic;                                        -- input clk (dest clock)
        data_in     : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0);    -- data to be synced
        data_out_s  : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0)     -- synced data to clk_dst
    );
end component; 

    signal cktp_period_cnt  : unsigned(31 downto 0)         := (others => '0');
    signal sr_period_cnt    : unsigned(31 downto 0)         := (others => '0');
    signal sr_offset_final  : std_logic_vector(31 downto 0) := (others => '0');
    signal sr_period        : unsigned(31 downto 0)         := (others => '0');
    signal vmm_ena_i        : std_logic := '0';
    signal vmm_ena_sreg     : std_logic_vector(2 downto 0)  := (others => '0');
    signal vmm_ena_160      : std_logic := '0';

    type stateType is (ST_WAIT_CKTP_0, ST_WAIT_LOW, ST_WAIT_CKTP_1, ST_ASSERT_SR, ST_WAIT_CKTP_2);
    signal state       : stateType;

    attribute FSM_ENCODING              : string;
    attribute FSM_ENCODING of state     : signal is "ONE_HOT"; 

begin

-- FSM that evaluates the CKTP period and asserts the softReset
cktpPeriod_eval_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        if(cktp_enable = '0' or sr_enable = '0')then
            vmm_ena_i       <= '1';
            cktp_period_cnt <= (others => '0');
            sr_period_cnt   <= (others => '0');
            state           <= ST_WAIT_CKTP_0;
        else
            case state is

            -- wait for the CKTP to go high
            when ST_WAIT_CKTP_0 =>
                vmm_ena_i           <= '1';
                if(CKTP = '1')then
                    cktp_period_cnt <= cktp_period_cnt + 1;
                    state           <= ST_WAIT_LOW;
                else
                    cktp_period_cnt <= cktp_period_cnt;
                    state           <= ST_WAIT_CKTP_0;
                end if;

            -- wait for the CKTP to go low
            when ST_WAIT_LOW =>
                vmm_ena_i       <= '1';
                cktp_period_cnt <= cktp_period_cnt + 1;
                if(CKTP = '0')then
                    state  <= ST_WAIT_CKTP_1;
                else
                    state  <= ST_WAIT_LOW;
                end if;

            -- wait for the CKTP to go high again
            when ST_WAIT_CKTP_1 =>
                vmm_ena_i           <= '1';
                if(CKTP = '1')then
                    cktp_period_cnt <= cktp_period_cnt;
                    state           <= ST_ASSERT_SR;
                else
                    cktp_period_cnt <= cktp_period_cnt + 1;
                    state           <= ST_WAIT_CKTP_1;
                end if;

            -- will cycle around the following 2 states now

            -- wait to assert SR, stay here until SR assertion
            when ST_ASSERT_SR =>
                if(sr_period_cnt >= sr_period)then
                    sr_period_cnt   <= (others => '0');
                    vmm_ena_i       <= '0';
                    state           <= ST_WAIT_CKTP_2;  
                else
                    sr_period_cnt   <= sr_period_cnt + 1;
                    vmm_ena_i       <= '1';
                    state           <= ST_ASSERT_SR;
                end if;
                
            -- wait for CKTP again...
            when ST_WAIT_CKTP_2 =>
                vmm_ena_i       <= '1';

                if(CKTP = '1')then
                    state   <= ST_ASSERT_SR;
                else
                    state   <= ST_WAIT_CKTP_2;
                end if;


            when others => state <= ST_WAIT_CKTP_0;
            end case;
        end if;
    end if;
end process;

-- vmm ena pulse stretcher
enaStretcher_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        vmm_ena_sreg    <= vmm_ena_i & vmm_ena_sreg(2 downto 1);
        vmm_ena_160     <= vmm_ena_i and vmm_ena_sreg(0) and vmm_ena_sreg(1) and vmm_ena_sreg(2);
    end if;
end process;

-- ena2CKBC
ena2CKBC_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        vmm_ena <= vmm_ena_160;
    end if;
end process;

    sr_offset_final <= x"00000" & sr_offset;
    sr_period       <= cktp_period_cnt - unsigned(sr_offset_final);

end RTL;
