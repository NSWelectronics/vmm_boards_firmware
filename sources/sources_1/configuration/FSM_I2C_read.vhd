library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity FSM_I2C_read is
    Port (
        CLK_50_MHz                  : in  std_logic;
        destIP_out                  : out std_logic_vector(31 downto 0);
        srcIP_out                   : out std_logic_vector(31 downto 0);
        mac_out                     : out std_logic_vector(47 downto 0);
        destIP_retr                 : out std_logic_vector(31 downto 0);
        srcIP_retr                  : out std_logic_vector(31 downto 0);
        mac_retr                    : out std_logic_vector(47 downto 0);
        start                       : in  std_logic;
        done                        : out std_logic;
        destIP_address              : in  std_logic_vector(7 downto 0);
        srcIP_address               : in  std_logic_vector(7 downto 0);
        mac_address                 : in  std_logic_vector(7 downto 0);
        i2c_byte_retrieved          : in  std_logic_vector(7 downto 0);
        i2c_address_to_read         : out std_logic_vector(7 downto 0);
        i2c_start_sig               : out std_logic;
        i2c_done_sig                : in  std_logic;
        i2c_read_writeN_ctrl_flag   : out std_logic;
        destIP_default_val          : in  std_logic_vector(31 downto 0);
        srcIP_default_val           : in  std_logic_vector(31 downto 0);
        mac_default_val             : in  std_logic_vector(47 downto 0);
        srcIP_expected_val          : in  std_logic_vector(23 downto 0)
    );
end FSM_I2C_read;

architecture Logic of FSM_I2C_read is

    component mux_2to1_Nbit
        generic (N : integer);
        port ( 
            sel : in  std_logic;
            A   : in  std_logic_vector(N-1 downto 0);
            B   : in  std_logic_vector(N-1 downto 0);
            X   : out std_logic_vector(N-1 downto 0)
        );
    end component;

    type stateType                          is (ST_IDLE, ST_READ, ST_SEND_CMD, ST_WAIT, ST_RETRIEVE, ST_CHECK_VAL, ST_DEFAULT_VAL);
    signal state                            : stateType := ST_IDLE;
    attribute FSM_ENCODING                  : string;
    attribute FSM_ENCODING of state         : signal is "ONE_HOT";
    signal read_counter                     : unsigned(3 downto 0) := (others => '0');
    signal finished                         : std_logic := '0';
    signal i2c_address_to_read_unsg         : unsigned(7 downto 0) := (others => '0');
    
    signal destIP_retrieved                 : std_logic_vector(31 downto 0);
    signal srcIP_retrieved                  : std_logic_vector(31 downto 0);
    signal mac_retrieved                    : std_logic_vector(47 downto 0); 
    
    signal force_default_ip_values_mux      : std_logic := '0';
    
begin

    i2c_read_writeN_ctrl_flag   <= '1'; --always trying to read with i2c, wrapper will use mux to determine when this fsm will have acess to i2c module
    done                        <= finished;
    i2c_address_to_read         <= std_logic_vector(i2c_address_to_read_unsg); 
    
    destIP_retr                 <= destIP_retrieved;
    srcIP_retr                  <= srcIP_retrieved;
    mac_retr                    <= mac_retrieved;

    i2c_mux: mux_2to1_Nbit
    generic map (N => 112)
    port map(
        sel                 => force_default_ip_values_mux,
        A(31 downto 0)      => destIP_default_val,
        A(63 downto 32)     => srcIP_default_val,
        A(111 downto 64)    => mac_default_val,
        B(31 downto 0)      => destIP_retrieved,
        B(63 downto 32)     => srcIP_retrieved,
        B(111 downto 64)    => mac_retrieved,
        X(31 downto 0)      => destIP_out,
        X(63 downto 32)     => srcIP_out,
        X(111 downto 64)    => mac_out
    );
    
    output: process(CLK_50_MHz)
    begin
     
        if(rising_edge(CLK_50_MHz)) then
            case state is
            
                when ST_IDLE =>
                    read_counter                    <= (others => '0');
                    i2c_address_to_read_unsg        <= (others => '0');
                    i2c_start_sig                   <= '0';
                    if(start = '1') then
                        destIP_retrieved            <= (others => '0');
                        srcIP_retrieved             <= (others => '0');
                        mac_retrieved               <= (others => '0');
                        finished                    <= '0';
                        force_default_ip_values_mux <= '0';
                        state                       <= ST_READ; --begin sending commands to i2c module
                    end if;
                
                when ST_SEND_CMD =>
                    i2c_start_sig   <= '0';
                    state           <= ST_WAIT;
                
                when ST_WAIT =>
                    if(i2c_done_sig = '1') then
                        state       <= ST_RETRIEVE;
                    end if;
                
                when ST_READ =>
                    case read_counter is
                    
                        when "0000" | "0001" | "0010" | "0011"=>
                            i2c_address_to_read_unsg    <= unsigned(destIP_address) + read_counter(1 downto 0);
                            i2c_start_sig               <= '1';
                            state                       <= ST_SEND_CMD;
                                                   
                        when "0100" | "0101" | "0110" | "0111" =>
                            i2c_address_to_read_unsg    <= unsigned(srcIP_address) + read_counter(1 downto 0);
                            i2c_start_sig               <= '1';
                            state                       <= ST_SEND_CMD;
                        
                        when "1000" | "1001" | "1010" | "1011" | "1100" | "1101" =>
                            i2c_address_to_read_unsg    <= unsigned(mac_address) + read_counter(2 downto 0); 
                            i2c_start_sig               <= '1'; 
                            state                       <= ST_SEND_CMD;
                                  
                        when "1110" =>
                            state                       <= ST_CHECK_VAL;  --set default IP if srcIP does not match the expected pattern
                            --state                       <= ST_DEFAULT_VAL; --skip default IP and set from EEPROM regardless stored values
                        WHEN OTHERS => null;
                        
                    end case;
                
                when ST_CHECK_VAL =>
                    
                    if ( srcIP_retrieved(31 downto 8) = srcIP_expected_val ) then
                        finished                    <= '1';
                        state                       <= ST_IDLE;
                    else
                        force_default_ip_values_mux <= '1';
                        state                       <= ST_DEFAULT_VAL;
                    end if;
                    
                when ST_DEFAULT_VAL =>
                    finished                        <= '1';
                    state                           <= ST_IDLE;
                                   
                when ST_RETRIEVE =>
                
                    read_counter <= read_counter + 1; 
                    
                    case read_counter is
                    
                        when "0000" =>
                            destIP_retrieved(7 downto 0)    <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "0001" =>
                            destIP_retrieved(15 downto 8)   <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "0010" =>
                            destIP_retrieved(23 downto 16)  <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "0011" =>
                            destIP_retrieved(31 downto 24)  <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "0100" =>
                            srcIP_retrieved(7 downto 0)     <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "0101" =>
                            srcIP_retrieved(15 downto 8)    <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "0110" =>
                            srcIP_retrieved(23 downto 16)   <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "0111" =>
                            srcIP_retrieved(31 downto 24)   <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "1000" =>
                            mac_retrieved(7 downto 0)       <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "1001" =>
                            mac_retrieved(15 downto 8)      <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "1010" =>
                            mac_retrieved(23 downto 16)     <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "1011" =>
                            mac_retrieved(31 downto 24)     <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "1100" =>
                            mac_retrieved(39 downto 32)     <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                        
                        when "1101" =>
                            mac_retrieved(47 downto 40)     <= i2c_byte_retrieved;
                            state                           <= ST_READ;
                            
                        when others => null;
                                                    
                     end case;
                            
                when others => null;
                
            end case;
        end if;
    end process;

end Logic;
