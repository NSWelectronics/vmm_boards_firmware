-----------------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 10.08.2018
-- Design Name: VMM Interface Block
-- Module Name: vmm_intf_block - RTL
-- Project Name: MMFE8 - NTUA-BNL_VMM_firmware
-- Target Devices: Artix7 xc7a200t-2fbg484 and xc7a200t-3fbg484
-- Tool Versions: Vivado 2018.2
-- Description: Wrapper for the ODDR/IDDR and IOB registers

-- Dependencies: MMFE8 NTUA/BNL Project
-- 
-- Changelog:
-- 18.08.2018 Added CKBC gater module. (Christos Bakalis)
--
-----------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity vmm_intf_block is
    generic(is_mmfe8    : std_logic := '1';
            is_level0   : std_logic := '0');
    port(
        ------------------------------------
        ----- FPGA Logic Interface ---------
        clk_40              : in  std_logic;
        clk_160             : in  std_logic;
        clk_320             : in  std_logic;
        mmcm_locked         : in  std_logic;
        cktp                : in  std_logic;
        ckdt                : in  std_logic;
        ckbc                : in  std_logic;
        ckbc_mode           : in  std_logic;
        trg_en              : in  std_logic;
        req_ckbc            : in  std_logic;
        ckbc_max_num        : in  std_logic_vector(7 downto 0);
        ckbc_freq           : in  std_logic_vector(5 downto 0);
        vmm_tki_in          : in  std_logic;
        vmm_tko_out         : out std_logic;
        ext_trg_out         : out std_logic;
        ext_rst_out         : out std_logic;
        ext_inh_out         : out std_logic;
        ext_busy_in         : in  std_logic;
        vmm_ckbc_enable     : in  std_logic;
        vmm_cktp_enable     : in  std_logic;
        vmm_ckdt_enable     : in  std_logic_vector(7 downto 0);
        vmm_ena_in          : in  std_logic_vector(7 downto 0);
        vmm_cs_in           : in  std_logic_vector(7 downto 0);
        vmm_sdi_in          : in  std_logic_vector(7 downto 0);
        vmm_sck_in          : in  std_logic_vector(7 downto 0);
        vmm_cktk_in         : in  std_logic_vector(7 downto 0);
        vmm_data0_out       : out std_logic_vector(7 downto 0);
        vmm_data1_out       : out std_logic_vector(7 downto 0);
        ------------------------------------
        ------------ VMM Interface ---------
        vmm_ena_out         : out std_logic_vector(7 downto 0);
        vmm_cktk_out        : out std_logic_vector(7 downto 0);
        vmm_ckbc_out        : out std_logic_vector(7 downto 0);
        vmm_ckdt_out        : out std_logic_vector(7 downto 0);
        vmm_cktp_out        : out std_logic_vector(7 downto 0);
        vmm_ckart_out       : out std_logic_vector(8 downto 0);
        vmm_sdi_out         : out std_logic_vector(7 downto 0);
        vmm_sck_out         : out std_logic_vector(7 downto 0);
        vmm_cs_out          : out std_logic_vector(7 downto 0);
        vmm_tki_out         : out std_logic;
        vmm_tki_mmfe8_out   : out std_logic;
        vmm_tko_in          : in  std_logic;
        vmm_data0_in        : in  std_logic_vector(7 downto 0);
        vmm_data1_in        : in  std_logic_vector(7 downto 0);
        ------------------------------------
        ------------ Ext Interface ---------
        ext_rst_in          : in  std_logic;
        ext_trg_in          : in  std_logic;
        ext_inh_in          : in  std_logic;
        ext_busy_out        : out std_logic
 );
end vmm_intf_block;

architecture RTL of vmm_intf_block is

component ckbc_gater
    generic(is_level0 : std_logic := '0');
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_40          : in  std_logic;
        clk_320         : in  std_logic;
        trg_en          : in  std_logic;
        ro_mode         : in  std_logic;
        ckbc_enable     : in  std_logic;
        mmcm_locked     : in  std_logic;
        ckbc_freq       : in  std_logic_vector(5 downto 0);
        ------------------------------------
        ------- Trigger Interface ----------
        req_ckbc        : in  std_logic; -- must be synced to 40
        ckbc_max_num    : in  std_logic_vector(7 downto 0);
        ------------------------------------
        ----- CKBC ODDR Interface ----------
        ckbc_rst        : out std_logic
    );
end component;

    signal rst_ckdt     : std_logic_vector(7 downto 0) := (others => '0');
    signal rst_ckbc     : std_logic_vector(7 downto 0) := (others => '0');
    signal rst_cktp     : std_logic_vector(7 downto 0) := (others => '0');

    signal trg_i        : std_logic_vector(1 downto 0) := (others => '0');
    signal rst_i        : std_logic_vector(1 downto 0) := (others => '0');
    signal inh_i        : std_logic_vector(1 downto 0) := (others => '0');

    type sregArray is array (7 downto 0) of std_logic_vector(2 downto 0);
    signal ena_sreg     : sregArray;
    signal cktk_sreg    : sregArray;
    signal sdi_sreg     : sregArray;
    signal cs_sreg      : sregArray;
    signal sck_sreg     : sregArray;
    signal tki_sreg     : sregArray;

    signal rst_ckbc_i   : std_logic := '0';

-- function to convert std_logic to integer for instance generation
function sl2int (x: std_logic) return integer is
begin
    if(x='1')then 
        return 7; 
    else 
        return 0; 
    end if;
end;
begin

tki_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        tki_sreg(0) <= vmm_tki_in & tki_sreg(0)(2 downto 1);

        case ckbc_freq is
        when "001010" => -- 10 Mhz
            vmm_tki_out         <= tki_sreg(0)(2) or tki_sreg(0)(1) or tki_sreg(0)(0) or vmm_tki_in;
            vmm_tki_mmfe8_out   <= tki_sreg(0)(2) or tki_sreg(0)(1) or tki_sreg(0)(0) or vmm_tki_in;
        when "010100" => -- 20 Mhz
            vmm_tki_out         <= tki_sreg(0)(2) or vmm_tki_in;
            vmm_tki_mmfe8_out   <= tki_sreg(0)(2) or vmm_tki_in;
        when "101000" => -- 40 Mhz
            vmm_tki_out         <= vmm_tki_in;
            vmm_tki_mmfe8_out   <= vmm_tki_in;
        when others => 
            vmm_tki_out         <= vmm_tki_in;
            vmm_tki_mmfe8_out   <= vmm_tki_in;
        end case;

        vmm_tko_out <= vmm_tko_in;
    end if;
end process;

gen_buffers_vmm: for I in 0 to 7 generate

-- configuration signals pipeline and stretcher
configPipe_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then

        ena_sreg(I)     <= vmm_ena_in(I)  & ena_sreg(I)(2 downto 1);
        cktk_sreg(I)    <= vmm_cktk_in(I) & cktk_sreg(I)(2 downto 1);
        sdi_sreg(I)     <= vmm_sdi_in(I)  & sdi_sreg(I)(2 downto 1);
        cs_sreg(I)      <= vmm_cs_in(I)   & cs_sreg(I)(2 downto 1);
        sck_sreg(I)     <= vmm_sck_in(I)  & sck_sreg(I)(2 downto 1);

        case ckbc_freq is
        when "001010" => 
            vmm_ena_out(I)  <= ena_sreg(I)(2) or ena_sreg(I)(1) or ena_sreg(I)(0) or vmm_ena_in(I);
            vmm_cktk_out(I) <= cktk_sreg(I)(2) or cktk_sreg(I)(1) or cktk_sreg(I)(0) or vmm_cktk_in(I);
            vmm_sdi_out(I)  <= sdi_sreg(I)(2) or sdi_sreg(I)(1) or sdi_sreg(I)(0) or vmm_sdi_in(I);
            vmm_cs_out(I)   <= cs_sreg(I)(2) or cs_sreg(I)(1) or cs_sreg(I)(0) or vmm_cs_in(I);
            vmm_sck_out(I)  <= sck_sreg(I)(2) or sck_sreg(I)(1) or sck_sreg(I)(0) or vmm_sck_in(I);
        when "010100" => 
            vmm_ena_out(I)  <= ena_sreg(I)(2) or vmm_ena_in(I);
            vmm_cktk_out(I) <= cktk_sreg(I)(2) or vmm_cktk_in(I);
            vmm_sdi_out(I)  <= sdi_sreg(I)(2) or vmm_sdi_in(I);
            vmm_cs_out(I)   <= cs_sreg(I)(2) or vmm_cs_in(I);
            vmm_sck_out(I)  <= sck_sreg(I)(2) or vmm_sck_in(I);
        when "101000" => 
            vmm_ena_out(I)  <= vmm_ena_in(I);
            vmm_cktk_out(I) <= vmm_cktk_in(I);
            vmm_sdi_out(I)  <= vmm_sdi_in(I);
            vmm_cs_out(I)   <= vmm_cs_in(I);
            vmm_sck_out(I)  <= vmm_sck_in(I);
        when others   => 
            vmm_ena_out(I)  <= vmm_ena_in(I);
            vmm_cktk_out(I) <= vmm_cktk_in(I);
            vmm_sdi_out(I)  <= vmm_sdi_in(I);
            vmm_cs_out(I)   <= vmm_cs_in(I);
            vmm_sck_out(I)  <= vmm_sck_in(I);
        end case;
    end if;
end process;

    -- no pipeline for data signals (they get registered later on)
    vmm_data0_out(I) <= vmm_data0_in(I);
    vmm_data1_out(I) <= vmm_data1_in(I);

    rst_ckdt(I) <= not vmm_ckdt_enable(I);
    rst_cktp(I) <= not vmm_cktp_enable;
    rst_ckbc(I) <= rst_ckbc_i;

ODDR_CKDT: ODDR
    generic map(
        DDR_CLK_EDGE => "OPPOSITE_EDGE",
        INIT         => '0',
        SRTYPE       => "SYNC")
    port map(
        Q   => vmm_ckdt_out(I),
        C   => ckdt,
        CE  => '1',
        D1  => '1',
        D2  => '0',
        R   => rst_ckdt(I),
        S   => '0' 
    );

ODDR_CKTP: ODDR
    generic map(
        DDR_CLK_EDGE => "OPPOSITE_EDGE",
        INIT         => '0',
        SRTYPE       => "SYNC")
    port map(
        Q   => vmm_cktp_out(I),
        C   => cktp,
        CE  => '1',
        D1  => '1',
        D2  => '0',
        R   => rst_cktp(I),
        S   => '0' 
    );

ODDR_CKBC: ODDR
    generic map(
        DDR_CLK_EDGE => "OPPOSITE_EDGE",
        INIT         => '0',
        SRTYPE       => "SYNC")
    port map(
        Q   => vmm_ckbc_out(I),
        C   => ckbc,
        CE  => '1',
        D1  => '1',
        D2  => '0',
        R   => rst_ckbc(I),
        S   => '0' 
    );

end generate gen_buffers_vmm;

gen_buffers_ckart: for I in 0 to 8 generate
ODDR_CKART: ODDR
    generic map(
        DDR_CLK_EDGE => "OPPOSITE_EDGE",
        INIT         => '0',
        SRTYPE       => "SYNC")
    port map(
        Q   => vmm_ckart_out(I),
        C   => clk_160,
        CE  => '1',
        D1  => '1',
        D2  => '0',
        R   => '0',
        S   => '0' 
    );
end generate gen_buffers_ckart;

-- external interface
IDDR_TRG: IDDR
    generic map (
        DDR_CLK_EDGE => "SAME_EDGE",
        INIT_Q1      => '0',
        INIT_Q2      => '0',
        SRTYPE       => "SYNC")
    port map (
        Q1  => trg_i(0),
        Q2  => trg_i(1),
        C   => clk_160,
        CE  => '1',
        D   => ext_trg_in,
        R   => '0',
        S   => '0'
    );

IDDR_RST: IDDR
    generic map (
        DDR_CLK_EDGE => "SAME_EDGE",
        INIT_Q1      => '0',
        INIT_Q2      => '0',
        SRTYPE       => "SYNC")
    port map (
        Q1  => rst_i(0),
        Q2  => rst_i(1),
        C   => clk_160,
        CE  => '1',
        D   => ext_rst_in,
        R   => '0',
        S   => '0'
    );

IDDR_INH: IDDR
    generic map (
        DDR_CLK_EDGE => "SAME_EDGE",
        INIT_Q1      => '0',
        INIT_Q2      => '0',
        SRTYPE       => "SYNC")
    port map (
        Q1  => inh_i(0),
        Q2  => inh_i(1),
        C   => clk_40,
        CE  => '1',
        D   => ext_inh_in,
        R   => '0',
        S   => '0'
    );

ODDR_BUSY: ODDR
    generic map(
        DDR_CLK_EDGE => "SAME_EDGE",
        INIT         => '0',
        SRTYPE       => "SYNC")
    port map(
        Q   => ext_busy_out,
        C   => clk_40,
        CE  => '1',
        D1  => ext_busy_in,
        D2  => ext_busy_in,
        R   => '0',
        S   => '0' 
    );

extIntf_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        ext_trg_out <= trg_i(0) or trg_i(1);
        ext_rst_out <= rst_i(0) or rst_i(1);
        ext_inh_out <= inh_i(0) or inh_i(1);
    end if;
end process;

ckbc_gater_inst: ckbc_gater
    generic map(is_level0 => is_level0)
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_40          => clk_40,
        clk_320         => clk_320,
        trg_en          => trg_en,
        ro_mode         => ckbc_mode,
        mmcm_locked     => mmcm_locked,
        ckbc_enable     => vmm_ckbc_enable,
        ckbc_freq       => ckbc_freq,
        ------------------------------------
        ------- Trigger Interface ----------
        req_ckbc        => req_ckbc,
        ckbc_max_num    => ckbc_max_num,
        ------------------------------------
        ----- CKBC ODDR Interface ----------
        ckbc_rst        => rst_ckbc_i
    );

end RTL;

