library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use ieee.numeric_std.all;

entity i2c_wrapper is
    Port ( 
        CLK_50_MHz          : in    std_logic;
        CLK_40_MHz          : in    std_logic;
        CLK_125_MHz         : in    std_logic;
        new_IP_ready        : in    std_logic;
        module_busy         : out   std_logic;
        SDA                 : inout std_logic;
        SCL                 : out   std_logic;
        destIP              : in    std_logic_vector(31 downto 0);
        srcIP               : in    std_logic_vector(31 downto 0);
        mac                 : in    std_logic_vector(47 downto 0);
        destIP_returned     : out   std_logic_vector(31 downto 0);
        srcIP_returned      : out   std_logic_vector(31 downto 0);
        mac_returned        : out   std_logic_vector(47 downto 0);
        destIP_default_val  : in    std_logic_vector(31 downto 0);
        srcIP_default_val   : in    std_logic_vector(31 downto 0);
        mac_default_val     : in    std_logic_vector(47 downto 0)
     );
end i2c_wrapper;

architecture Logic of i2c_wrapper is

    component CDCC
    generic(
        NUMBER_OF_BITS : integer := 8);
    port(
        clk_src     : in  std_logic;
        clk_dst     : in  std_logic;
        data_in     : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0);
        data_out_s  : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0));
    end component;

    component I2C_controller
	port(
		CLK_50_MHz    : in    std_logic;
		ResetN        : in    std_logic;
		Read_WriteN   : in    std_logic;
		DataIn        : in    std_logic_vector(7 downto 0);
		AddressIn     : in    std_logic_vector(7 downto 0);    
		SDA           : inout std_logic;      
		SCL           : out   std_logic;
		DataOut       : out   std_logic_vector(7 downto 0);
		Done          : out   std_logic;
        Start         : in    std_logic;
        i2cClockOut   : out   std_logic);
	end component;
	
	component FSM_I2C_read
	port ( 
        CLK_50_MHz                  : in  std_logic;
        destIP_out                  : out std_logic_vector(31 downto 0);
        srcIP_out                   : out std_logic_vector(31 downto 0);
        mac_out                     : out std_logic_vector(47 downto 0);
        destIP_retr                 : out std_logic_vector(31 downto 0);
        srcIP_retr                  : out std_logic_vector(31 downto 0);
        mac_retr                    : out std_logic_vector(47 downto 0);
        start                       : in  std_logic;
        done                        : out std_logic;
        destIP_address              : in  std_logic_vector(7 downto 0);
        srcIP_address               : in  std_logic_vector(7 downto 0);
        mac_address                 : in  std_logic_vector(7 downto 0);
        i2c_byte_retrieved          : in  std_logic_vector(7 downto 0);
        i2c_address_to_read         : out std_logic_vector(7 downto 0);
        i2c_start_sig               : out std_logic;
        i2c_done_sig                : in  std_logic;
        i2c_read_writeN_ctrl_flag   : out std_logic;
        destIP_default_val          : in  std_logic_vector(31 downto 0);
        srcIP_default_val           : in  std_logic_vector(31 downto 0);
        mac_default_val             : in  std_logic_vector(47 downto 0);
        srcIP_expected_val          : in  std_logic_vector(23 downto 0)
    );
    end component;
    
    component FSM_I2C_write
    port (
        CLK_50_MHz                  : in  std_logic;
        destIP                      : in  std_logic_vector(31 downto 0);
        srcIP                       : in  std_logic_vector(31 downto 0);
        mac                         : in  std_logic_vector(47 downto 0);
        destIP_retrieved            : in  std_logic_vector(31 downto 0);
        srcIP_retrieved             : in  std_logic_vector(31 downto 0);
        mac_retrieved               : in  std_logic_vector(47 downto 0);
        start                       : in  std_logic;
        done                        : out std_logic;
        destIP_address              : in  std_logic_vector(7 downto 0);
        srcIP_address               : in  std_logic_vector(7 downto 0);
        mac_address                 : in  std_logic_vector(7 downto 0);
        i2c_address_to_write        : out std_logic_vector(7 downto 0);
        i2c_byte_to_write           : out std_logic_vector(7 downto 0);
        i2c_start_sig               : out std_logic;
        i2c_done_sig                : in  std_logic;
        i2c_read_writeN_ctrl_flag   : out std_logic;
        i2c_control_request         : out std_logic;
        read_fsm_done               : in  std_logic;
        read_fsm_start              : out std_logic
    );
    end component;
    
    component mux_2to1_Nbit
        generic (N : integer);
        port ( 
            sel : in  std_logic;
            A   : in  std_logic_vector(N-1 downto 0);
            B   : in  std_logic_vector(N-1 downto 0);
            X   : out std_logic_vector(N-1 downto 0)
        );
    end component;
	
	signal i2cDone                      : std_logic := '0';
	signal i2cDataOut                   : std_logic_vector(7 downto 0);
    signal i2cDataIn                    : std_logic_vector(7 downto 0);
    signal i2cStart                     : std_logic;
    signal i2cReadWriteNflag            : std_logic;
    signal i2cAddressIn                 : std_logic_vector(7 downto 0);
    
    signal i2cStart_write_fsm           : std_logic;
    signal i2cStart_read_fsm            : std_logic;
    signal i2cReadWriteNflag_write_fsm  : std_logic;
    signal i2cReadWriteNflag_read_fsm   : std_logic;
    signal i2cAdressIn_write_fsm        : std_logic_vector(7 downto 0);
    signal i2cAdressIn_read_fsm         : std_logic_vector(7 downto 0);
    
    signal read_FSM_start               : std_logic;
    signal read_FSM_done                : std_logic;
    
    signal write_FSM_start              : std_logic;
    signal write_FSM_done               : std_logic;
    
    signal i2c_fsm_select               : std_logic;
    
    signal write_fsm_signal_read        : std_logic;
    
    signal destIP_retrieved             : std_logic_vector(31 downto 0);
    signal srcIP_retrieved              : std_logic_vector(31 downto 0);
    signal mac_retrieved                : std_logic_vector(47 downto 0);
    
    signal read_request                 : std_logic;
    signal write_request                : std_logic;
    
    signal init_read_s                  : std_logic := '0';
    signal new_IP_ready_s               : std_logic := '0';
    
    signal done                         : std_logic := '0';
    signal module_busy_int              : std_logic := '1';
    
    constant destIP_eeprom_addr         : std_logic_vector(7 downto 0) := x"00";
    constant srcIP_eeprom_addr          : std_logic_vector(7 downto 0) := x"04";
    constant mac_eeprom_addr            : std_logic_vector(7 downto 0) := x"08";
    
    constant srcIP_expected_val         : std_logic_vector(23 downto 0) := x"C0A800";
    
    signal i2cWrap_initRead             : std_logic := '0';
    signal wr_accept_pf                 : std_logic := '0';
    signal i2cWrap_initRead_cnt         : unsigned(20 downto 0) := (others => '0');
    
    signal destIP_s                     : std_logic_vector(31 downto 0) := (others => '0');
    signal srcIP_s                      : std_logic_vector(31 downto 0) := (others => '0');
    signal mac_s                        : std_logic_vector(47 downto 0) := (others => '0');      
   
    signal destIP_returned_i            : std_logic_vector(31 downto 0) := (others => '0');
    signal srcIP_returned_i             : std_logic_vector(31 downto 0) := (others => '0');
    signal mac_returned_i               : std_logic_vector(47 downto 0) := (others => '0');
    
    -- Debugging
    signal probe0_out                   : std_logic_vector(127 downto 0);
    signal i2cClockOut                  : std_logic;
    
    signal SCL_internal                 : std_logic;


begin
 
    read_FSM_start  <= init_read_s or write_fsm_signal_read;
    write_FSM_start <= new_IP_ready_s;   
    module_busy_int <= not done;
    
    cdcc_input_unit: CDCC
    generic map (NUMBER_OF_BITS => 114)
    port map(
        clk_src                     => CLK_40_MHz,
        clk_dst                     => CLK_50_MHz,
        data_in(31 downto 0)        => destIP,
        data_in(63 downto 32)       => srcIP,
        data_in(111 downto 64)      => mac,
        data_in(112)                => i2cWrap_initRead,
        data_in(113)                => new_IP_ready,
        data_out_s(31 downto 0)     => destIP_s,
        data_out_s(63 downto 32)    => srcIP_s,
        data_out_s(111 downto 64)   => mac_s,
        data_out_s(112)             => init_read_s,
        data_out_s(113)             => new_IP_ready_s
    );
    
    cdcc_output_unit: CDCC
    generic map (NUMBER_OF_BITS => 1)
    port map(
        clk_src         => CLK_50_MHz,
        clk_dst         => CLK_40_MHz,
        data_in(0)      => module_busy_int,
        data_out_s(0)   => module_busy
    );
    
    cdcc_ip_unit: CDCC
    generic map (NUMBER_OF_BITS => 112)
    port map(
        clk_src                     => CLK_50_MHz,
        clk_dst                     => CLK_125_MHz,
        data_in(31 downto 0)        => destIP_returned_i,
        data_in(63 downto 32)       => srcIP_returned_i,
        data_in(111 downto 64)      => mac_returned_i,
        data_out_s(31 downto 0)     => destIP_returned,
        data_out_s(63 downto 32)    => srcIP_returned,
        data_out_s(111 downto 64)   => mac_returned
    );

    SCL <= SCL_internal;

    I2C_module: I2C_controller
    Port MAP(
        CLK_50_MHz  => CLK_50_MHz,
        ResetN      => '1',
        SCL         => SCL_internal,
        SDA         => SDA,
        Read_WriteN => i2cReadWriteNflag,
        Start       => i2cStart,
        Done        => i2cDone,
        DataIn      => i2cDataIn,
        AddressIn   => i2cAddressIn,
        DataOut     => i2cDataOut,
        i2cClockOut => i2cClockOut
    );
    
    I2C_read_fsm: FSM_I2C_read
    Port MAP(
        CLK_50_MHz                  => CLK_50_MHz,
        start                       => read_FSM_start,
        done                        => read_FSM_done,
        destIP_address              => destIP_eeprom_addr,
        srcIP_address               => srcIP_eeprom_addr,
        mac_address                 => mac_eeprom_addr,
        destIP_out                  => destIP_returned_i,
        srcIP_out                   => srcIP_returned_i,
        mac_out                     => mac_returned_i,
        destIP_retr                 => destIP_retrieved,
        srcIP_retr                  => srcIP_retrieved,
        mac_retr                    => mac_retrieved,
        i2c_byte_retrieved          => i2cDataOut,
        i2c_address_to_read         => i2cAdressIn_read_fsm,
        i2c_start_sig               => i2cStart_read_fsm,
        i2c_done_sig                => i2cDone,
        i2c_read_writeN_ctrl_flag   => i2cReadWriteNflag_read_fsm,
        destIP_default_val          => destIP_default_val,
        srcIP_default_val           => srcIP_default_val,
        mac_default_val             => mac_default_val,
        srcIP_expected_val          => srcIP_expected_val
    );
    
    I2C_write_fsm: FSM_I2C_write
    Port map(
        CLK_50_MHz                  => CLK_50_MHz,
        destIP_retrieved            => destIP_retrieved,
        srcIP_retrieved             => srcIP_retrieved,
        mac_retrieved               => mac_retrieved,
        start                       => write_FSM_start,
        done                        => write_FSM_done,
        destIP_address              => destIP_eeprom_addr,
        srcIP_address               => srcIP_eeprom_addr,
        mac_address                 => mac_eeprom_addr,
        destIP                      => destIP_s,
        srcIP                       => srcIP_s,
        mac                         => mac_s,
        i2c_address_to_write        => i2cAdressIn_write_fsm,
        i2c_byte_to_write           => i2cDataIn,
        i2c_control_request         => i2c_fsm_select,
        i2c_start_sig               => i2cStart_write_fsm,
        i2c_done_sig                => i2cDone,
        i2c_read_writeN_ctrl_flag   => i2cReadWriteNflag_write_fsm,
        read_fsm_done               => read_fsm_done,
        read_fsm_start              => write_fsm_signal_read
    );
    
    i2c_mux: mux_2to1_Nbit
    generic map (N => 10)
    port map(
        sel             => i2c_fsm_select,
        A(9)            => i2cReadWriteNflag_write_fsm,
        A(8)            => i2cStart_write_fsm,
        A(7 downto 0)   => i2cAdressIn_write_fsm,
        B(9)            => i2cReadWriteNflag_read_fsm,
        B(8)            => i2cStart_read_fsm,
        B(7 downto 0)   => i2cAdressIn_read_fsm,
        X(9)            => i2cReadWriteNflag,
        X(8)            => i2cStart,
        X(7 downto 0)   => i2cAddressIn
    );
    
    control_signals_proc: process(CLK_50_MHz)
    begin
        if CLK_50_MHz'EVENT and CLK_50_MHz = '1' then
        
            if(init_read_s = '1') then
                read_request    <= '1';
                done            <= '0';
            elsif(new_IP_ready_s = '1') then
                write_request   <= '1';
                done            <= '0';
            end if;
            
            if((read_FSM_done = '1' and read_request = '1') or (write_FSM_done = '1' and write_request = '1')) then
                done            <= '1';
                read_request    <= '0';
                write_request   <= '0';
            end if;
                        
        end if;
    end process;
    
i2c_init_read_process: process(CLK_40_MHz)
begin
    if rising_edge(CLK_40_MHz) then
        if(i2cWrap_initRead_cnt > "111111111111111110111" and i2cWrap_initRead_cnt < "111111111111111111111") then
            i2cWrap_initRead_cnt   <= i2cWrap_initRead_cnt + 1;
            i2cWrap_initRead       <= '1';
        elsif(i2cWrap_initRead_cnt = "111111111111111111111") then
            i2cWrap_initRead_cnt   <= i2cWrap_initRead_cnt;
            i2cWrap_initRead       <= '0';           
        else
            i2cWrap_initRead_cnt   <= i2cWrap_initRead_cnt + 1;
            i2cWrap_initRead       <= '0';
        end if;
    end if;
end process;

end Logic;
