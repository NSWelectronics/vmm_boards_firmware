-----------------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 18.08.2018
-- Design Name: CKBC gater
-- Module Name: cbkc_gater - RTL
-- Project Name: MMFE8 - NTUA-BNL_VMM_firmware
-- Target Devices: Artix7 xc7a200t-2fbg484 and xc7a200t-3fbg484
-- Tool Versions: Vivado 2018.2
-- Description: Module that enables the CKBC only upon reception of a trigger 
-- for a configurable amount of pulses.
--
-- Dependencies: MMFE8 NTUA/BNL Project
-- 
-- Changelog:
-- 06.09.2018 Add variable CKBC frequency support. (Christos Bakalis)
--
-----------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ckbc_gater is
    generic(is_level0 : std_logic := '0');
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_40          : in  std_logic;
        clk_320         : in  std_logic;
        trg_en          : in  std_logic;
        ro_mode         : in  std_logic;
        ckbc_enable     : in  std_logic;
        mmcm_locked     : in  std_logic;
        ckbc_freq       : in  std_logic_vector(5 downto 0);
        ------------------------------------
        ------- Trigger Interface ----------
        req_ckbc        : in  std_logic; -- must be synced to 40
        ckbc_max_num    : in  std_logic_vector(7 downto 0);
        ------------------------------------
        ----- CKBC ODDR Interface ----------
        ckbc_rst        : out std_logic
    );
end ckbc_gater;

architecture RTL of ckbc_gater is
    
    -- if align_thr = "001" ->
    -- reset changes state 9.375ns before CKBC rising edge, and 3.125ns after CKBC falling edge
    constant align_thr          : std_logic_vector(2 downto 0) := "001";

    signal start_align          : std_logic := '0';
    signal ckbc_enable_s40      : std_logic := '0';
    signal req_ckbc_s320        : std_logic := '0';
    signal start_align_s320     : std_logic := '0';
    signal ckbc_enable_s320     : std_logic := '0';
    signal rst_roMode           : std_logic := '0';
    signal clk_aligned          : std_logic := '0';
    signal rst_freq             : std_logic := '0';    
    signal rst_freq_fsm         : std_logic := '0';
    signal rst_roMode_fsm       : std_logic := '0';
    signal trg_en_s40           : std_logic := '0';
    signal trg_en_s320          : std_logic := '0';
    signal ro_mode_s40          : std_logic := '0';
    signal ro_mode_s320         : std_logic := '0';
    signal freq_cnt_max_s320_i  : std_logic_vector(1 downto 0) := (others => '0');
    signal freq_cnt_max_s320_s  : std_logic_vector(1 downto 0) := (others => '0');
    signal freq_cnt_max         : std_logic_vector(1 downto 0) := (others => '0');
    signal freq_cnt             : unsigned(1 downto 0) := (others => '0');
    signal align_cnt            : unsigned(2 downto 0) := (others => '0');
    signal limit_cnt            : unsigned(7 downto 0) := (others => '0');

    type stateType is (ST_INIT, ST_IDLE, ST_WAIT_CNT, ST_SEND);
    signal state    : stateType := ST_INIT;

    type stateTypeFreq is (ST_REL_CKBC, ST_GND_CKBC);
    signal state_freq  : stateTypeFreq := ST_REL_CKBC;

begin

-- for the frequency gater
ckbc_freq_proc: process(ckbc_freq)
begin
    case ckbc_freq is
    when "001010" => -- 10 Mhz
        freq_cnt_max <= "11";
    when "010100" => -- 20 Mhz
        freq_cnt_max <= "01";
    when "101000" => -- 40 Mhz
        freq_cnt_max <= "00";
    when others => 
        freq_cnt_max <= "00";
    end case;
end process;

-- trigger align process
TrAlign_ckbc_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        start_align <= mmcm_locked;
    end if;
end process;

-- local registering process
local_reg_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        ckbc_enable_s40 <= ckbc_enable;
        ro_mode_s40     <= ro_mode;
        trg_en_s40      <= trg_en;
    end if;
end process;

-- cdc/pipeline and reset manager
cdc_rst_proc: process(clk_320)
begin
    if(rising_edge(clk_320))then
        start_align_s320    <= start_align;
        ckbc_enable_s320    <= ckbc_enable_s40;
        req_ckbc_s320       <= req_ckbc;
        ro_mode_s320        <= ro_mode_s40;
        trg_en_s320         <= trg_en_s40;
        freq_cnt_max_s320_i <= freq_cnt_max;
        freq_cnt_max_s320_s <= freq_cnt_max_s320_i;
        ckbc_rst            <= rst_freq or rst_roMode;

        if(start_align_s320 = '0' or ro_mode_s320 = '0' or trg_en_s320 = '0' or freq_cnt_max /= "00")then
            rst_roMode_fsm <= '1';
        else
            rst_roMode_fsm <= '0';
        end if;

        if(freq_cnt_max_s320_s /= freq_cnt_max_s320_i)then
            rst_freq_fsm <= '1';
        elsif(freq_cnt_max_s320_s = "00")then
            rst_freq_fsm <= '1';
        else
            rst_freq_fsm <= '0';
        end if;

    end if;
end process;

-- align process
align_ckbc_proc: process(clk_320)
begin
    if(rising_edge(clk_320))then
        if(start_align_s320 = '1')then
            align_cnt <= align_cnt + 1;
        else
            align_cnt <= (others => '0');
        end if;

        if(align_cnt = unsigned(align_thr))then
            clk_aligned <= '1';
        else
            clk_aligned <= '0';
        end if;
    end if;
end process;

-- main CKBC freq FSM
ckbcFreq_FSM_proc: process(clk_320)
begin
    if(rising_edge(clk_320))then
        if(rst_freq_fsm = '1')then
            rst_freq    <= not ckbc_enable_s320;
            freq_cnt    <= (others => '0');
            state_freq  <= ST_REL_CKBC;
        else
            case state_freq is

            -- which part of the waverform are we in?
            when ST_REL_CKBC =>
                if(clk_aligned = '1')then
                    rst_freq    <= '1';
                    state_freq  <= ST_GND_CKBC;
                else
                    rst_freq    <= not ckbc_enable_s320;
                    state_freq  <= ST_REL_CKBC;
                end if;

            -- what is the frequency?
            when ST_GND_CKBC =>
                if(clk_aligned = '1' and freq_cnt + 1 < unsigned(freq_cnt_max_s320_s))then
                    rst_freq    <= '1';
                    freq_cnt    <= freq_cnt + 1;
                    state_freq  <= ST_GND_CKBC;
                elsif(clk_aligned = '1' and freq_cnt + 1 = unsigned(freq_cnt_max_s320_s))then
                    rst_freq    <= not ckbc_enable_s320;
                    freq_cnt    <= (others => '0');
                    state_freq  <= ST_REL_CKBC;
                else
                    rst_freq    <= '1';
                    freq_cnt    <= freq_cnt;
                    state_freq  <= ST_GND_CKBC;
                end if;

            when others => state_freq <= ST_REL_CKBC;
            end case;
        end if;

    end if;
end process;

roModeFSM_generate : if (is_level0 = '0') generate

-- FSM that decides how many CKBCs to send if in readout_mode (finite CKBCs)
FSM_proc: process(clk_320)
begin
    if(rising_edge(clk_320))then
        if(rst_roMode_fsm = '1')then
            rst_roMode  <= not ckbc_enable_s320;
            limit_cnt   <= (others => '0');
            state       <= ST_INIT;

        else
            case state is

            -- first reset in the center of the ckbc low state
            when ST_INIT =>
                if(clk_aligned = '1')then
                    rst_roMode  <= '1';
                    state       <= ST_IDLE;
                else
                    rst_roMode  <= not ckbc_enable_s320;
                    state       <= ST_INIT;
                end if;
            
            -- wait for trigger
            when ST_IDLE =>
                if(req_ckbc_s320 = '1')then
                    state <= ST_WAIT_CNT;
                else
                    state <= ST_IDLE;
                end if;

            -- align
            when ST_WAIT_CNT =>
                if(clk_aligned = '1')then -- aligned with the center of the low state of ckbc
                    rst_roMode  <= '0';
                    limit_cnt   <= limit_cnt + 1;
                    state       <= ST_SEND;
                else
                    rst_roMode  <= rst_roMode;
                    limit_cnt   <= limit_cnt;
                    state       <= ST_WAIT_CNT;
                end if;

            -- send the pulse(s)
            when ST_SEND =>
                if(limit_cnt < unsigned(ckbc_max_num) and clk_aligned = '1')then
                    rst_roMode  <= '0';
                    limit_cnt   <= limit_cnt + 1;
                    state       <= ST_SEND;
                elsif(limit_cnt = unsigned(ckbc_max_num) and clk_aligned = '1')then
                    rst_roMode  <= '1';
                    limit_cnt   <= (others => '0');
                    state       <= ST_IDLE;
                else
                    rst_roMode  <= '0';
                    limit_cnt   <= limit_cnt;
                    state       <= ST_SEND;
                end if;

            when others => state <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

end generate roModeFSM_generate;

l0GndReg_generate : if (is_level0 = '1') generate
    rst_roMode  <= '0';
end generate l0GndReg_generate;

end RTL;
