-----------------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 05.08.2018
-- Design Name: Flow FSM
-- Module Name: flow_fsm - RTL
-- Project Name: MMFE8 - NTUA-BNL_VMM_firmware
-- Target Devices: Artix7 xc7a200t-2fbg484 and xc7a200t-3fbg484
-- Tool Versions: Vivado 2018.2
-- Description: Central FSM of the design

-- Dependencies: MMFE8 NTUA/BNL Project
-- 
-- Changelog:
--
-----------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity flow_fsm is
    generic(is_level0   : std_logic := '0';
            is_vmm3a    : std_logic := '0';
            use_extSR   : std_logic := '0';
            use_inhib   : std_logic := '0');
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_40          : in  std_logic;
        rst             : in  std_logic;
        daq_inhib       : in  std_logic;
        trg_mode        : in  std_logic;
        analog_mode     : in  std_logic;
        ext_sr          : in  std_logic;
        per_sr          : in  std_logic;
        mmcm_locked     : in  std_logic;
        st_flow_dbg     : out std_logic_vector(4 downto 0);
        ------------------------------------
        ----- UDP din/dout Interface -------
        vmm_bitmask     : in  std_logic_vector(7 downto 0);
        vmm_conf        : in  std_logic;
        vmm_confRdy     : in  std_logic;
        vmm_confEnd     : in  std_logic;
        ip_conf         : in  std_logic;
        xadc_conf       : in  std_logic;
        daq_on          : in  std_logic;
        cs_udp          : in  std_logic;
        sck_udp         : in  std_logic;
        sdi_udp         : in  std_logic;
        reply_done      : in  std_logic;
        udp_done        : in  std_logic;
        reply_enable    : out std_logic;
        sel_reply       : out std_logic;
        vmm_confEnable  : out std_logic;
        ------------------------------------
        -------- Submodules Interface ------
        xadc_busy       : in  std_logic;
        ip_busy         : in  std_logic;
        tki_analog      : in  std_logic;
        vmm_ena_analog  : in  std_logic;
        trg_ext         : in  std_logic;
        cktp_enable     : out std_logic;
        cktp_primary    : out std_logic;
        trg_enable      : out std_logic;
        daq_enable      : out std_logic;
        analog_enable   : out std_logic;
        xadc_start      : out std_logic;
        ip_start        : out std_logic;
        rst_readout     : out std_logic;
        inhib_conf      : out std_logic;   
        ------------------------------------
        ------------ VMM Interface ---------
        ckbc_enable     : out std_logic;
        vmm_ena         : out std_logic_vector(7 downto 0);
        vmm_cs          : out std_logic_vector(7 downto 0);
        vmm_sdi         : out std_logic_vector(7 downto 0);
        vmm_sck         : out std_logic_vector(7 downto 0);
        vmm_tki         : out std_logic
     );
end flow_fsm;

architecture RTL of flow_fsm is

    -- flow fsm signals
    signal daq_enable_i     : std_logic := '0';
    signal sel_vmm_conf     : std_logic := '0';
    signal analog_enable_i  : std_logic := '0';
    signal int_sr           : std_logic := '0';
    signal tki_flow         : std_logic := '0';
    signal ckbc_enable_i    : std_logic := '0';
    signal daq_inhib_f      : std_logic := '0';
    signal cktp_enable_i    : std_logic := '0';
    signal bitmask_conf     : std_logic_vector(7 downto 0)  := (others => '0');
    signal wait_cnt         : unsigned(6 downto 0)          := (others => '0');

    -- reset manager signals
    signal vmm_ena_rst      : std_logic := '0';
    signal vmm_tki_rst      : std_logic := '0';
    signal rst_busy_i       : std_logic := '0';
    signal rst_busy         : std_logic := '0';
    signal wait_cnt_rst     : unsigned(2 downto 0) := (others => '0');
    
    -- other
    signal vmm_cs_rst       : std_logic := '0';

    -- state_flow signals
    type state_rstType is (ST_IDLE, ST_ENA_LOW, ST_TKI_HIGH);
    signal state_rst    : state_rstType := ST_IDLE;

    type state_flowType is (ST_IDLE, ST_VMM_CONF_START, ST_VMM_CONF, ST_VMM_CONF_END, ST_CONF_REPLY, ST_XADC_INIT, ST_XADC_WAIT, ST_FLASH_INIT,
                            ST_FLASH_WAIT, ST_WAIT_RST_BUSY, ST_WAIT_RST_DONE, ST_DAQ_SYNC, ST_DAQ_CKBC, ST_DAQ_INIT, ST_DAQ_TRG, ST_DAQ, ST_DAQ_OFF);
    signal state_flow   : state_flowType := ST_IDLE;
                     
    attribute FSM_ENCODING                  : string;
    attribute FSM_ENCODING of state_rst     : signal is "ONE_HOT";
    attribute FSM_ENCODING of state_flow    : signal is "ONE_HOT";

begin

-- flow FSM
flowFSM_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        if(rst = '1' or mmcm_locked = '0')then
            st_flow_dbg     <= "11111";
            wait_cnt        <= (others => '0');
            cktp_enable_i   <= '0';
            cktp_primary    <= '0';
            trg_enable      <= '0';
            daq_enable_i    <= '0';
            xadc_start      <= '0';
            ip_start        <= '0';
            vmm_confEnable  <= '0';
            analog_enable_i <= '0';
            reply_enable    <= '0';
            sel_reply       <= '0';
            sel_vmm_conf    <= '0';
            tki_flow        <= '0';
            int_sr          <= '0';
            inhib_conf      <= '0';
            ckbc_enable_i   <= '1';
            rst_readout     <= '1';
            state_flow      <= ST_IDLE;
        else
            case state_flow is

            -- what to do
            when ST_IDLE =>
                st_flow_dbg     <= "00000";
                wait_cnt        <= (others => '0');
                cktp_enable_i   <= '0';
                cktp_primary    <= '0';
                trg_enable      <= '0';
                daq_enable_i    <= '0';
                xadc_start      <= '0';
                ip_start        <= '0';
                vmm_confEnable  <= '0';
                analog_enable_i <= '0';
                reply_enable    <= '0';
                sel_reply       <= '0';
                sel_vmm_conf    <= '0';
                tki_flow        <= '0';
                ckbc_enable_i   <= '1';
                rst_readout     <= '1';
                

                if(vmm_conf = '1')then
                    int_sr      <= '0';
                    inhib_conf  <= '1';
                    state_flow  <= ST_VMM_CONF_START;
                elsif(xadc_conf = '1')then
                    int_sr      <= '0';
                    inhib_conf  <= '1';
                    state_flow  <= ST_XADC_INIT;
                elsif(ip_conf = '1')then
                    int_sr      <= '0';
                    inhib_conf  <= '1';
                    state_flow  <= ST_FLASH_INIT;
                elsif(daq_on = '1' and daq_inhib_f = '0' and use_extSR = '1')then
                    int_sr      <= '0'; -- no need to soft-reset, the user does it
                    inhib_conf  <= '1';
                    state_flow  <= ST_DAQ_SYNC;
                elsif(daq_on = '1' and daq_inhib_f = '0' and use_extSR = '0' and rst_busy = '0')then
                    int_sr      <= '1'; -- always soft-reset first
                    inhib_conf  <= '1';
                    state_flow  <= ST_WAIT_RST_BUSY;
                elsif(ext_sr = '1' and use_extSR = '1')then
                    int_sr      <= '0';
                    inhib_conf  <= '1';
                    state_flow  <= ST_WAIT_RST_BUSY;
                else
                    int_sr      <= '0';
                    inhib_conf  <= '0';
                    state_flow  <= ST_IDLE;
                end if;


            -- wait for the reset module to go busy
            when ST_WAIT_RST_BUSY =>
                st_flow_dbg <= "00001";
                int_sr      <= '0';

                if(rst_busy = '1')then
                    state_flow <= ST_WAIT_RST_DONE;
                else
                    state_flow <= ST_WAIT_RST_BUSY;
                end if;

            -- wait for the reset module to finish
            when ST_WAIT_RST_DONE =>
                st_flow_dbg <= "00010";
                
                if(rst_busy = '0' and daq_on = '1')then -- reset done, go to DAQ
                    state_flow <= ST_DAQ_SYNC;
                elsif(rst_busy = '0' and daq_on = '0')then -- reset done
                    state_flow <= ST_CONF_REPLY;
                else
                    state_flow <= ST_WAIT_RST_DONE;
                end if;

            -- sync the boards (if there are more)
            when ST_DAQ_SYNC =>
                st_flow_dbg <= "00011";
            
                if(daq_on = '0')then
                    state_flow  <= ST_IDLE; -- abort
                elsif(trg_mode = '0')then
                    state_flow  <= ST_DAQ_CKBC; -- no need to sync
                else
                    if(use_extSR = '1' and trg_ext = '1')then
                        state_flow <= ST_DAQ_CKBC;  -- init all boards at the same time
                    elsif(use_extSR = '1' and trg_ext = '0')then
                        state_flow <= ST_DAQ_SYNC; -- stay
                    else
                        state_flow <= ST_DAQ_CKBC; -- if use_extSR is low, don't care to sync anyway
                    end if;
                end if;

            -- stop the CKBC for the startup sequence of VMM3
            when ST_DAQ_CKBC =>
                st_flow_dbg <= "00100";
            
                if(is_vmm3a = '1')then
                    ckbc_enable_i <= ckbc_enable_i;    
                else
                    ckbc_enable_i <= '0';
                end if;

                if(wait_cnt = "0000100" or is_vmm3a = '1')then
                    wait_cnt    <= (others => '0'); 
                    state_flow  <= ST_DAQ_INIT;
                else
                    wait_cnt    <= wait_cnt + 1;
                    state_flow  <= ST_DAQ_CKBC;
                end if;

            -- initiates the primary CKTP for the VMM3 start-up sequence
            when ST_DAQ_INIT =>
                st_flow_dbg <= "00101";
                
                if(is_vmm3a = '1')then
                    cktp_primary    <= '0';
                    state_flow      <= ST_DAQ_TRG;
                else
                    if(wait_cnt < "0100000")then
                        wait_cnt        <= wait_cnt + 1;
                        cktp_primary    <= '1';
                        state_flow      <= ST_DAQ_INIT;
                    elsif(wait_cnt >= "0100000" and wait_cnt < "0100100")then
                        wait_cnt        <= wait_cnt + 1;
                        cktp_primary    <= '0';
                        state_flow      <= ST_DAQ_INIT;   
                    else
                        wait_cnt        <= (others => '0');
                        cktp_primary    <= '0';
                        state_flow      <= ST_DAQ_TRG;   
                    end if;
                end if;

            -- enable trigger acceptance
            when ST_DAQ_TRG =>
                st_flow_dbg     <= "00110";
                rst_readout     <= '0';
                trg_enable      <= '1';
                ckbc_enable_i   <= '1';
                state_flow      <= ST_DAQ;

                if(is_level0 = '0' and analog_mode = '0')then
                    tki_flow <= '1';
                else
                    tki_flow <= '0';
                end if;

                if(analog_mode = '0')then
                    daq_enable_i    <= '1';
                    analog_enable_i <= '0';
                else
                    daq_enable_i    <= '0';
                    analog_enable_i <= '1';
                end if;  

            -- wait for daq_off
            when ST_DAQ =>
                st_flow_dbg     <= "00111";
                cktp_enable_i   <= '1';

                if(daq_on = '0')then
                    state_flow  <= ST_DAQ_OFF;
                else
                    state_flow  <= ST_DAQ;
                end if;
                
            -- inhibit triggers+CKTP and wait for FIFOs to empty
            when ST_DAQ_OFF =>
                cktp_enable_i   <= '0';
                trg_enable      <= '0';
                st_flow_dbg     <= "01000";
                
                if(udp_done = '1')then
                    state_flow  <= ST_IDLE;
                else
                    state_flow  <= ST_DAQ_OFF;
                end if;

            -- wait for the udp block to be ready to send VMM conf bits
            when ST_VMM_CONF_START =>
                st_flow_dbg <= "01001";
                
                if(vmm_confRdy = '1')then
                    sel_vmm_conf    <= '1';
                    bitmask_conf    <= vmm_bitmask;
                    state_flow      <= ST_VMM_CONF;
                else
                    sel_vmm_conf    <= '0';
                    bitmask_conf    <= (others => '0');
                    state_flow      <= ST_VMM_CONF_START;
                end if;

            -- wait for it to finish sampling the packet
            when ST_VMM_CONF =>
                st_flow_dbg     <= "01010";
                vmm_confEnable  <= '1';
                sel_reply       <= '1';

                if(vmm_confEnd = '1')then
                    state_flow  <= ST_CONF_REPLY;
                else
                    state_flow  <= ST_VMM_CONF;
                end if;

            -- send conf reply
            when ST_CONF_REPLY =>
                st_flow_dbg     <= "01011";
                vmm_confEnable  <= '0';
                sel_vmm_conf    <= '0';
                reply_enable    <= '1';

                if(reply_done = '1')then
                    state_flow <= ST_IDLE;
                else
                    state_flow <= ST_CONF_REPLY;
                end if;

            -- start the xADC
            when ST_XADC_INIT =>
                st_flow_dbg     <= "01100";
                
                if(xadc_busy = '1')then
                    xadc_start  <= '0';
                    state_flow  <= ST_XADC_WAIT;
                else
                    xadc_start  <= '1';
                    state_flow  <= ST_XADC_INIT; 
                end if;

            -- wait for xADC to finish and go to IDLE
            when ST_XADC_WAIT =>
                st_flow_dbg     <= "01101";
                cktp_enable_i   <= '1';
                if(xadc_busy = '0')then
                    state_flow <= ST_IDLE;
                else
                    state_flow <= ST_XADC_WAIT;
                end if;

            -- start the flash/IP configuration
            when ST_FLASH_INIT =>
                st_flow_dbg     <= "01110";
            
                if(ip_busy = '1')then
                    ip_start    <= '0';
                    state_flow  <= ST_FLASH_WAIT;
                else
                    ip_start    <= '1';
                    state_flow  <= ST_FLASH_INIT;
                end if;

              -- wait for AXI4SPI/I2C to finish and go to IDLE
            when ST_FLASH_WAIT =>
                st_flow_dbg     <= "01111";
                if(ip_busy = '0')then
                    state_flow <= ST_IDLE;
                else
                    state_flow <= ST_FLASH_WAIT;
                end if;

            when others => state_flow <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- reset manager FSM
resetManagerFSM_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then

        rst_busy <= rst_busy_i; -- delay for one cycle

        case state_rst is

        -- decides if to get into a VMM reset sequence
        when ST_IDLE =>
            vmm_ena_rst     <= '1';
            vmm_tki_rst     <= '0';
            wait_cnt_rst    <= (others => '0');

            if((ext_sr = '1' and use_extSR = '1') or ((int_sr = '1' or per_sr = '1') and use_extSR = '0'))then
                rst_busy_i  <= '1';
                state_rst   <= ST_ENA_LOW;
            else
                rst_busy_i  <= '0';
                state_rst   <= ST_IDLE;
            end if;

        -- ground the enable signal
        when ST_ENA_LOW =>
            vmm_ena_rst     <= '0';
            vmm_tki_rst     <= '0';
            rst_busy_i      <= rst_busy_i;
            wait_cnt_rst    <= (others => '0');

            if(daq_enable_i = '1')then -- dont do ocr/bcr while taking data
                state_rst <= ST_IDLE; 
            elsif(daq_enable_i = '0' and is_level0 = '1')then -- proceed with ocr/bcr if at level0
                state_rst <= ST_TKI_HIGH;
            else
                state_rst <= ST_IDLE;
            end if;

        -- double bcr = ocr
        when ST_TKI_HIGH =>
            vmm_ena_rst     <= '1';
            rst_busy_i      <= rst_busy_i;
            wait_cnt_rst    <= wait_cnt_rst + 1;
            
            if(wait_cnt_rst = "110")then
                vmm_tki_rst <= '1';
                state_rst   <= ST_TKI_HIGH;
            elsif(wait_cnt_rst = "111")then
                vmm_tki_rst <= '1';
                state_rst   <= ST_IDLE;
            else
                vmm_tki_rst <= '0';
                state_rst   <= ST_TKI_HIGH;
            end if;

        when others => state_rst <= ST_IDLE;
        end case;
    end if;
end process;

-- CS control process for reset
csCtrl_proc: process(vmm_ena_rst)
begin
    if(is_vmm3a = '1')then
        vmm_cs_rst <= '1';     
    else
        vmm_cs_rst <= vmm_ena_rst;
    end if;
end process;

-- VMM signals multiplexer
gen_vmm_sigs: for I in 0 to 7 generate
muxVMM_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then

        -- ena
        case sel_vmm_conf is
        when '0'    => vmm_ena(I) <= (vmm_ena_rst xor analog_enable_i) or vmm_ena_analog; -- fan it out
        when '1'    => vmm_ena(I) <= '0' or (not bitmask_conf(I));
        when others => vmm_ena(I) <= '1';
        end case;

        -- cs
        case sel_vmm_conf is
        when '0'    => vmm_cs(I) <= vmm_cs_rst; -- fan it out
        when '1'    => vmm_cs(I) <= cs_udp or (not bitmask_conf(I));
        when others => vmm_cs(I) <= '1';
        end case;

        -- sck, sdi
        case sel_vmm_conf is
        when '0'    => vmm_sck(I) <= '0'; vmm_sdi(I) <= '0';
        when '1'    => vmm_sck(I) <= sck_udp and bitmask_conf(I);
                       vmm_sdi(I) <= sdi_udp and bitmask_conf(I); 
        when others => vmm_sck(I) <= '0'; vmm_sdi(I) <= '0';
        end case;

        -- tki
        if(daq_enable_i = '1' and analog_enable_i = '1')then
            vmm_tki <= tki_analog;
        elsif(is_level0 = '0' and analog_mode = '0')then
            vmm_tki <= tki_flow;
        else
            vmm_tki <= vmm_tki_rst;
        end if;

    end if;
end process;
end generate gen_vmm_sigs;


    analog_enable       <= analog_enable_i;
    daq_enable          <= daq_enable_i;
    ckbc_enable         <= ckbc_enable_i;
    daq_inhib_f         <= daq_inhib and use_inhib;
    cktp_enable         <= cktp_enable_i and (not trg_mode);

end RTL;
