----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
--   
-- Create Date: 28.04.2017 14:18:44
-- Design Name: Two-Phase Analog VMM Readout
-- Module Name: twoPhaseAnalog_readout - RTL
-- Project Name: NTUA-BNL VMM3 Readout Firmware
-- Target Devices: Xilinx xc7a200t-2fbg484
-- Tool Versions: Vivado 2016.4
-- Description: Asserts the necessary signals for two-phase analog readout mode
-- 
-- Dependencies: 
-- 
-- Changelog: 
-- 29.03.2018 Changes in FSM to adapt to VMM3 datasheet. (Christos Bakalis)
--
----------------------------------------------------------------------------------
library UNISIM;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use UNISIM.VComponents.all;

entity twoPhaseAnalog_readout is
    generic(cktkPeriod : integer := 160_000); -- 160_000_000 for 1Hz, 160_000 for 1Khz, 16_000 for 10 Khz, 1600 for 100 Khz 
    port(
        -------------------------
        --- General Interface ---
        clk_160             : in  std_logic; -- core logic clock
        clk_125             : in  std_logic; -- for FIFO2UDP interfacing
        analog_on           : in  std_logic; -- in acquisition mode (125)
        trigger_pulse       : in  std_logic; -- internal trigger (125)
        fifo_init           : in  std_logic; -- initialize the fifo 
        analog_busy         : out std_logic; -- busy (125)
        -------------------------
        ---- xADC Interface -----
        xadc_busy           : in  std_logic; -- xADC is reading (125)
        xadc_start          : out std_logic; -- activate xADC (125)
        xadc_read_tdo       : out std_logic; -- read TDO on xadc
        -------------------------
        -- FIFO2UDP Interface ---
        UDPDone             : in  std_logic; -- (125)
        wr_en               : out std_logic;
        packLen             : out std_logic_vector(11 downto 0);
        end_packet          : out std_logic;
        dout                : out std_logic_vector(15 downto 0);
        -------------------------
        ---- VMM Interface ------
        vmm_ena             : out std_logic; 
        vmm_tki             : out std_logic;
        vmm_ckdt            : out std_logic;
        vmm_cktk            : out std_logic;
        vmm_data0           : in  std_logic;
        vmm_data1           : in  std_logic
    );
end twoPhaseAnalog_readout;

architecture RTL of twoPhaseAnalog_readout is

component ila_overview
    port(
        clk     : in std_logic;
        probe0  : in std_logic_vector(63 downto 0)
    );
end component;

component CDCC
    generic(
        NUMBER_OF_BITS : integer := 8);
    port(
        clk_src     : in  std_logic;
        clk_dst     : in  std_logic;
        data_in     : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0);
        data_out_s  : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0)
    );
end component;

component channel_buffer
    port (
        wr_clk      : in  std_logic;
        rd_clk      : in  std_logic;
        rst         : in  std_logic;
        din         : in  std_logic_vector(15 downto 0);
        wr_en       : in  std_logic;
        rd_en       : in  std_logic;
        dout        : out std_logic_vector(15 downto 0);
        full        : out std_logic;
        empty       : out std_logic;
        wr_rst_busy : out std_logic;
        rd_rst_busy : out std_logic
    );
end component;

    -- main FSM
    signal to_cnt_ena       : std_logic := '0';
    signal token_ack        : std_logic := '0';
    signal vmm_ckdt_i       : std_logic := '0';
    signal vmm_ena_i        : std_logic := '1';
    signal token_enable     : std_logic := '0';
    signal token_sent       : std_logic := '0';
    signal rst_vmm          : std_logic := '0';    
    signal flag_addr        : std_logic_vector(7 downto 0)  := (others => '0');
    signal index            : unsigned(2 downto 0)          := (others => '0');
    signal wait_cnt         : unsigned(1 downto 0)          := (others => '0');
    signal anlg_state       : std_logic_vector(3 downto 0)  := (others => '0');
    signal wr_en_i          : std_logic := '0';
    signal read_tdo         : std_logic := '0';
    signal dout_i           : std_logic_vector(15 downto 0) := (others => '0');

    -- token FSM
    signal token_aux_cnt    : unsigned(6 downto 0)          := (others => '0');
    signal cktkPeriod_cnt   : unsigned(27 downto 0)         := (others => '0');
    signal vmm_tki_i        : std_logic := '0';
    signal first_pulse      : std_logic := '1';
    signal vmm_cktk_i       : std_logic := '0';
    signal reset_done       : std_logic := '0';
    signal token_state      : std_logic_vector(2 downto 0)  := (others => '0');
    signal cnt_cktk         : unsigned(1 downto 0)          := (others => '0');

    -- other
    signal vmm_data0_i      : std_logic := '0';
    signal vmm_data0_s      : std_logic := '0';
    
    signal timeout_cnt      : unsigned(9 downto 0) := (others => '0');
    signal timeout          : std_logic := '0';
    signal rd_en_i          : std_logic := '0';
    signal fifo_full        : std_logic := '0';
    signal fifo_empty       : std_logic := '0';
    signal wrEn_sreg        : std_logic_vector(2 downto 0) := (others => '0');
    signal rdy_sreg         : std_logic_vector(4 downto 0) := (others => '0');
    signal packRdy_i        : std_logic := '0';
    signal wr_en_udp_i      : std_logic := '0';
    signal analog_on_s      : std_logic := '0';
    signal UDPDone_s        : std_logic := '0';
    signal xadc_busy_s      : std_logic := '0';
    signal trigg_s          : std_logic := '0';
    signal anlg_busy_i      : std_logic := '0';
    signal xadcStart_i      : std_logic := '0';
    signal xadc_start_delay : std_logic := '0';

    type stateMain is (ST_IDLE, ST_MONITOR_D0, ST_START_TOKEN, ST_READ_ADDR_0, ST_SEND_CHAN, ST_WAIT_UDP,
                       ST_READ_ADDR_1, ST_START_XADC, ST_WAIT_XADC, ST_WAIT_TOKEN_0, ST_WAIT_TOKEN_1, ST_RST_VMM);
    signal state      : stateMain := ST_IDLE;

    type statetoken is (ST_ASSERT_TOKEN, ST_WAIT_SENT, ST_WAIT_WR_EN, ST_WAIT_MAIN, ST_CNT_PERIOD, ST_SEND_3CKTK_0, ST_SEND_3CKTK_1); 
    signal state_token : statetoken := ST_ASSERT_TOKEN;
    
    attribute FSM_ENCODING                  : string;
    attribute FSM_ENCODING of state         : signal is "ONE_HOT";
    attribute FSM_ENCODING of state_token   : signal is "ONE_HOT";

    attribute ASYNC_REG                     : string;
    attribute ASYNC_REG of vmm_data0_i      : signal is "TRUE";
    attribute ASYNC_REG of vmm_data0_s      : signal is "TRUE";

    -- debugging
--    attribute mark_debug                  : string;
--    attribute mark_debug of vmm_data0_s   : signal is "TRUE";
--    attribute mark_debug of vmm_data1     : signal is "TRUE";
--    attribute mark_debug of anlg_busy_i   : signal is "TRUE";
--    attribute mark_debug of vmm_ena_i     : signal is "TRUE";
--    attribute mark_debug of to_cnt_ena    : signal is "TRUE";
--    attribute mark_debug of token_ack     : signal is "TRUE";
--    attribute mark_debug of wr_en_i       : signal is "TRUE";
--    attribute mark_debug of xadcStart_i   : signal is "TRUE";
--    attribute mark_debug of vmm_ckdt_i    : signal is "TRUE";
--    attribute mark_debug of token_enable  : signal is "TRUE";
--    attribute mark_debug of flag_addr     : signal is "TRUE";
--    attribute mark_debug of anlg_state    : signal is "TRUE";
--    attribute mark_debug of analog_on_s   : signal is "TRUE";
--    attribute mark_debug of trigg_s       : signal is "TRUE";
--    attribute mark_debug of xadc_busy_s   : signal is "TRUE";
--    attribute mark_debug of UDPDone_s     : signal is "TRUE";
--    attribute mark_debug of vmm_tki_i     : signal is "TRUE";
--    attribute mark_debug of token_sent    : signal is "TRUE";
--    attribute mark_debug of token_state   : signal is "TRUE";
--    attribute mark_debug of timeout       : signal is "TRUE";
--    attribute mark_debug of reset_done    : signal is "TRUE";
--    attribute mark_debug of vmm_cktk_i    : signal is "TRUE";
--    attribute mark_debug of first_pulse   : signal is "TRUE";
--    attribute mark_debug of rst_vmm       : signal is "TRUE";

begin

-- sync data0
sync_data0_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        vmm_data0_i <= vmm_data0;
        vmm_data0_s <= vmm_data0_i;
    end if;
end process;

-- main FSM of the analog readout process
analog_FSM_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        if(analog_on_s = '0')then
            anlg_busy_i     <= '0';
            vmm_ena_i       <= '1';
            to_cnt_ena      <= '0';
            token_ack       <= '0';
            wr_en_i         <= '0';
            xadcStart_i     <= '0';
            vmm_ckdt_i      <= '0';
            token_enable    <= '0';
            rst_vmm         <= '0';
            read_tdo        <= '0';
            flag_addr       <= (others => '0');
            index           <= (others => '0');
            wait_cnt        <= (others => '0');
            anlg_state      <= "1111";
            state           <= ST_IDLE;
        else
            case state is

            -- wait for trigger
            when ST_IDLE =>
                anlg_state      <= "0000";
                anlg_busy_i     <= '0';
                vmm_ena_i       <= '1';
                to_cnt_ena      <= '0';
                token_ack       <= '0';
                vmm_ckdt_i      <= '0';
                xadcStart_i     <= '0';
                wr_en_i         <= '0';
                read_tdo        <= '0';
                rst_vmm         <= '0';
                token_enable    <= '0';
                flag_addr       <= (others => '0');
                index           <= (others => '0');
                if(trigg_s = '1')then
                    state <= ST_MONITOR_D0;
                else
                    state <= ST_IDLE;
                end if;

            -- is D0 high?
            when ST_MONITOR_D0 =>
                anlg_state  <= "0001";
                anlg_busy_i <= '1';
                to_cnt_ena  <= '1';
                if(vmm_data0_s = '1')then
                    vmm_ena_i   <= '0';
                    state       <= ST_START_TOKEN; -- flag detected
                elsif(timeout = '1')then
                    vmm_ena_i   <= '1';
                    state       <= ST_IDLE; -- too late
                else
                    vmm_ena_i   <= '1';
                    state       <= ST_MONITOR_D0; -- wait
                end if;

            -- initiate token strobe
            when ST_START_TOKEN =>
                anlg_state  <= "0010";
                token_enable <= '1';
                token_ack    <= '0';
                vmm_ena_i   <= '0';
                to_cnt_ena  <= '0';
                if(token_sent = '1')then
                    vmm_ckdt_i  <= '1';
                    state       <= ST_READ_ADDR_0; 
                else
                    vmm_ckdt_i  <= '0';
                    state       <= ST_START_TOKEN;
                end if;

            -- deserialize address
            when ST_READ_ADDR_0 =>
                anlg_state  <= "0011";
                token_ack   <= '1';
                vmm_ckdt_i  <= '0';
                wait_cnt    <= wait_cnt + 1;
                if(wait_cnt = "11")then
                    flag_addr(to_integer(index)) <= vmm_data0_s;
                    state   <= ST_READ_ADDR_1;
                else
                    state   <= ST_READ_ADDR_0;
                end if;

            when ST_READ_ADDR_1 =>
                anlg_state  <= "0100";
                if((to_integer(index)) < 7)then
                    vmm_ckdt_i  <= '1';
                    index       <= index + 1;
                    wr_en_i     <= '0';
                    state       <= ST_READ_ADDR_0;
                else
                    vmm_ckdt_i  <= '0';
                    index       <= (others => '0');
                    wr_en_i     <= '1';
                    state       <= ST_SEND_CHAN;
                end if;

            -- send chanel data and wait for UDP to start sending
            when ST_SEND_CHAN =>
                anlg_state  <= "0101";
                wr_en_i     <= '0';
                if(UDPdone_s = '0')then
                    state <= ST_WAIT_UDP;
                else
                    state <= ST_SEND_CHAN;
                end if;

            -- wait for UDP to finish
            when ST_WAIT_UDP =>
                anlg_state  <= "0110";
                if(UDPdone_s = '1')then
                    state <= ST_START_XADC;
                else
                    state <= ST_WAIT_UDP;
                end if;

            -- start xADC
            when ST_START_XADC =>
                anlg_state  <= "0111";
                if(xadc_busy_s = '1')then
                    xadcStart_i <= '0';
                    state       <= ST_WAIT_XADC;
                else
                    xadcStart_i <= '1';
                    state       <= ST_START_XADC;
                end if;

            -- wait for xADC to finish
            when ST_WAIT_XADC =>
                anlg_state      <= "1000";
                xadcStart_i     <= '0';
                if(xadc_busy_s = '1')then
                    read_tdo    <= read_tdo;
                    state       <= ST_WAIT_XADC;
                elsif(xadc_busy_s = '0' and read_tdo = '0')then
                    read_tdo    <= '1'; -- read xADC once more but sample TDO now
                    state       <= ST_START_XADC;
                elsif(xadc_busy_s = '0' and read_tdo = '1')then
                    read_tdo    <= '0'; -- flip it back to sample PDO first afterwards
                    state       <= ST_WAIT_TOKEN_0;
                else
                    read_tdo    <= read_tdo;
                    state       <= ST_WAIT_XADC;
                end if;    

            -- wait for token process to re-send the pulse
            when ST_WAIT_TOKEN_0 =>
                anlg_state  <= "1001";
                token_ack   <= '0'; -- let it roll
                if(token_sent = '0')then
                    state <= ST_WAIT_TOKEN_1;
                else
                    state <= ST_WAIT_TOKEN_0;
                end if;

            when ST_WAIT_TOKEN_1 =>
                anlg_state  <= "1010";
                if(token_sent = '1' and vmm_data0_s = '1')then
                    vmm_ckdt_i  <= '1';
                    state       <= ST_READ_ADDR_0;
                elsif(token_sent = '1' and vmm_data0_s = '0')then
                    vmm_ckdt_i  <= '0';
                    state       <= ST_RST_VMM;
                else
                    vmm_ckdt_i  <= '0';
                    state       <= ST_WAIT_TOKEN_1;
                end if;

            -- send 3 CKTKs to reset the thing
            when ST_RST_VMM =>
                anlg_state  <= "1011";
                rst_vmm     <= '1'; 
                if(reset_done = '1')then
                    state <= ST_IDLE;
                else
                    state <= ST_RST_VMM;
                end if;

            when others => state <= ST_IDLE;

            end case;
        end if;
    end if;
end process;

-- token asserting FSM
token_FSM_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        if(token_enable = '0')then
            token_aux_cnt   <= (others => '0');
            cktkPeriod_cnt  <= (others => '0');
            cnt_cktk        <= (others => '0');
            first_pulse     <= '1';
            token_sent      <= '0';
            vmm_tki_i       <= '0';
            vmm_cktk_i      <= '0';
            reset_done      <= '0';
            token_state     <= "111";
            state_token     <= ST_ASSERT_TOKEN;
        else
            case state_token is

            -- send a token pulse of ~800ns width
            when ST_ASSERT_TOKEN =>
                token_state     <= "000";
                token_aux_cnt   <= token_aux_cnt + 1;
                cktkPeriod_cnt  <= cktkPeriod_cnt + 1;
                token_sent      <= '0';

                if(first_pulse = '1')then -- send token only once
                    vmm_tki_i   <= '1';
                else
                    vmm_tki_i   <= '0';
                end if;
                
                if(token_aux_cnt = "1111111")then
                    vmm_cktk_i    <= '1';
                    state_token   <= ST_WAIT_SENT;
                elsif(token_aux_cnt >= "0000011")then
                    vmm_cktk_i    <= '1';
                    state_token   <= ST_ASSERT_TOKEN;
                else
                    vmm_cktk_i    <= '0';
                    state_token   <= ST_ASSERT_TOKEN;
                end if;

            -- wait before asserting the token_sent signal
            -- to give time to DATA0 since it is synced
            when ST_WAIT_SENT =>
                token_state     <= "001";
                vmm_tki_i       <= '0';
                first_pulse     <= '0'; -- ground in order not to have a token again
                vmm_cktk_i      <= '1';
                cktkPeriod_cnt  <= cktkPeriod_cnt + 1;
                if(token_aux_cnt = "0000111")then
                    token_aux_cnt <= (others => '0');
                    token_sent    <= '1';
                    state_token   <= ST_WAIT_MAIN;
                else
                    token_aux_cnt <= token_aux_cnt + 1;
                    token_sent    <= '0';
                    state_token   <= ST_WAIT_SENT;
                end if;

            -- wait for the main FSM to read address and xADC values
            when ST_WAIT_MAIN =>
                token_state      <= "010";
                cktkPeriod_cnt  <= cktkPeriod_cnt + 1;
                vmm_cktk_i      <= '1';
                if(token_ack = '1')then -- main FSM got the message
                    token_sent    <= '0';
                    state_token   <= ST_WAIT_WR_EN;
                elsif(rst_vmm = '1')then -- time to reset since nothing at DATA0
                    token_sent    <= '0';
                    state_token   <= ST_SEND_3CKTK_0;
                else
                    token_sent    <= '1';
                    state_token   <= ST_WAIT_MAIN;
                end if;

            -- wait to read the channel adress and flags before de-asserting the CKTK
            when ST_WAIT_WR_EN =>
                token_state     <= "011";
                cktkPeriod_cnt  <= cktkPeriod_cnt + 1;
                if(wr_en_i = '1')then
                    vmm_cktk_i  <= '0';
                    state_token <= ST_CNT_PERIOD;
                else
                    vmm_cktk_i  <= '1';
                    state_token <= ST_WAIT_WR_EN;
                end if;

            -- keep counting
            when ST_CNT_PERIOD =>
                token_state       <= "100";
                if(cktkPeriod_cnt >= to_unsigned((cktkPeriod), 28) and token_ack = '0')then -- ready to move to next channel
                    cktkPeriod_cnt  <= (others => '0');
                    state_token     <= ST_ASSERT_TOKEN;
                elsif(cktkPeriod_cnt >= to_unsigned((cktkPeriod), 28) and token_ack = '1')then -- hold, still reading
                    cktkPeriod_cnt  <= cktkPeriod_cnt;
                    state_token     <= ST_CNT_PERIOD;
                else
                    cktkPeriod_cnt  <= cktkPeriod_cnt + 1;
                    state_token     <= ST_CNT_PERIOD;
                end if;

            -- send 3xCKTK to reset the ASIC
            when ST_SEND_3CKTK_0 =>
                token_state         <= "101";
                vmm_cktk_i          <= '0';
                cktkPeriod_cnt      <= (others => '0');
                if(cnt_cktk = "11")then
                    reset_done      <= '1'; -- done, stay here until reset by main FSM
                    token_aux_cnt   <= (others => '0');
                    state_token     <= ST_SEND_3CKTK_0;
                elsif(token_aux_cnt = "0001111")then
                    reset_done      <= '0';
                    token_aux_cnt   <= (others => '0');
                    state_token     <= ST_SEND_3CKTK_1;
                else
                    reset_done      <= '0';
                    token_aux_cnt   <= token_aux_cnt + 1;
                    state_token     <= ST_SEND_3CKTK_0;
                end if;

            when ST_SEND_3CKTK_1 =>
                token_state       <= "110";
                vmm_cktk_i        <= '1';
                if(token_aux_cnt = "0000011")then
                    token_aux_cnt <= (others => '0');
                    cnt_cktk      <= cnt_cktk + 1;      
                    state_token   <= ST_SEND_3CKTK_0;
                else
                    token_aux_cnt <= token_aux_cnt + 1;
                    cnt_cktk      <= cnt_cktk;
                    state_token   <= ST_SEND_3CKTK_1;
                end if;


            when others => state_token <= ST_ASSERT_TOKEN;

            end case;
        end if;
    end if;
end process;

-- timeout counter
timeout_cnt_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        if(to_cnt_ena = '0')then
            timeout     <= '0';
            timeout_cnt <= (others => '0');
        else
            if(timeout_cnt = "1111111111")then
                timeout_cnt <= timeout_cnt;
                timeout     <= '1';    
            else
                timeout_cnt <= timeout_cnt + 1;
                timeout     <= '0';
            end if;
        end if;
    end if;
end process;

channel_buffer_inst: channel_buffer
    port map(
        rst         => fifo_init,
        wr_clk      => clk_160,
        rd_clk      => clk_125,
        din         => dout_i,
        wr_en       => wr_en_i,
        rd_en       => rd_en_i,
        dout        => dout,
        full        => fifo_full,
        empty       => fifo_empty,
        wr_rst_busy => open,
        rd_rst_busy => open
    );

-- read the buffer and send it to FIFO2UDP
udp_intf_proc: process(clk_125)
begin
    if(rising_edge(clk_125))then

        -- read once
        if(wr_en_udp_i = '1')then
            rd_en_i     <= '0';
            wr_en_udp_i <= '0';
            packRdy_i   <= '0';
        elsif(rd_en_i = '1')then
            rd_en_i     <= '0';
            wr_en_udp_i <= '1';
            packRdy_i   <= '1';
        elsif(fifo_empty = '0')then
            rd_en_i     <= '1';
            wr_en_udp_i <= '0';
            packRdy_i   <= '0';
        else
            rd_en_i     <= '0';
            wr_en_udp_i <= '0';
            packRdy_i   <= '0';
        end if;

        wrEn_sreg   <= wr_en_udp_i & wrEn_sreg(2 downto 1);
        rdy_sreg    <= packRdy_i   & rdy_sreg(4 downto 1);
        wr_en       <= wrEn_sreg(0);
        end_packet  <= rdy_sreg(0);

    end if;
end process;

CDCC_125to160: CDCC
    generic map (NUMBER_OF_BITS => 4)
    port map(
        clk_src         => clk_125,
        clk_dst         => clk_160,
        data_in(0)      => analog_on,
        data_in(1)      => trigger_pulse,
        data_in(2)      => xadc_busy,
        data_in(3)      => UDPDone,
        data_out_s(0)   => analog_on_s,
        data_out_s(1)   => trigg_s,
        data_out_s(2)   => xadc_busy_s,
        data_out_s(3)   => UDPDone_s
    );

CDCC_160to125: CDCC
    generic map (NUMBER_OF_BITS => 3)
    port map(
        clk_src         => clk_160,
        clk_dst         => clk_125,
        data_in(0)      => anlg_busy_i,
        data_in(1)      => xadcStart_i,
        data_in(2)      => read_tdo,
        data_out_s(0)   => analog_busy,
        data_out_s(1)   => xadc_start_delay,
        data_out_s(2)   => xadc_read_tdo
    );

-- to delay xadc_start
CDCC_125to125: CDCC
    generic map (NUMBER_OF_BITS => 1)
    port map(
        clk_src         => clk_125,
        clk_dst         => clk_125,
        data_in(0)      => xadc_start_delay,
        data_out_s(0)   => xadc_start
    );

--ila_overview_inst: ila_overview
--    port map(
--        clk                     => clk_160,
--        probe0(0)               => vmm_data0_s, 
--        probe0(1)               => vmm_data1,
--        probe0(2)               => anlg_busy_i,
--        probe0(3)               => vmm_ena_i,
--        probe0(4)               => to_cnt_ena,
--        probe0(5)               => token_ack,
--        probe0(6)               => wr_en_i,
--        probe0(7)               => xadcStart_i,
--        probe0(8)               => vmm_ckdt_i,
--        probe0(9)               => token_enable,
--        probe0(17 downto 10)    => flag_addr,
--        probe0(21 downto 18)    => anlg_state,
--        probe0(22)              => analog_on_s,
--        probe0(23)              => trigg_s,   
--        probe0(24)              => xadc_busy_s,
--        probe0(25)              => UDPDone_s,
--        probe0(26)              => first_pulse,
--        probe0(27)              => vmm_cktk_i,
--        probe0(28)              => vmm_tki_i,
--        probe0(29)              => token_sent,
--        probe0(32 downto 30)    => token_state,
--        probe0(33)              => timeout,
--        probe0(34)              => reset_done,
--        probe0(35)              => rst_vmm,
--        probe0(63 downto 36)    => (others => '0')
--    );

    vmm_ena     <= vmm_ena_i;
    vmm_ckdt    <= vmm_ckdt_i;
    vmm_tki     <= vmm_tki_i;
    vmm_cktk    <= vmm_cktk_i;
    packLen     <= x"001";
    dout_i      <= x"00" & flag_addr;
    --             byte      byte

end RTL;
