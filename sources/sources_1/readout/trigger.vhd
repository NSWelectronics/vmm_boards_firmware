----------------------------------------------------------------------------------
-- Company: NTU ATHENS - BNL
-- Engineer: Paris Moschovakos
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Paris Moschovakos
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 18.05.2016
-- Design Name: 
-- Module Name: trigger.vhd - Behavioral
-- Project Name: MMFE8 
-- Target Devices: Artix7 xc7a200t-2fbg484 and xc7a200t-3fbg484 
-- Tool Versions: Vivado 2016.2
--
-- Changelog:
-- 18.08.2016 Added tr_hold signal to hold trigger when reading out (Reid Pinkham)
-- 26.02.2017 Moved to a global clock domain @125MHz (Paris)
-- 27.02.2017 Synced trout (Paris)
-- 31.03.2017 Added 2 ckbc tr_mode, requests 2 CKBC upon ext trigger (Paris)
-- 06.04.2017 Configurable latency was added for the 2 CKBC tr_mode (Paris)
-- 28.04.2017 Added two processes that assert the level0 signal. (Christos Bakalis)
-- 09.08.2018 Housekeeping and moving to single clock-domain. (Christos Bakalis)
--
----------------------------------------------------------------------------------
library IEEE;
library UNISIM;
use IEEE.std_logic_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;
use UNISIM.VComponents.all;

entity trigger is
    generic(is_level0   : std_logic);
    port(
        ckbc            : in  std_logic;
        clk_art         : in  std_logic;
        clk_art2        : in  std_logic;
        rst_trig        : in  std_logic;
        state_trg       : out std_logic_vector(2 downto 0);
        
        ckbcMode        : in  std_logic;
        cktp_enable     : in  std_logic;
        cktp_pulse_width: in  std_logic_vector(4 downto 0);
        CKTP_raw        : in  std_logic;
        request2ckbc    : out std_logic;
        
        tr_en           : in  std_logic; -- synced to 160
        tr_hold         : in  std_logic; -- synced to 160
        tr_mode         : in  std_logic;
        tr_ext          : in  std_logic; -- synced to 160
        level_0         : out std_logic;
        busy_l0         : out std_logic;
        trigger_pf      : out std_logic;

        ckbc_max_conf   : in  std_logic_vector(7 downto 0);
        ckbc_max_num    : out std_logic_vector(7 downto 0);
        latency_extra   : in  std_logic_vector(15 downto 0);
        latency         : in  std_logic_vector(15 downto 0);

        tr_out          : out std_logic;
        tr_raw          : out std_logic
    );
end trigger;

architecture Behavioral of trigger is

-- Signals

    signal sel_number           : std_logic := '0';
    signal tr_out_i             : std_logic := '0'; 
    signal tr_en_buff           : std_logic := '0';
    
    signal cktp_width_final     : std_logic_vector(11 downto 0) := "000101000000";           --4 * 80 = 320
    signal tr_int               : std_logic := '0';

    constant ckbc_max_fix       : std_logic_vector(7 downto 0) := x"01"; -- one extra CKBC
 
    -- Special Readout Mode
    signal request2ckbc_i       : std_logic := '0';
    signal trigLatencyExtra     : unsigned(15 downto 0) := x"0058"; -- 88*6.25=550ns
    signal trigLatencyCnt       : unsigned(15 downto 0) := (others => '0');
    signal trigLatency          : unsigned(15 downto 0) := x"008c";
     
    type stateType is (waitingForTrigger, waitingForLatency, waitingForSingleCKBC, changeNumberOfCKBC, 
                      timeoutThenRequest, reqOneCKBC, timeoutBeforeIdle, issueRequest, checkTrigger, checkForTrg);
    signal state            : stateType := waitingForTrigger;
    signal state_l0         : stateType := waitingForTrigger;
    
-- Components if any 
    component trint_gen
    port(
        clk_160     : in  std_logic;
        cktp_start  : in  std_logic;
        cktp_pulse  : in  std_logic;
        ckbc_mode   : in  std_logic;
        cktp_width  : in  std_logic_vector(11 downto 0);
        trint       : out std_logic
    );
    end component;

begin

trenAnd: process(clk_art)
begin
    if rising_edge(clk_art) then
        if (tr_en = '1' and tr_hold = '0') then -- No hold command, trigger enabled
            tr_en_buff <= '1';
        else
            tr_en_buff <= '0';
        end if;
    end if;
end process;

triggerDistrSignalProc: process (clk_art, rst_trig)
    begin
        if rst_trig = '1' then
            tr_out_i <= '0';
        elsif rising_edge(clk_art) then
            if tr_mode = '0' then
                if (tr_en_buff = '1' and tr_mode = '0' and tr_int = '1') then
                    tr_out_i <= '1';
                elsif (tr_mode = '0' and tr_int = '0') then
                    tr_out_i <= '0';
                else
                    tr_out_i <= '0';
                end if;
            else
                if (tr_en_buff = '1' and tr_mode = '1' and tr_ext = '1') then
                    tr_out_i <= '1';
                elsif (tr_mode = '1' and tr_ext = '0') then
                    tr_out_i <= '0';
                else
                    tr_out_i <= '0';
                end if;
            end if;
        end if;
    end process;
    
triggerRawMux:process (tr_ext, tr_int, tr_mode)
begin
    if tr_mode = '1' then
        tr_raw <= tr_ext;
    elsif tr_mode = '0' then
        tr_raw <= tr_int;
    end if;
end process;

select_number_ckbc: process(clk_art2)
begin
    if rising_edge(clk_art2) then
        case sel_number is
        when '0'    => ckbc_max_num <= ckbc_max_conf;
        when '1'    => ckbc_max_num <= ckbc_max_fix;
        when others => ckbc_max_num <= ckbc_max_conf;
        end case;
    end if;
end process;

-- Processes
generate_2ckbc: if (is_level0 = '0') generate

trReadoutMode2CkbcDelayedRequest: process(clk_art)
begin
    if rising_edge(clk_art) then
        if(rst_trig = '1')then
            request2ckbc_i  <= '0';
            trigLatencyCnt  <= (others => '0');
            sel_number      <= '0';
            trigger_pf      <= '0';
            state           <= waitingForTrigger;
        else      
            case state is
            
                when waitingForTrigger =>
                    request2ckbc_i      <= '0';
                    sel_number          <= '0';
                    trigger_pf          <= '0';
                    trigLatencyCnt      <= (others => '0');
                    if ((tr_en_buff = '1' and tr_out_i = '1' and ckbcMode = '1' and tr_mode = '1') or
                        (tr_en_buff = '1' and tr_int = '1' and ckbcMode = '1' and tr_mode = '0'))then
                        state           <= waitingForLatency;
                    end if;
                    
                when waitingForLatency =>
                    if trigLatencyCnt < trigLatency then
                        request2ckbc_i  <= '0';
                        trigLatencyCnt  <= trigLatencyCnt + 1;
                    else
                        request2ckbc_i  <= '1';
                        state           <= issueRequest;
                    end if;
                    
                when issueRequest =>
                    request2ckbc_i      <= '0';
                    trigLatencyCnt      <= (others => '0');
                    state               <= waitingForSingleCKBC;

                when waitingForSingleCKBC =>
                    request2ckbc_i      <= '0';
                    if trigLatencyCnt < trigLatencyExtra then
                        trigLatencyCnt <= trigLatencyCnt + 1;
                    else
                        trigLatencyCnt <= (others => '0');
                        state          <= changeNumberOfCKBC;
                    end if;

                when changeNumberOfCKBC =>
                    sel_number <= '1';
                    state      <= timeoutThenRequest;

                when timeoutThenRequest =>
                    if trigLatencyCnt < 2 then
                        trigLatencyCnt <= trigLatencyCnt + 1;
                    else
                        state          <= reqOneCKBC;
                    end if;

                when reqOneCKBC =>
                    request2ckbc_i <= '1';
                    trigLatencyCnt <= (others => '0');
                    state          <=  timeoutBeforeIdle;

                when timeoutBeforeIdle =>
                    request2ckbc_i  <= '0';
                    trigger_pf      <= '1';
                    if trigLatencyCnt < 2 then
                        trigLatencyCnt <= trigLatencyCnt + 1;
                    else
                        state          <= checkForTrg;
                    end if;

               when checkForTrg => -- wait for trigs to go low
                    if((tr_int = '0' and tr_mode = '0') or
                        (tr_out_i = '0' and tr_mode = '1'))then
                        state <= waitingForTrigger;
                    else
                        state <= checkForTrg;
                    end if;
                    
                when others =>
                    request2ckbc_i      <= '0';
                    trigLatencyCnt      <= (others => '0');
                    sel_number          <= '0';
                    trigger_pf          <= '0';
                    state               <= waitingForTrigger;

            end case;
        end if;
    end if;
end process;

end generate generate_2ckbc;

generate_level0: if (is_level0 = '1') generate

-- asserts level0 accept signal at the VMMs with a maximum of ~1.6 us latency
level0Asserter: process(ckbc)
begin
    if(rising_edge(ckbc))then
        if(rst_trig = '1')then
            state_trg       <= "111";
            level_0         <= '0';
            trigLatencyCnt  <= (others => '0');
            busy_l0         <= '0';
            state_l0        <= waitingForTrigger;
        else
            case state_l0 is

            when waitingForTrigger =>
                state_trg       <= "000";
                level_0         <= '0';
                trigLatencyCnt  <= (others => '0');
                busy_l0         <= '0';

                -- proceed only if pf is @ idle
                if(tr_out_i = '1' and tr_hold = '0')then
                    state_l0 <= waitingForLatency;
                else
                    state_l0 <= waitingForTrigger;
                end if;

            when waitingForLatency => -- open the acceptance window for the level-0 buffer
                state_trg   <= "001";
                busy_l0     <= '1';
                if trigLatencyCnt < trigLatency then
                    trigLatencyCnt  <= trigLatencyCnt + 1;
                    level_0         <= '0';
                    state_l0        <= waitingForLatency;
                else
                    trigLatencyCnt  <= (others => '0');
                    level_0         <= '1';
                    state_l0        <= issueRequest;
                end if;
                
            when issueRequest =>
                state_trg   <= "011";
                level_0     <= '0';
                state_l0    <= checkTrigger;

            when checkTrigger =>
                state_trg   <= "100";
                level_0     <= '0';
                busy_l0     <= '0';

                if(tr_out_i = '0' and tr_hold = '0')then
                    state_l0 <= waitingForTrigger;
                else
                    state_l0 <= checkTrigger;
                end if;

            when others =>
                level_0         <= '0';
                trigLatencyCnt  <= (others => '0');
                state_l0        <= waitingForTrigger;
            end case;
        end if;
    end if;
end process;
end generate generate_level0; 
    
-- Signal assignments
tr_out              <= tr_out_i;
request2ckbc        <= request2ckbc_i;
trigLatency         <= unsigned(latency);
trigLatencyExtra    <= unsigned(latency_extra);
cktp_width_final    <= std_logic_vector(unsigned(cktp_pulse_width)*"1010000");  -- input x 80

-- Instantiations if any

cktp_trint_module: trint_gen
    port map(
        clk_160         => clk_art,
        cktp_start      => cktp_enable,
        cktp_pulse      => CKTP_raw,
        ckbc_mode       => ckbcMode,
        cktp_width      => cktp_width_final,
        trint           => tr_int -- synced to 160 Mhz
    );

end Behavioral;