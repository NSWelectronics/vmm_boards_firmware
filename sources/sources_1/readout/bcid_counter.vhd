----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 13.03.2018 14:37:44
-- Design Name: BCID counter
-- Module Name: bcid_counter - RTL
-- Project Name: NTUA-BNL VMM3 Readout Firmware
-- Target Devices: Xilinx xc7a100t-2fgg484
-- Tool Versions: Vivado 2016.4
-- Description: Local BCID counter for the readout firmware with its accompanying
-- clock-domain-crossing circuitry.
-- 
-- Dependencies: 
-- 
-- Changelog: 
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bcid_counter is
    port(
        --------------------------------
        ------- General Interface ------
        bc_clk      : in  std_logic; -- BCclk
        user_clk    : in  std_logic; -- user clock
        pf_state    : in  std_logic_vector(4 downto 0); -- pf_state
        rst         : in  std_logic; -- reset for FSMs
        ena         : in  std_logic; -- enable signal
        ckbc_enable : in  std_logic; -- enable CKBC
        fifo_init   : in  std_logic; -- fifo init signal
        --------------------------------
        -- Packet Formation Interface --
        bcid_req    : in  std_logic; -- BCID request
        bcid_rdy    : out std_logic; -- BCID rdy
        bcid_val    : out std_logic_vector(11 downto 0) -- BCID value
    );
end bcid_counter;

architecture RTL of bcid_counter is

component bcid_cdc_fifo
    port(
        rst         : in  std_logic;
        wr_clk      : in  std_logic;
        rd_clk      : in  std_logic;
        din         : in  std_logic_vector(11 downto 0);
        wr_en       : in  std_logic;
        rd_en       : in  std_logic;
        dout        : out std_logic_vector(11 downto 0);
        full        : out std_logic;
        empty       : out std_logic;
        wr_rst_busy : out std_logic;
        rd_rst_busy : out std_logic
    );
end component;

component CDCC
    generic(
        NUMBER_OF_BITS : integer := 8); -- number of signals to be synced
    port(
        clk_src     : in  std_logic;                                        -- input clk (source clock)
        clk_dst     : in  std_logic;                                        -- input clk (dest clock)
        data_in     : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0);    -- data to be synced
        data_out_s  : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0)     -- synced data to clk_dst
    );
end component; 

    signal bcid_cnt     : unsigned(11 downto 0)         := (others => '0');
    signal wait_cnt     : unsigned(1 downto 0)          := (others => '0');
    signal bcid_fsm_i   : std_logic_vector(11 downto 0) := (others => '0');
    signal fifo_full    : std_logic := '0';
    signal fifo_empty   : std_logic := '0';
    signal bcid_req_s40 : std_logic := '0';
    signal rst_s40      : std_logic := '0';
    signal wr_en        : std_logic := '0';
    signal rd_en        : std_logic := '0';
    signal wr_rst_busy  : std_logic := '0';
    signal rd_rst_busy  : std_logic := '0';

    type stateType_wr is (ST_IDLE, ST_WRITE, ST_DONE);
    signal state_wr      : stateType_wr := ST_IDLE;

    type stateType_rd is (ST_IDLE, ST_WAIT, ST_READY);
    signal state_rd      : stateType_rd := ST_IDLE;

    attribute FSM_ENCODING              : string;
    attribute FSM_ENCODING of state_wr  : signal is "ONE_HOT";
    attribute FSM_ENCODING of state_rd  : signal is "ONE_HOT";

begin

-- BCID counter
bcid_cnt_proc: process(bc_clk)
begin
    if(rising_edge(bc_clk))then
        if(ena = '0')then
            bcid_cnt <= (others => '0');
        elsif(ckbc_enable = '0')then
            bcid_cnt <= bcid_cnt;
        else
            bcid_cnt <= bcid_cnt + 1;
        end if;
    end if;
end process;

-- FSM write
FSM_write_proc: process(bc_clk)
begin
    if(rising_edge(bc_clk))then
        if(rst_s40 = '1')then
            bcid_fsm_i  <= (others => '0');
            wr_en       <= '0';
            state_wr    <= ST_IDLE;
        else
            case state_wr is
            -- wait for request signal
            when ST_IDLE =>
                if(bcid_req_s40 = '1')then
                    bcid_fsm_i  <= std_logic_vector(bcid_cnt);
                    state_wr    <= ST_WRITE;
                else
                    bcid_fsm_i  <= (others => '0');
                    state_wr    <= ST_IDLE;
                end if;
                
            -- write to the FIFO
            when ST_WRITE =>
                wr_en       <= '1';
                state_wr    <= ST_DONE;

            -- wait for the handshaking signal to go low
            when ST_DONE =>
                wr_en <= '0';
                if(bcid_req_s40 = '0')then
                    state_wr <= ST_IDLE;
                else
                    state_wr <= ST_DONE;
                end if;

            when others => state_wr <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- FSM read
FSM_read_proc: process(user_clk)
begin
    if(rising_edge(user_clk))then
        if(rst = '1')then
            bcid_rdy <= '0';
            wait_cnt <= (others => '0');
            rd_en    <= '0';
            state_rd <= ST_IDLE;
        else
            case state_rd is

            -- is the FIFO non-empty?
            when ST_IDLE =>
                bcid_rdy <= '0';
                if(fifo_empty = '0')then
                    rd_en       <= '1';
                    state_rd    <= ST_WAIT;
                else
                    rd_en       <= '0';
                    state_rd    <= ST_IDLE;
                end if;

            -- wait for the signal to stabilize
            when ST_WAIT =>
                rd_en       <= '0';
                wait_cnt    <= wait_cnt + 1;
                if(wait_cnt = "11")then
                    state_rd    <= ST_READY;
                else
                    state_rd    <= ST_WAIT;
                end if;

            -- BCID value ready
            when ST_READY =>
                bcid_rdy <= '1';
                if(bcid_req = '0')then
                    state_rd <= ST_IDLE;
                else
                    state_rd <= ST_READY;
                end if;

            when others => state_rd <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

bcid_cdc_fifo_inst: bcid_cdc_fifo
    port map(
        rst         => fifo_init,
        wr_clk      => bc_clk,
        rd_clk      => user_clk,
        din         => bcid_fsm_i,
        wr_en       => wr_en,
        rd_en       => rd_en,
        dout        => bcid_val,
        full        => fifo_full,
        empty       => fifo_empty,
        wr_rst_busy => wr_rst_busy,
        rd_rst_busy => rd_rst_busy
    );

CDCC_125to40: CDCC
    generic map(NUMBER_OF_BITS => 2)
    port map(
        clk_src         => user_clk,
        clk_dst         => bc_clk,
        data_in(0)      => bcid_req,
        data_in(1)      => rst,
        data_out_s(0)   => bcid_req_s40,
        data_out_s(1)   => rst_s40
    );

end RTL;
