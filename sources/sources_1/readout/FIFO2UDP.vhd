----------------------------------------------------------------------------------
-- Company: NTU ATHNENS - BNL - Michigan
-- Engineer: Panagiotis Gkountoumis & Reid Pinkham & Paris Moschovakos
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Panagiotis Gkountoumis & Reid Pinkham & Paris Moschovakos
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 18.04.2016 13:00:21
-- Design Name: 
-- Module Name: config_logic - Behavioral
-- Project Name: MMFE8 
-- Target Devices: Arix7 xc7a200t-2fbg484 and xc7a200t-3fbg484 
-- Tool Versions: Vivado 2016.2
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
-- Changelog:
-- 23.07.2016 Output signal "sending" to hold packet_formation from issuing new 
-- packets (Paris)
-- 26.07.2016 Increased the size of the FIFO to 2048 in order to be able to handle
-- jumbo UDP frames. (Paris)
-- 22.08.2016 Re-wrote the main logic into a single state machine to fix the freezing
-- bug. (Reid Pinkham)
-- 26.02.2016 Moved to a global clock domain @125MHz (Paris)
-- 03.08.2018 Moved back to dual clock domain + housekeeping (Christos Bakalis)
--
----------------------------------------------------------------------------------

library unisim;
use unisim.vcomponents.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;

entity FIFO2UDP is
    Port ( 
        clk_125                     : in  std_logic;
        clk_160                     : in  std_logic;
        destinationIP               : in  std_logic_vector(31 downto 0);
        daq_data_in                 : in  std_logic_vector(15 downto 0);
        fifo_data_out               : out std_logic_vector (7 downto 0);
        udp_txi		                : out udp_tx_type;	
        udp_tx_start                : out std_logic;
        UDPDone                     : out std_logic;
        UDPBusy                     : out std_logic;
        xadc_busy                   : in  std_logic;
        analog_mode                 : in  std_logic;
        udp_tx_data_out_ready       : in  std_logic;
        wr_en                       : in  std_logic;
        end_packet                  : in  std_logic;
        global_reset                : in  std_logic;
        packet_length_in            : in  std_logic_vector(11 downto 0);
        reset_DAQ_FIFO              : in  std_logic;
        confReply_packet            : in  std_logic
    );
end FIFO2UDP;

architecture Behavioral of FIFO2UDP is

    signal count                       : unsigned(3 downto 0) := x"0";
    signal count_length                : unsigned(15 downto 0) := x"0000";
    signal daq_fifo_re                 : std_logic := '0';
    signal fifo_empty_UDP              : std_logic := '0';
    signal fifo_full_UDP               : std_logic := '0';
    signal prog_fifo_empty             : std_logic := '0';
    signal data_out                    : std_logic_vector(7 downto 0) := x"00";
    signal data_out_valid              : std_logic := '0';
    signal packet_length               : unsigned(15 downto 0) := x"0000";
    signal data_out_last               : std_logic := '0';
    signal udp_tx_start_int            : std_logic := '0';
    signal wr_en_int                   : std_logic := '0';
    signal fifo_len_wr_en              : std_logic := '0';
    signal fifo_len_rd_en              : std_logic := '0';
    signal packet_len_r                : std_logic_vector(11 downto 0);
    signal fifo_empty_len              : std_logic := '0';
    signal fifo_full_len               : std_logic := '0'; 
    signal daq_data_out                : std_logic_vector(7 downto 0) := x"00"; 
    signal wait_flag                   : std_logic := '0';
    signal rst_all                     : std_logic := '0';
    signal fsm_busy                    : std_logic := '0';
    signal flag_end                    : std_logic := '0';
    signal dvalid_i                    : std_logic := '0';
    signal UDPDone_i                   : std_logic := '0';
    signal xadc_busy_s                 : std_logic := '0'; 
    signal analog_mode_s               : std_logic := '0'; 
    signal confReply_packet_s          : std_logic := '0';
    signal global_reset_s              : std_logic := '0'; 
    signal reset_DAQ_FIFO_s            : std_logic := '0'; 
    
    signal prog_full_daq_i             : std_logic := '0';
    signal prog_full_len_i             : std_logic := '0';
    signal prog_empty_daq_i            : std_logic := '0';
    signal prog_empty_len_i            : std_logic := '0';

    signal prog_empty_daq_s            : std_logic := '0';
    signal prog_empty_len_s            : std_logic := '0';

    signal UDPBusy_i                   : std_logic := '0';
  
component readout_fifo is
    port(
        rst         : in  std_logic;
        wr_clk      : in  std_logic;
        rd_clk      : in  std_logic;
        din         : in  std_logic_vector(15 downto 0);
        wr_en       : in  std_logic;
        rd_en       : in  std_logic;
        dout        : out std_logic_vector(7 downto 0);
        full        : out std_logic;
        empty       : out std_logic;
        prog_full   : out std_logic;
        prog_empty  : out std_logic;
        wr_rst_busy : out std_logic;
        rd_rst_busy : out std_logic
    );
end component;

component packet_len_fifo
    port(
        rst         : in  std_logic;
        wr_clk      : in  std_logic;
        rd_clk      : in  std_logic;
        din         : in  std_logic_vector(11 downto 0);
        wr_en       : in  std_logic;
        rd_en       : in  std_logic;
        dout        : out std_logic_vector(11 downto 0);
        full        : out std_logic;
        empty       : out std_logic;
        prog_full   : out std_logic;
        prog_empty  : out std_logic;
        wr_rst_busy : out std_logic;
        rd_rst_busy : out std_logic
    );
end component;

component CDCC
    generic(
        NUMBER_OF_BITS : integer := 8); -- number of signals to be synced
    port(
        clk_src     : in  std_logic;                                        -- input clk (source clock)
        clk_dst     : in  std_logic;                                        -- input clk (dest clock)
        data_in     : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0);    -- data to be synced
        data_out_s  : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0)     -- synced data to clk_dst
    );
end component;

begin

daq_FIFO_instance: readout_fifo
    port map(
        rst         => rst_all,
        wr_clk      => clk_160,
        rd_clk      => clk_125,
        din         => daq_data_in,
        wr_en       => wr_en,
        rd_en       => daq_fifo_re,
        dout        => daq_data_out,
        full        => fifo_full_UDP,
        empty       => fifo_empty_UDP,
        prog_full   => prog_full_daq_i,
        prog_empty  => prog_empty_daq_i,
        wr_rst_busy => open,
        rd_rst_busy => open
    );

packet_len_fifo_instance: packet_len_fifo
    port map (
        rst         => rst_all,
        wr_clk      => clk_160,
        rd_clk      => clk_125,
        din         => packet_length_in,
        wr_en       => fifo_len_wr_en,
        rd_en       => fifo_len_rd_en,
        dout        => packet_len_r,
        full        => fifo_full_len,
        empty       => fifo_empty_len,
        prog_full   => prog_full_len_i,
        prog_empty  => prog_empty_len_i,
        wr_rst_busy => open,
        rd_rst_busy => open
    );

-- process that prevents end_packet double latching
fill_packLen_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        if(flag_end = '1' and end_packet = '0')then
            fifo_len_wr_en  <= '0';
            flag_end        <= '0';    
        elsif(flag_end = '1' and end_packet = '1')then
            fifo_len_wr_en  <= '0';
            flag_end        <= '1';
        elsif(end_packet = '1')then
            fifo_len_wr_en  <= '1';
            flag_end        <= '1';
        else
            fifo_len_wr_en  <= '0';
            flag_end        <= '0';
        end if;
    end if;
end process;

UDPDone_proc: process(clk_125)
begin
    if rising_edge(clk_125) then
        if fifo_empty_UDP = '1' and fifo_empty_len = '1' and fsm_busy = '0' then -- IF Statement to inidcate when packets have been sent
            UDPDone_i <= '1';
        else
            UDPDone_i <= '0';
        end if;
    end if;
end process;

-- udp busy manager
UDPbusy_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        if(UDPBusy_i = '1')then
            if(prog_empty_len_s = '1' and prog_empty_daq_s = '1')then
                UDPBusy_i <= '0';
            else
                UDPBusy_i <= '1';
            end if;
        elsif(prog_full_len_i = '1' or prog_full_daq_i = '1')then
            UDPBusy_i <= '1';
        else
            UDPBusy_i <= '0';
        end if;
    end if;
end process;

process (clk_125)
begin
    if rising_edge(clk_125) then
        if rst_all = '1' then -- IF statement to read from length fifo and initiate a packet send
            data_out_last       <= '0';    
            data_out_valid      <= '0';     
            udp_tx_start_int    <= '0';
            fifo_len_rd_en      <= '0';
            daq_fifo_re         <= '0';
            wait_flag           <= '0';
            fsm_busy            <= '0';
            packet_length       <= (others => '0');
            count_length        <= (others => '0');
            count               <= x"0";
        else
            case count is
                when x"0" =>
                    data_out_last       <= '0';    
                    data_out_valid      <= '0';     
                    udp_tx_start_int    <= '0';
                    daq_fifo_re         <= '0';
                    wait_flag           <= '0';
                    fsm_busy            <= '0';
                    if fifo_empty_len = '0' then -- Send packets until FIFO is empty
                        fifo_len_rd_en  <= '1';
                        count           <= x"1";
                    else
                        fifo_len_rd_en  <= '0';
                        count           <= x"0";
                    end if;

                when x"1" => -- state to allow fifo time to respond
                    fsm_busy        <= '1';
                    count           <= x"2";
                    fifo_len_rd_en  <= '0';

                when x"2" =>
                    if(wait_flag = '0')then
                        wait_flag   <= '1';
                        count       <= x"2";
                    else
                        wait_flag   <= '0';
                        count       <= x"3";
                    end if;

                when x"3" =>
                    packet_length   <= resize(unsigned("0000" & packet_len_r) * 2 + 4, 16);
                    count_length    <= resize(unsigned("0000" & packet_len_r) * 2, 16);
                    fifo_len_rd_en  <= '0';
                    count           <= x"4";

                when x"4" =>
                      data_out_last     <= '0';    
                      data_out_valid    <= '0';
                      data_out          <= (others => '0');
                      udp_tx_start_int  <= '0';
                      if(confReply_packet_s = '1')then
                        udp_txi.hdr.dst_port    <= x"E887";
                      elsif(analog_mode_s = '1' and xadc_busy_s = '0')then
                        udp_txi.hdr.dst_port    <= x"1880";
                      else
                        udp_txi.hdr.dst_port    <= x"1778";
                      end if;
                      count <= x"5";

                when x"5" =>
                      udp_tx_start_int         <= '1';
                      udp_txi.hdr.dst_ip_addr  <= destinationIP;         -- set a generic ip adrress (192.168.0.255)
                      udp_txi.hdr.src_port     <= x"19CB";                -- set src and dst ports
                      udp_txi.hdr.data_length  <= std_logic_vector(packet_length); -- defined to be 16 bits in UDP
                      daq_fifo_re              <= '0';                           
                      udp_txi.hdr.checksum     <= x"0000";     
                      count <= x"6";

                when x"6" =>
                    if udp_tx_data_out_ready = '1' then     
                      udp_tx_start_int  <= '0'; 
                      daq_fifo_re       <= '1';
                      count             <= x"7";
                    end if;

                when x"7" =>
                    if udp_tx_data_out_ready = '1' then   
                      count_length      <= count_length - 1;      
                      udp_tx_start_int  <= '0'; 
                      data_out          <= daq_data_out;
                      count             <= x"8";
                    end if;

                when x"8" =>
                    if udp_tx_data_out_ready = '1' then
                        if count_length = 1 then
                            daq_fifo_re                 <= '0';
                        elsif count_length = 0 then
                            count                       <= x"9"; 
                            daq_fifo_re                 <= '0';
                        else
                            daq_fifo_re                 <= '1';
                        end if; 
                        count_length        <= count_length - 1;    
                        udp_tx_start_int    <= '0';                
                        data_out_valid      <= '1';          
                        data_out_last       <= '0';       
                        data_out            <= daq_data_out;
                    else
                        daq_fifo_re         <= '0';
                    end if;

                when x"9" =>
                    daq_fifo_re         <= '0';
                    data_out_valid      <= '1';
                    data_out            <= daq_data_out;
                    count               <= x"a";

                when x"a" =>
                  if udp_tx_data_out_ready = '1' then    
                      daq_fifo_re                 <= '0';
                      udp_tx_start_int            <= '0';
                      data_out_last               <= '0';
                      data_out <= x"ff";
                      count <= x"b";
                  end if;

                when x"b" =>
                  if udp_tx_data_out_ready = '1' then    
                      daq_fifo_re                 <= '0';
                      udp_tx_start_int            <= '0';
                      data_out_last               <= '0';
                      data_out <= x"ff";
                      count <= x"c";
                  end if;

                when x"c" =>
                  if udp_tx_data_out_ready = '1' then    
                      daq_fifo_re                 <= '0';
                      udp_tx_start_int            <= '0';
                      data_out_last               <= '0';
                      data_out <= x"ff";
                      count <= x"d";
                  end if;

                when x"d" =>
                    if udp_tx_data_out_ready = '1' then
                        daq_fifo_re                 <= '0';    
                        udp_tx_start_int            <= '0';
                        data_out_last               <= '1';
                        data_out_valid              <= '0';
                        data_out                    <= x"ff";
                        count <= x"e";
                    end if;

                when x"e" =>
                      data_out_last     <= '0';    
                      data_out_valid    <= '0';
                      data_out          <= (others => '0');
                      udp_tx_start_int  <= '0';
                      count             <= x"f";

                when x"f" =>
                      count                         <= x"0";
                      count_length                  <= x"0000";
                      data_out_last                 <= '0';    
                      data_out_valid                <= '0';                  
                      udp_tx_start_int              <= '0';

                when others =>
                    data_out_last       <= '0';    
                    data_out_valid      <= '0';     
                    udp_tx_start_int    <= '0';
                    daq_fifo_re         <= '0';
                    wait_flag           <= '0';
                    fsm_busy            <= '0';
                    count               <= x"0";                      
            end case;
        end if;
    end if;
end process;

reg_sigs_proc: process(clk_125)
begin
    if(rising_edge(clk_125))then
        udp_txi.data.data_out_last  <= data_out_last;
        udp_txi.data.data_out       <= data_out;
        dvalid_i                    <= data_out_valid;
        udp_txi.data.data_out_valid <= dvalid_i;
    end if;
end process;

udp_tx_start                <= udp_tx_start_int;
wr_en_int                   <= wr_en;
rst_all                     <= reset_DAQ_FIFO_s or global_reset_s;
UDPBusy                     <= UDPBusy_i;

---------------------------------------------------------
--------- Clock Domain Crossing Sync Block --------------
---------------------------------------------------------

CDCC_125to160: CDCC
    generic map(NUMBER_OF_BITS => 3)
    port map(
        clk_src         => clk_125,
        clk_dst         => clk_160,
  
        data_in(0)      => UDPDone_i,
        data_in(1)      => prog_empty_daq_i,
        data_in(2)      => prog_empty_len_i,
        data_out_s(0)   => UDPDone,
        data_out_s(1)   => prog_empty_daq_s,
        data_out_s(2)   => prog_empty_len_s
    );

CDCC_40to125: CDCC
    generic map(NUMBER_OF_BITS => 5)
    port map(
        clk_src                 => clk_160,
        clk_dst                 => clk_125,
        data_in(0)              => xadc_busy,
        data_in(1)              => analog_mode,
        data_in(2)              => confReply_packet,
        data_in(3)              => global_reset,
        data_in(4)              => reset_DAQ_FIFO,
        data_out_s(0)           => xadc_busy_s,
        data_out_s(1)           => analog_mode_s,
        data_out_s(2)           => confReply_packet_s,
        data_out_s(3)           => global_reset_s,
        data_out_s(4)           => reset_DAQ_FIFO_s
    );
---------------------------------------------------

end Behavioral;