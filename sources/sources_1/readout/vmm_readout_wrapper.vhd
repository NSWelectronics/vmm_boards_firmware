----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 28.04.2017 12:39:23
-- Design Name: VMM Readout Wrapper
-- Module Name: vmm_readout_wrapper - RTL
-- Project Name: NTUA-BNL VMM3 Readout Firmware
-- Target Devices: Xilinx xc7a200t-2fbg484
-- Tool Versions: Vivado 2016.4
-- Description: Wrapper that contains the two main components that implement the
-- VMM3 readout, namely vmm_readout (old continouous mode) and L0_wrapper (level-0)
-- mode.
-- 
-- Dependencies: 
-- 
-- Changelog: 
-- 28.03.2018 Added two-phase-analog readout functionality. (Christos Bakalis)
--
----------------------------------------------------------------------------------
library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;
use UNISIM.VComponents.all;

entity vmm_readout_wrapper is
    generic(is_vmm3a    : std_logic;
            is_mmfe8    : std_logic;
            is_level0   : std_logic;
            is_analog   : std_logic);
    Port(
    ------------------------------------
    --- Continuous Readout Interface ---
    clkTkProc       : in  std_logic;                    -- Used to clock checking for data process
    clkDtProc       : in  std_logic;                    -- Used to clock word readout process
    clk             : in  std_logic;                    -- Main clock
    --
    daq_enable      : in  std_logic;
    trigger_pulse   : in  std_logic;                     -- Trigger
    cktk_max        : in  std_logic_vector(7 downto 0);  -- Max number of CKTKs
    --
    dt_state_o      : out std_logic_vector(3 downto 0); -- for debugging
    dt_cntr_st_o    : out std_logic_vector(3 downto 0); -- for debugging
    ------------------------------------
    ---- Level-0 Readout Interface -----
    clk_ckdt        : in  std_logic;                    -- will be forwarded to the VMM
    clk_ckbc        : in  std_logic;
    rst_buff        : in  std_logic;                    -- reset the level-0 buffer
    rst_intf_proc   : in  std_logic;                    -- reset the pf interface
    --
    level_0         : in  std_logic;                    -- level-0 signal
    wr_accept       : in  std_logic;                    -- buffer acceptance window
    --
    vmm_conf        : in  std_logic;                    -- high during VMM configuration
    daq_on_inhib    : out std_logic;                    -- prevent daq_on state before checking link health
    level0_idle     : out std_logic;
    l0_sreg_out     : out std_logic;
    --
    state_buff_dbg  : out std_logic_vector(1 downto 0);
    fifo_full_dbg   : out std_logic;
    commas_dbg      : out std_logic;
    reset_done      : out std_logic;
    ------------------------------------
    -- 2phaseAnalog Readout Interface --
    fifo_init       : in  std_logic;
    analog_on       : in  std_logic;
    analog_busy     : out std_logic;
    trint           : in  std_logic;
    xadc_busy       : in  std_logic;
    xadc_start      : out std_logic;
    xadc_read_tdo   : out std_logic;
    UDPDone         : in  std_logic;
    wr_en_udp       : out std_logic;
    packLen_udp     : out std_logic_vector(11 downto 0);
    endPacket_udp   : out std_logic;
    dout_udp        : out std_logic_vector(15 downto 0);
    vmm_ena         : out std_logic;
    vmm_tki         : out std_logic;
    ------------------------------------
    ---- Packet Formation Interface ----
    vmmWordReady    : out std_logic;
    vmmWord         : out std_logic_vector(15 downto 0);
    vmmEventDone    : out std_logic;
    rd_ena_buff     : in  std_logic;                     -- read the readout buffer (level0 or continuous)
    vmmId           : in  std_logic_vector(2 downto 0);  -- VMM to be readout
    linkHealth_bmsk : out std_logic_vector(7 downto 0);  -- status of comma alignment links
    ------------------------------------
    ---------- VMM3 Interface ----------
    vmm_data0_vec   : in  std_logic_vector(7 downto 0);  -- Single-ended data0 from VMM
    vmm_data1_vec   : in  std_logic_vector(7 downto 0);  -- Single-ended data1 from VMM
    vmm_ckdt_glbl   : out std_logic;                     -- Strobe to VMM CKDT
    vmm_ckdt_enable : out std_logic_vector(7 downto 0);  -- Enable signal for VMM CKDT
    vmm_cktk_vec    : out std_logic_vector(7 downto 0)   -- Strobe to VMM CKTK
    );
end vmm_readout_wrapper;

architecture RTL of vmm_readout_wrapper is

    component vmm_readout is
    Generic(is_vmm3a : std_logic);
    Port( 
        clkTkProc               : in std_logic;     -- Used to clock checking for data process
        clkDtProc               : in std_logic;     -- Used to clock word readout process
        clk                     : in std_logic;     -- Used for fast switching between processes

        vmm_data0_vec           : in std_logic_vector(7 downto 0);      -- Single-ended data0 from VMM
        vmm_data1_vec           : in std_logic_vector(7 downto 0);      -- Single-ended data1 from VMM
        vmm_ckdt_enable         : out std_logic_vector(7 downto 0);     -- Enable signal for VMM CKDT
        vmm_cktk_vec            : out std_logic_vector(7 downto 0);     -- Strobe to VMM CKTK
        vmm_ckdt                : out std_logic;                        -- Strobe to VMM CKDT 

        daq_enable              : in std_logic;
        trigger_pulse           : in std_logic;                     -- Trigger
        cktk_max                : in std_logic_vector(7 downto 0);
        vmmId                   : in std_logic_vector(2 downto 0);  -- VMM to be readout
        ethernet_fifo_wr_en     : out std_logic;                    -- To be used for reading out seperate FIFOs in VMMx8 parallel readout
        vmm_data_buf            : buffer std_logic_vector(37 downto 0);
        
        vmmWordReady            : out std_logic;
        vmmWord                 : out std_logic_vector(15 downto 0);
        vmmEventDone            : out std_logic;
        
        rd_en                   : in  std_logic;
        
        dt_state_o              : out std_logic_vector(3 downto 0);
        dt_cntr_st_o            : out std_logic_vector(3 downto 0)
    );
    end component;

    component level0_wrapper is
    Generic(is_mmfe8   : std_logic);
    Port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckdt        : in  std_logic; -- will be forwarded to the VMM
        clk             : in  std_logic; -- buffer read domain
        clk_ckbc        : in  std_logic;
        rst_buff        : in  std_logic; -- reset buffer
        level_0         : in  std_logic; -- level-0 signal
        wr_accept       : in  std_logic; -- buffer acceptance window
        vmm_conf        : in  std_logic; -- high during VMM configuration
        daq_on_inhib    : out std_logic; -- prevent daq_on state before checking link health
        level0_idle     : out std_logic; -- ready to take data
        l0_sreg_out     : out std_logic_vector(7 downto 0); -- debugging
        state_buff_dbg  : out std_logic_vector(1 downto 0);
        fifo_full_dbg   : out std_logic;
        commas_dbg      : out std_logic;
        ------------------------------------
        ---- Packet Formation Interface ----
        rd_ena_buff     : in  std_logic;
        rst_intf_proc   : in  std_logic;                    -- reset the pf interface
        vmmId           : in std_logic_vector(2 downto 0);  -- VMM to be readout
        vmmWordReady    : out std_logic;
        vmmWord         : out std_logic_vector(15 downto 0);
        vmmEventDone    : out std_logic;
        linkHealth_bmsk : out std_logic_vector(7 downto 0);
        reset_done      : out std_logic;
        ------------------------------------
        ---------- VMM3 Interface ----------
        vmm_data0_vec   : in  std_logic_vector(7 downto 0);  -- Single-ended data0 from VMM
        vmm_data1_vec   : in  std_logic_vector(7 downto 0);  -- Single-ended data1 from VMM
        vmm_cktk_vec    : out std_logic_vector(7 downto 0)   -- Strobe to VMM CKTK
    );
    end component;

    component twoPhaseAnalog_readout
    Generic(cktkPeriod : integer := 160_000); -- 160_000_000 for 1Hz, 160_000 for 1Khz, 16_000 for 10 Khz, 1600 for 100 Khz 
    Port(
        -------------------------
        --- General Interface ---
        clk_160             : in  std_logic; -- core logic clock
        clk_125             : in  std_logic; -- for FIFO2UDP interfacing
        analog_on           : in  std_logic; -- in acquisition mode (125)
        trigger_pulse       : in  std_logic; -- internal trigger (125)
        fifo_init           : in  std_logic; -- initialize the fifo 
        analog_busy         : out std_logic; -- busy (125)
        -------------------------
        ---- xADC Interface -----
        xadc_busy           : in  std_logic; -- xADC is reading (125)
        xadc_start          : out std_logic; -- activate xADC (125)
        xadc_read_tdo       : out std_logic; -- read TDO on xadc
        -------------------------
        -- FIFO2UDP Interface ---
        UDPDone             : in  std_logic; -- (125)
        wr_en               : out std_logic;
        packLen             : out std_logic_vector(11 downto 0);
        end_packet          : out std_logic;
        dout                : out std_logic_vector(15 downto 0);
        -------------------------
        ---- VMM Interface ------
        vmm_ena             : out std_logic; 
        vmm_tki             : out std_logic;
        vmm_cktk            : out std_logic;
        vmm_ckdt            : out std_logic;
        vmm_data0           : in  std_logic;
        vmm_data1           : in  std_logic
    );
    end component;

    signal data0_in_vec_cont    : std_logic_vector(7 downto 0)  := (others => '0');
    signal data1_in_vec_cont    : std_logic_vector(7 downto 0)  := (others => '0');
    signal cktk_out_vec_cont    : std_logic_vector(7 downto 0)  := (others => '0');
    signal vmm_ckdt_enable_cont : std_logic_vector(7 downto 0)  := (others => '0');
    signal cktk_out_vec_analog  : std_logic_vector(7 downto 0)  := (others => '0');
    signal vmmWord_cont         : std_logic_vector(15 downto 0) := (others => '0');
    signal rd_en_cont           : std_logic := '0';
    signal vmmWordReady_cont    : std_logic := '0';
    signal vmmEventDone_cont    : std_logic := '0';
    signal vmm_ckdt_cont        : std_logic := '0';

    signal data0_in_vec_l0      : std_logic_vector(7 downto 0)  := (others => '0');
    signal data1_in_vec_l0      : std_logic_vector(7 downto 0)  := (others => '0');
    signal cktk_out_vec_l0      : std_logic_vector(7 downto 0)  := (others => '0');
    signal l0_sreg_out_i        : std_logic_vector(7 downto 0)  := (others => '0');
    signal vmmWord_l0           : std_logic_vector(15 downto 0) := (others => '0');
    signal rd_en_l0             : std_logic := '0';
    signal vmmWordReady_l0      : std_logic := '0';
    signal vmmEventDone_l0      : std_logic := '0';

    signal vmm_ckdt_glbl_i      : std_logic := '0';
    signal vmm_ckdt_analog      : std_logic := '0';
    signal vmm_cktk_analog      : std_logic := '0';
    signal vmm_ckdt_cont_final  : std_logic := '0';

begin

-- continuous mode module instantiation
continuousReadoutMode: if is_level0 = '0' and is_analog = '0' generate
readout_vmm_cont: vmm_readout
    generic map(is_vmm3a => is_vmm3a)
    port map(
        clkTkProc               => clkTkProc,
        clkDtProc               => clkDtProc,
        clk                     => clk,
        
        vmm_data0_vec           => data0_in_vec_cont,
        vmm_data1_vec           => data1_in_vec_cont,
        vmm_ckdt_enable         => vmm_ckdt_enable_cont,
        vmm_cktk_vec            => cktk_out_vec_cont,
        vmm_ckdt                => vmm_ckdt_cont,

        daq_enable              => daq_enable,
        trigger_pulse           => trigger_pulse,
        cktk_max                => cktk_max,
        vmmId                   => vmmId,
        ethernet_fifo_wr_en     => open,
        vmm_data_buf            => open,
        
        rd_en                   => rd_en_cont,
        
        vmmWordReady            => vmmWordReady_cont,
        vmmWord                 => vmmWord_cont,
        vmmEventDone            => vmmEventDone_cont,
        
        dt_state_o              => dt_state_o,
        dt_cntr_st_o            => dt_cntr_st_o
    );
end generate continuousReadoutMode;

analogReadoutMode: if is_level0 = '0' and is_analog = '1' generate
twoPhaseAnalog_readout_inst: twoPhaseAnalog_readout
    generic map(cktkPeriod => 160_000) -- 160_000_000 for 1Hz, 160_000 for 1Khz, 16_000 for 10 Khz, 1600 for 100 Khz 
    port map(
        -------------------------
        --- General Interface ---
        clk_160             => clk_ckdt,
        clk_125             => clk,
        analog_on           => analog_on,
        trigger_pulse       => trigger_pulse,
        fifo_init           => fifo_init,
        analog_busy         => analog_busy,
        -------------------------
        ---- xADC Interface -----
        xadc_busy           => xadc_busy,
        xadc_start          => xadc_start,
        xadc_read_tdo       => xadc_read_tdo,
        -------------------------
        -- FIFO2UDP Interface ---
        UDPDone             => UDPDone,
        wr_en               => wr_en_udp,
        packLen             => packLen_udp,
        end_packet          => endPacket_udp,
        dout                => dout_udp,
        -------------------------
        ---- VMM Interface ------
        vmm_ena             => vmm_ena,
        vmm_tki             => vmm_tki,
        vmm_ckdt            => vmm_ckdt_analog,
        vmm_cktk            => vmm_cktk_analog,
        vmm_data0           => data0_in_vec_cont(0),
        vmm_data1           => data1_in_vec_cont(0)
    );
end generate analogReadoutMode;

level0_readout_case : if is_level0 = '1' generate
readout_vmm_l0: level0_wrapper
    generic map(is_mmfe8 => is_mmfe8)
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_ckdt        => clk_ckdt, -- vmm_ckdt_glbl_i ??
        clk             => clk,
        clk_ckbc        => clk_ckbc,
        rst_buff        => rst_buff,
        level_0         => level_0,
        wr_accept       => wr_accept,
        vmm_conf        => vmm_conf,
        daq_on_inhib    => daq_on_inhib,
        level0_idle     => level0_idle,
        l0_sreg_out     => l0_sreg_out_i,
        state_buff_dbg  => state_buff_dbg,
        fifo_full_dbg   => fifo_full_dbg,
        commas_dbg      => commas_dbg,
        ------------------------------------
        ---- Packet Formation Interface ----
        rd_ena_buff     => rd_ena_buff,
        rst_intf_proc   => rst_intf_proc,
        vmmId           => vmmId,
        vmmWordReady    => vmmWordReady_l0,
        vmmWord         => vmmWord_l0,
        vmmEventDone    => vmmEventDone_l0,
        linkHealth_bmsk => linkHealth_bmsk,
        reset_done      => reset_done,
        ------------------------------------
        ---------- VMM3 Interface ----------
        vmm_data0_vec   => data0_in_vec_l0,
        vmm_data1_vec   => data1_in_vec_l0,
        vmm_cktk_vec    => cktk_out_vec_l0
    );
end generate level0_readout_case;

-- multiplexer/demultiplexer for different mode cases
vmm_io_muxDemux: process(vmmWordReady_cont, vmmEventDone_cont, vmmWord_cont, vmm_ckdt_enable_cont, cktk_out_vec_cont, rd_ena_buff,
                         vmmWordReady_l0, vmmEventDone_l0, vmmWord_l0, cktk_out_vec_l0, vmm_data0_vec, vmm_data1_vec, analog_on)
begin
    case is_level0 is
    when '0' =>
        -- outputs
        vmmWordReady        <= vmmWordReady_cont;
        vmmEventDone        <= vmmEventDone_cont;
        vmmWord             <= vmmWord_cont;
        if(analog_on = '0')then
            vmm_ckdt_enable     <= vmm_ckdt_enable_cont;
            vmm_cktk_vec        <= cktk_out_vec_cont;
        else
            vmm_ckdt_enable     <= x"FF";
            vmm_cktk_vec        <= cktk_out_vec_analog;
        end if;
        
        -- inputs
        rd_en_cont          <= rd_ena_buff;
        rd_en_l0            <= '0';
        data0_in_vec_cont   <= vmm_data0_vec;
        data1_in_vec_cont   <= vmm_data1_vec;
        data0_in_vec_l0     <= (others => '0');
        data1_in_vec_l0     <= (others => '0');
    when '1' =>
        -- outputs
        vmmWordReady        <= vmmWordReady_l0;
        vmmEventDone        <= vmmEventDone_l0;
        vmmWord             <= vmmWord_l0;
        vmm_ckdt_enable     <= x"FF";
        vmm_cktk_vec        <= cktk_out_vec_l0;
        -- inputs
        rd_en_cont          <= '0';
        rd_en_l0            <= rd_ena_buff;
        data0_in_vec_cont   <= (others => '0');
        data1_in_vec_cont   <= (others => '0');
        data0_in_vec_l0     <= vmm_data0_vec;
        data1_in_vec_l0     <= vmm_data1_vec;
    when others =>
        -- outputs
        vmmWordReady        <= '0';
        vmmEventDone        <= '0';
        vmmWord             <= (others => '0');
        vmm_ckdt_enable     <= (others => '0');
        vmm_cktk_vec        <= (others => '0');
        -- inputs
        data0_in_vec_cont   <= (others => '0');
        data1_in_vec_cont   <= (others => '0');
        data0_in_vec_l0     <= (others => '0');
        data1_in_vec_l0     <= (others => '0');
    end case;
end process;

CKDT_BUFGMUX: BUFGMUX
    port map(O => vmm_ckdt_glbl_i, I0 => vmm_ckdt_cont_final, I1 => clk_ckdt, S => is_level0);

    vmm_ckdt_glbl       <= vmm_ckdt_glbl_i;
    vmm_ckdt_cont_final <= vmm_ckdt_analog when (analog_on = '1') else vmm_ckdt_cont;
    
    cktk_out_vec_analog(7) <= vmm_cktk_analog;
    cktk_out_vec_analog(6) <= vmm_cktk_analog;
    cktk_out_vec_analog(5) <= vmm_cktk_analog;
    cktk_out_vec_analog(4) <= vmm_cktk_analog;
    cktk_out_vec_analog(3) <= vmm_cktk_analog;
    cktk_out_vec_analog(2) <= vmm_cktk_analog;
    cktk_out_vec_analog(1) <= vmm_cktk_analog;
    cktk_out_vec_analog(0) <= vmm_cktk_analog;
    
    l0_sreg_out            <= l0_sreg_out_i(5);

end RTL;

