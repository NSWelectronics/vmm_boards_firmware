----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 28.04.2017 14:18:44
-- Design Name: Level-0 Buffer Wrapper
-- Module Name: l0_buffer_wrapper - RTL
-- Project Name: NTUA-BNL VMM3 Readout Firmware
-- Target Devices: Xilinx xc7a200t-2fbg484
-- Tool Versions: Vivado 2016.4
-- Description: Wrapper that contains the FIFO that buffers level-0 data, and the
-- necessary acomppanying logic (packet_formation interface)
-- 
-- Dependencies: 
-- 
-- Changelog: 
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

entity l0_buffer_wrapper is
    Port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckdt        : in  std_logic;
        rst_buff        : in  std_logic;
        wr_accept       : in  std_logic;
        level_0         : in  std_logic;
        is_idle         : out std_logic;
        state_buff_dbg  : out std_logic_vector(1 downto 0);
        fifo_full_dbg   : out std_logic;
        ------------------------------------
        --- Deserializer Interface ---------
        inhib_wr        : out std_logic;
        commas_true     : in  std_logic;
        dout_dec        : in  std_logic_vector(7 downto 0);
        wr_en           : in  std_logic;
        ------------------------------------
        ---- Packet Formation Interface ----
        rd_ena_buff     : in  std_logic;
        rst_intf_proc   : in  std_logic;
        reset_done      : out std_logic;
        vmmWordReady    : out std_logic;
        vmmWord         : out std_logic_vector(15 downto 0);
        vmmEventDone    : out std_logic
    );
end l0_buffer_wrapper;

architecture RTL of l0_buffer_wrapper is

component level0_buffer
    port(
        clk     : in  std_logic;
        srst    : in  std_logic;
        din     : in  std_logic_vector(7 downto 0);
        wr_en   : in  std_logic;
        rd_en   : in  std_logic;
        dout    : out std_logic_vector(15 downto 0);
        full    : out std_logic;
        empty   : out std_logic
    );
end component;

    signal fifo_full        : std_logic := '0';
    signal fifo_empty       : std_logic := '0';
    signal inhibit_write    : std_logic := '1';
    signal enable_timeout   : std_logic := '0';
    signal fifo_dout        : std_logic_vector(15 downto 0) := (others => '0');
    signal sel_error        : std_logic := '0';
    signal state_buff       : std_logic_vector(1 downto 0) := "11";
    signal timeout_cnt      : unsigned(10 downto 0) := (others => '0'); -- 2 us
    signal timeout          : std_logic := '0';
    signal reset_done_i     : std_logic := '1';
    
    type stateType is (ST_IDLE, ST_WAIT_FOR_DATA, ST_ERROR, ST_READING, ST_DONE);
    signal state : stateType := ST_IDLE;
    
--    attribute mark_debug : string;
--    attribute mark_debug of wr_en          : signal is "TRUE";
--    attribute mark_debug of dout_dec       : signal is "TRUE";
--    attribute mark_debug of fifo_empty     : signal is "TRUE";
--    attribute mark_debug of rd_ena_buff    : signal is "TRUE";
--    attribute mark_debug of inhibit_write  : signal is "TRUE";
--    attribute mark_debug of state_buff     : signal is "TRUE";
--    attribute mark_debug of level_0        : signal is "TRUE";
    
--    attribute dont_touch : string;
--    attribute dont_touch of state_buff     : signal is "TRUE";

begin

-- Moore FSM that interfaces with packet_formation and vmm_driver
pf_interface_FSM: process(clk_ckdt)
begin
    if(rising_edge(clk_ckdt))then
        if(rst_intf_proc = '1')then
            state_buff      <= "11";
            vmmEventDone    <= '0';
            inhibit_write   <= '1';
            vmmWordReady    <= '0';
            enable_timeout  <= '0';
            is_idle         <= '0';
            reset_done_i    <= '1';
            sel_error       <= '0';
            state           <= ST_IDLE;
        else
            case state is

            -- wait for packet formation to open the window
            when ST_IDLE =>
                state_buff      <= "00";
                vmmEventDone    <= '0';
                inhibit_write   <= '1';
                is_idle         <= '1';
                vmmWordReady    <= '0';
                reset_done_i    <= '0';
                enable_timeout  <= '0';

                if(wr_accept = '1')then
                    state <= ST_WAIT_FOR_DATA;
                else
                    state <= ST_IDLE;
                end if;

            -- if there are data in the buffer and commas are being detected => ready to be read
            when ST_WAIT_FOR_DATA =>
                state_buff        <= "01";
                vmmEventDone      <= '0';
                vmmWordReady      <= '0';
                inhibit_write     <= '0';
                is_idle           <= '0';

                if(fifo_empty = '1')then -- no data? keep looking
                    enable_timeout  <= '1';
                else
                    enable_timeout  <= '0';
                end if;

                if(fifo_empty = '0' and commas_true = '1')then
                    state   <= ST_READING;
                elsif(timeout = '1' or fifo_full = '1')then -- timeout reached or filled with crap
                    state   <= ST_ERROR;
                else
                    state   <= ST_WAIT_FOR_DATA;
                end if;

            -- wait for pf to empty the buffer and prevent any further writes
            when ST_READING => 
                state_buff      <= "10";    
                inhibit_write   <= '1';
                enable_timeout  <= '0';
                vmmWordReady    <= '1';
                vmmEventDone    <= '0';
                           
                if(fifo_empty = '0')then
                    state   <= ST_READING;
                else
                    state   <= ST_DONE;
                end if;

            -- stay here until reset by pf
            when ST_DONE =>
                state_buff      <= "11";
                vmmWordReady    <= '0';
                inhibit_write   <= '1';
                enable_timeout  <= '0';
                vmmEventDone    <= '1';
                state           <= ST_DONE;

            -- error state that outputs a dummy header
            when ST_ERROR =>
                sel_error       <= '1';
                vmmWordReady    <= '1';
                if(rd_ena_buff = '1')then
                    state   <= ST_DONE;
                else
                    state   <= ST_ERROR;
                end if;

            when others =>
                vmmWordReady    <= '0';
                sel_error       <= '0';
                vmmEventDone    <= '0';
                inhibit_write   <= '0';
                enable_timeout  <= '0';
                state           <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- timeout process
to_proc: process(clk_ckdt)
begin
    if(rising_edge(clk_ckdt))then
        if(enable_timeout = '1')then
            if(timeout_cnt = "11111111111")then
                timeout     <= '1';
                timeout_cnt <= timeout_cnt;
            else
                timeout     <= '0';
                timeout_cnt <= timeout_cnt + 1;
            end if;
        else
           timeout_cnt <= (others => '0');
           timeout     <= '0';
        end if;
    end if;
end process;

selData_proc: process(sel_error, fifo_dout)
begin
    case sel_error is
    when '0'    => vmmWord <= fifo_dout;
    when '1'    => vmmWord <= x"0F" & x"FF"; -- bcid = 0xFFF
    when others => vmmWord <= fifo_dout;
    end case;
end process;

l0_buffering_fifo: level0_buffer
    port map(
        clk     => clk_ckdt,
        srst    => rst_buff,
        din     => dout_dec,
        wr_en   => wr_en,
        rd_en   => rd_ena_buff,
        dout    => fifo_dout,
        full    => fifo_full,
        empty   => fifo_empty
    );
    
--ila_level0_buf: ila_overview
--      PORT MAP (
--          clk                   => clk_ckdt,
--          probe0(0)             => wr_en,
--          probe0(8 downto 1)    => dout_dec,
--          probe0(9)             => fifo_empty,
--          probe0(10)            => rd_ena_buff,
--          probe0(12 downto 11)  => (others => '0'),
--          probe0(13)            => level_0,
--          probe0(15 downto 14)  => state_buff,
--          probe0(63 downto 16)  => (others => '0')
--      );

    inhib_wr        <= inhibit_write;
    state_buff_dbg  <= state_buff;
    fifo_full_dbg   <= fifo_full;
    reset_done      <= reset_done_i and fifo_empty;

end RTL;