
#======================= TIMING ASSERTIONS SECTION ====================
#============================= Primary Clocks =========================
#create_clock -period 5.000 -name refclk   -waveform {0.000 2.500} [get_ports REFCLK_P]
create_clock -period 8.000 -name gtrefclk -waveform {0.000 4.000} [get_ports GTREFCLK_P]

#============================= Virtual Clocks =========================
#============================= Generated Clocks =======================
## SPI FLASH BEGIN ##

# You must provide all the delay numbers
# CCLK delay is 0.5, 6.7 ns min/max for K7-2; refer Data sheet
# Consider the max delay for worst case analysis
## Following are the SPI device parameters
## Max Tco
## Min Tco
## Setup time requirement
## Hold time requirement
## Following are the board/trace delay numbers
## Assumption is that all Data lines are matched
### End of user provided delay numbers
## SPI FLASH END ##
# Following command creates a divide by 2 clock
# It also takes into account the delay added by STARTUP block to route the CCLK
create_generated_clock -name clk_sck -source [get_pins -hierarchical *axi_SPI/ext_spi_clk] -edges {3 5 7} -edge_shift {6.700 6.700 6.700} [get_pins -hierarchical *USRCCLKO]
### SPI FLASH END ##

## Continuous readout generated clock

## ODDR/CKDT
create_generated_clock -name ckdt_1 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[0].ODDR_CKDT/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[0].ODDR_CKDT/Q}]
create_generated_clock -name ckdt_2 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[1].ODDR_CKDT/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[1].ODDR_CKDT/Q}]
create_generated_clock -name ckdt_3 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[2].ODDR_CKDT/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[2].ODDR_CKDT/Q}]
create_generated_clock -name ckdt_4 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[3].ODDR_CKDT/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[3].ODDR_CKDT/Q}]
create_generated_clock -name ckdt_5 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[4].ODDR_CKDT/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[4].ODDR_CKDT/Q}]
create_generated_clock -name ckdt_6 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[5].ODDR_CKDT/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[5].ODDR_CKDT/Q}]
create_generated_clock -name ckdt_7 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[6].ODDR_CKDT/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[6].ODDR_CKDT/Q}]
create_generated_clock -name ckdt_8 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[7].ODDR_CKDT/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[7].ODDR_CKDT/Q}]

## ODDR/CKBC
create_generated_clock -name ckbc_1 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[0].ODDR_CKBC/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[0].ODDR_CKBC/Q}]
create_generated_clock -name ckbc_2 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[1].ODDR_CKBC/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[1].ODDR_CKBC/Q}]
create_generated_clock -name ckbc_3 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[2].ODDR_CKBC/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[2].ODDR_CKBC/Q}]
create_generated_clock -name ckbc_4 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[3].ODDR_CKBC/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[3].ODDR_CKBC/Q}]
create_generated_clock -name ckbc_5 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[4].ODDR_CKBC/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[4].ODDR_CKBC/Q}]
create_generated_clock -name ckbc_6 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[5].ODDR_CKBC/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[5].ODDR_CKBC/Q}]
create_generated_clock -name ckbc_7 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[6].ODDR_CKBC/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[6].ODDR_CKBC/Q}]
create_generated_clock -name ckbc_8 -source [get_pins {vmm_intf_block_inst/gen_buffers_vmm[7].ODDR_CKBC/C}] -divide_by 1 [get_pins {vmm_intf_block_inst/gen_buffers_vmm[7].ODDR_CKBC/Q}]

## ODDR/CKART
#create_generated_clock -name ckart_1 -source [get_pins vmm_intf_block_inst/gen_buffers_ckart[0].ODDR_CKART/C] -divide_by 1 [get_pins vmm_intf_block_inst/gen_buffers_ckart[0].ODDR_CKART/Q]
#create_generated_clock -name ckart_2 -source [get_pins vmm_intf_block_inst/gen_buffers_ckart[1].ODDR_CKART/C] -divide_by 1 [get_pins vmm_intf_block_inst/gen_buffers_ckart[1].ODDR_CKART/Q]
#create_generated_clock -name ckart_3 -source [get_pins vmm_intf_block_inst/gen_buffers_ckart[2].ODDR_CKART/C] -divide_by 1 [get_pins vmm_intf_block_inst/gen_buffers_ckart[2].ODDR_CKART/Q]
#create_generated_clock -name ckart_4 -source [get_pins vmm_intf_block_inst/gen_buffers_ckart[3].ODDR_CKART/C] -divide_by 1 [get_pins vmm_intf_block_inst/gen_buffers_ckart[3].ODDR_CKART/Q]
#create_generated_clock -name ckart_5 -source [get_pins vmm_intf_block_inst/gen_buffers_ckart[4].ODDR_CKART/C] -divide_by 1 [get_pins vmm_intf_block_inst/gen_buffers_ckart[4].ODDR_CKART/Q]
#create_generated_clock -name ckart_6 -source [get_pins vmm_intf_block_inst/gen_buffers_ckart[5].ODDR_CKART/C] -divide_by 1 [get_pins vmm_intf_block_inst/gen_buffers_ckart[5].ODDR_CKART/Q]
#create_generated_clock -name ckart_7 -source [get_pins vmm_intf_block_inst/gen_buffers_ckart[6].ODDR_CKART/C] -divide_by 1 [get_pins vmm_intf_block_inst/gen_buffers_ckart[6].ODDR_CKART/Q]
#create_generated_clock -name ckart_8 -source [get_pins vmm_intf_block_inst/gen_buffers_ckart[7].ODDR_CKART/C] -divide_by 1 [get_pins vmm_intf_block_inst/gen_buffers_ckart[7].ODDR_CKART/Q]


#============================= Clock Groups ===========================
# Exclusive clock group between the two possible CKDTs
#set_clock_groups -name exclusive_clk0_clk1 -physically_exclusive -group roClkCont -group [get_pins mmcm_master/inst/mmcm_adv_inst/CLKOUT2]

#============================= I/O Delays =============================
## SPI FLASH BEGIN ##
# Data is captured into FPGA on the second rising edge of ext_spi_clk after the SCK falling edge
# Data is driven by the FPGA on every alternate rising_edge of ext_spi_clk
set_input_delay -clock clk_sck -clock_fall -max 7.450 [get_ports IO*_IO]
set_input_delay -clock clk_sck -clock_fall -min 1.450 [get_ports IO*_IO]

## Data is captured into SPI on the following rising edge of SCK
## Data is driven by the IP on alternate rising_edge of the ext_spi_clk
set_output_delay -clock clk_sck -max 2.050 [get_ports IO*_IO]
set_output_delay -clock clk_sck -min -2.950 [get_ports IO*_IO]
## SPI FLASH END ##

#============================= Primary Clocks =========================
#======================================================================

#======================= TIMING EXCEPTIONS SECTION ====================
#==================== False Paths/Max Delays ==========================

# Global reset false path
set_false_path -reset_path -from [get_cells glbl_rst_i_reg]
set_false_path -reset_path -from [get_cells eth_rst_i_reg]
set_false_path -reset_path -from [get_cells rst_l0_buff_reg]
set_false_path -reset_path -from [get_cells rst_ro_final_reg]
set_false_path -reset_path -from [get_cells glbl_fifo_init_s_reg]

## SPI FLASH BEGIN ##
# this is to ensure min routing delay from SCK generation to STARTUP input
# User should change this value based on the results
# having more delay on this net reduces the Fmax
set_max_delay -datapath_only -from [get_pins -hier *SCK_O_reg_reg/C] -to [get_pins -hier *USRCCLKO] 1.500
set_min_delay -from [get_pins -hier *SCK_O_reg_reg/C] -to [get_pins -hier *USRCCLKO] 0.100
## SPI FLASH END ##

# CKBC gating delay (reset changes state 9.375ns before CKBC rising edge, and 3.125ns after CKBC falling edge)
set_max_delay -from [get_pins vmm_intf_block_inst/ckbc_gater_inst/ckbc_rst_reg/C] -to [get_pins {vmm_intf_block_inst/gen_buffers_vmm[*].ODDR_CKBC/R}] 5.000
set_min_delay -from [get_pins vmm_intf_block_inst/ckbc_gater_inst/ckbc_rst_reg/C] -to [get_pins {vmm_intf_block_inst/gen_buffers_vmm[*].ODDR_CKBC/R}] 0.100

# CDCC
set_false_path -from [get_cells {FIFO2UDP_inst/CDCC_125to160/data_in_reg_reg[*]}] -to [get_cells {FIFO2UDP_inst/CDCC_125to160/data_sync_stage_0_reg[*]}]
set_false_path -from [get_cells {FIFO2UDP_inst/CDCC_40to125/data_in_reg_reg[*]}] -to [get_cells {FIFO2UDP_inst/CDCC_40to125/data_sync_stage_0_reg[*]}]
set_false_path -from [get_cells {udp_data_in_handler_inst/CDCC_40to125/data_in_reg_reg[*]}] -to [get_cells {udp_data_in_handler_inst/CDCC_40to125/data_sync_stage_0_reg[*]}]
set_false_path -from [get_cells {udp_data_in_handler_inst/CDCC_125to40/data_in_reg_reg[*]}] -to [get_cells {udp_data_in_handler_inst/CDCC_125to40/data_sync_stage_0_reg[*]}]

set_false_path -from [get_cells {vmmFE_eth_wrapper_inst/sync_50to125/data_in_reg_reg[*]}] -to [get_cells {vmmFE_eth_wrapper_inst/sync_50to125/data_sync_stage_0_reg[*]}]

set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/CDCC_125to40/data_in_reg_reg[*]}] -to [get_cells {udp_data_in_handler_inst/fpga_config_logic/CDCC_125to40/data_sync_stage_0_reg[*]}]
set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/CDCC_40to125/data_in_reg_reg[*]}] -to [get_cells {udp_data_in_handler_inst/fpga_config_logic/CDCC_40to125/data_sync_stage_0_reg[*]}]

set_false_path -from [get_cells i2c_wrapper_generate.i2c_wrapper_inst/cdcc_ip_unit/data_in_reg_reg[*]] -to [get_cells i2c_wrapper_generate.i2c_wrapper_inst/cdcc_ip_unit/data_sync_stage_0_reg[*]]
set_false_path -from [get_cells i2c_wrapper_generate.i2c_wrapper_inst/cdcc_input_unit/data_in_reg_reg[*]] -to [get_cells i2c_wrapper_generate.i2c_wrapper_inst/cdcc_input_unit/data_sync_stage_0_reg[*]]
set_false_path -from [get_cells i2c_wrapper_generate.i2c_wrapper_inst/cdcc_output_unit/data_in_reg_reg[*]] -to [get_cells i2c_wrapper_generate.i2c_wrapper_inst/cdcc_output_unit/data_sync_stage_0_reg[*]]


set_false_path -from [get_cells {axi4_spi_generate.axi4_spi_inst/CDCC_50to40/data_in_reg_reg[*]}] -to [get_cells {axi4_spi_generate.axi4_spi_inst/CDCC_50to40/data_sync_stage_0_reg[*]}]
set_false_path -from [get_cells {axi4_spi_generate.axi4_spi_inst/CDCC_50to125/data_in_reg_reg[*]}] -to [get_cells {axi4_spi_generate.axi4_spi_inst/CDCC_50to125/data_sync_stage_0_reg[*]}]
set_false_path -from [get_cells {axi4_spi_generate.axi4_spi_inst/CDCC_125to50/data_in_reg_reg[*]}] -to [get_cells {axi4_spi_generate.axi4_spi_inst/CDCC_125to50/data_sync_stage_0_reg[*]}]


#high-fanout/slow control signals

#set_false_path -from [get_cells {packet_formation_inst/vmmId_i_reg[*]}] -to [get_cells {vmm_intf_block_inst/gen_buffers_vmm[*].ODDR_CKDT}]

#set_false_path -from [get_cells flow_fsm_inst/reply_enable_reg] -to [get_cells FIFO2UDP_inst/daq_FIFO_instance/U0/inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gbm.gbmg.gbmgc.ngecc.bmg/inst_blk_mem_gen/gnbram.gnativebmg.native_blk_mem_gen/valid.cstr/ramloop[*].ram.r/prim_noinit.ram/DEVICE_7SERIES.NO_BMM_INFO.SDP.SIMPLE_PRIM36.ram]

set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/latency_reg[*]}]
set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/ckbc_freq_reg[*]}]
set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/cktp_max_num_reg[*]}]
set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/cktp_skew_reg[*]}]
set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/cktp_period_reg[*]}]
set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/cktp_width_reg[*]}]
set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/ckbc_max_num_reg[*]}]
set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/vmm_id_xadc_reg[*]}]
set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/xadc_sample_size_reg[*]}]
set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/xadc_delay_reg[*]}]


set_false_path -from [get_pins {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/cktp_skew_reg[*]/C}] -to [get_pins clk_gen_wrapper_inst/skewing_module/CKTP_skewed_reg/D]

set_false_path -from [get_cells {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/cktp_skew_i_reg[*]}] -to [get_cells clk_gen_wrapper_inst/skewing_module/CKTP_skewed_reg]

set_false_path -from [get_pins {udp_data_in_handler_inst/fpga_config_logic/fpga_conf_router_inst/cktp_skew_i_reg[7]/C}] -to [get_pins clk_gen_wrapper_inst/skewing_module/CKTP_skewed_reg/D]

#============================= Multicycle Paths =======================
## SPI FLASH BEGIN ##
set_multicycle_path -setup -from clk_sck -to [get_clocks -of_objects [get_pins -hierarchical *ext_spi_clk]] 2
set_multicycle_path -hold -end -from clk_sck -to [get_clocks -of_objects [get_pins -hierarchical *ext_spi_clk]] 1
set_multicycle_path -setup -start -from [get_clocks -of_objects [get_pins -hierarchical *ext_spi_clk]] -to clk_sck 2
set_multicycle_path -hold -from [get_clocks -of_objects [get_pins -hierarchical *ext_spi_clk]] -to clk_sck 1
## SPI FLASH END ##


#============================= Disable Timing =========================
#======================================================================

#====================== PHYSICAL CONSTRAINTS SECTION ==================
set_property IOB TRUE [get_cells {vmm_intf_block_inst/gen_buffers_vmm[*].vmm_cktk_out_reg[*]}]
set_property IOB TRUE [get_cells {vmm_intf_block_inst/gen_buffers_vmm[*].vmm_cs_out_reg[*]}]
set_property IOB TRUE [get_cells {vmm_intf_block_inst/gen_buffers_vmm[*].vmm_ena_out_reg[*]}]
set_property IOB TRUE [get_cells {vmm_intf_block_inst/gen_buffers_vmm[*].vmm_sck_out_reg[*]}]
set_property IOB TRUE [get_cells {vmm_intf_block_inst/gen_buffers_vmm[*].vmm_sdi_out_reg[*]}]
set_property IOB TRUE [get_cells vmm_intf_block_inst/vmm_tki_out_reg]

#======================= Configurable CKBC/CKTP Constraints ==========

# CKBC global buffer placement constraint
#set_property LOC BUFGCTRL_X0Y0 [get_cells mmcm_master/inst/clkout5_buf]

# CKTP global buffer placement constraint
#set_property LOC BUFGCTRL_X0Y3 [get_cells clk_gen_wrapper_inst/CKTP_BUFGMUX]

#ASYNC_REG to skewing pipeline
#set_property ASYNC_REG true [get_cells clk_gen_wrapper_inst/cktp_generator/vmm_cktp_reg]
set_property ASYNC_REG true [get_cells clk_gen_wrapper_inst/skewing_module/CKTP_skewed_reg]
set_property ASYNC_REG true [get_cells clk_gen_wrapper_inst/skewing_module/cktp_02_reg]

#False paths for skewing pipeline (Caution!! Those lines might not be needed. It should be validated with an oscilloscope)
set_false_path -from [get_cells clk_gen_wrapper_inst/cktp_generator/vmm_cktp_reg] -to [get_cells clk_gen_wrapper_inst/skewing_module/CKTP_skewed_reg]
set_false_path -from [get_cells clk_gen_wrapper_inst/cktp_generator/vmm_cktp_reg] -to [get_cells clk_gen_wrapper_inst/skewing_module/cktp_02_reg]
#set_false_path -from [get_cells clk_gen_wrapper_inst/skewing_module/cktp_02_reg] -to [get_cells clk_gen_wrapper_inst/skewing_module/CKTP_skewed_reg]


# Added to disable timing to known artifact of phase skewing
set_disable_timing [get_timing_arcs -of_objects [get_cells clk_gen_wrapper_inst/skewing_module/CKTP_skewed_reg]]

#============================================================================================



