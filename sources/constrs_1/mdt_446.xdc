#----------------------------------------------------------------------
#----------------------------------------------------------------------
#======================== MDT 446/436 08/2016 =========================
#----------------------------------------------------------------------
#----------------------------------------------------------------------

# Data Lines input delays
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
set_input_delay -min 0.2 -clock [get_clocks ckdt_1] [get_ports DATA0_P[0]]
set_input_delay -max 0.4 -clock [get_clocks ckdt_1] [get_ports DATA0_N[0]]
set_input_delay -min 0.2 -clock [get_clocks ckdt_1] [get_ports DATA1_P[0]]
set_input_delay -max 0.4 -clock [get_clocks ckdt_1] [get_ports DATA1_N[0]]
#-----------------------------------------------------------------------------------------------------------------------------

#-------------------------------------- CKTK/L0 delay ------------------------------------------------------------------------
set_output_delay 0.3 -clock [get_clocks ckbc_1] [get_ports CKTK_P[0]]
set_output_delay 0.3 -clock [get_clocks ckbc_1] [get_ports CKTK_N[0]]
#-----------------------------------------------------------------------------------------------------------------------------

#------------------- CKBC/CKTP placement constraints  ------------------------------------------------------------------------
# register-to-CKBC buffer placement constraint
# xc7a200tfbg484
# CKBC global buffer placement constraint
set_property LOC BUFGCTRL_X0Y0 [get_cells mmcm_master/inst/clkout1_buf]

# CKTP global buffer placement constraint
set_property LOC BUFGCTRL_X0Y3 [get_cells clk_gen_wrapper_inst/CKTP_BUFGMUX]

# register-to-CKTP buffer placement constraint
# xc7a200tfbg484
set_property LOC SLICE_X83Y146 [get_cells clk_gen_wrapper_inst/cktp_generator/vmm_cktp_reg]
set_property LOC SLICE_X83Y147 [get_cells clk_gen_wrapper_inst/skewing_module/CKTP_skewed_reg]

# critical register of cktp generator placement constraint
# xc7a200tfbg484
set_property LOC SLICE_X82Y146 [get_cells clk_gen_wrapper_inst/cktp_generator/start_align_cnt_reg]
#-----------------------------------------------------------------------------------------------------------------------------

#====================== I/O Placement - IOSTANDARDS ===================

############################# MDT #############################
set_property PACKAGE_PIN V4             [get_ports REFCLK_P]
set_property PACKAGE_PIN W4             [get_ports REFCLK_N]
set_property IOSTANDARD LVDS_25         [get_ports REFCLK_P]
set_property IOSTANDARD LVDS_25         [get_ports REFCLK_N]
set_property DIFF_TERM TRUE             [get_ports REFCLK_P]

#D17 -> LOCKED
set_property PACKAGE_PIN F15            [get_ports GPIO_LED[0]]
set_property IOSTANDARD LVCMOS12        [get_ports GPIO_LED[0]]

#D16
set_property PACKAGE_PIN T3             [get_ports GPIO_LED[1]]
set_property IOSTANDARD LVCMOS25        [get_ports GPIO_LED[1]]

#D15
set_property PACKAGE_PIN P20            [get_ports GPIO_LED[2]]
set_property IOSTANDARD LVCMOS33        [get_ports GPIO_LED[2]]

#D14 -> PACKET_FORMATION_BUSY
set_property PACKAGE_PIN F4             [get_ports GPIO_LED[3]]
set_property IOSTANDARD LVCMOS25        [get_ports GPIO_LED[3]]

#D11
set_property PACKAGE_PIN J16            [get_ports GPIO_LED[4]]
set_property IOSTANDARD LVCMOS12        [get_ports GPIO_LED[4]]

#D12
set_property PACKAGE_PIN Y17            [get_ports GPIO_LED[5]]
set_property IOSTANDARD LVCMOS25        [get_ports GPIO_LED[5]]

############################# Ethernet #############################
set_property PACKAGE_PIN F6             [get_ports GTREFCLK_P]
set_property PACKAGE_PIN E6             [get_ports GTREFCLK_N]
set_property PACKAGE_PIN A8             [get_ports RXN]
set_property PACKAGE_PIN B8             [get_ports RXP]
set_property PACKAGE_PIN A4             [get_ports TXN]
set_property PACKAGE_PIN B4             [get_ports TXP]
set_property PACKAGE_PIN AA8            [get_ports PHY_INT]
set_property IOSTANDARD LVCMOS25        [get_ports PHY_INT]
set_property PACKAGE_PIN AB8            [get_ports PHY_RSTN]
set_property IOSTANDARD LVCMOS25        [get_ports PHY_RSTN]

#########################TRIGGER-MDT#############################
set_property PACKAGE_PIN V18            [get_ports LEMO_TRG]
set_property IOSTANDARD LVCMOS33        [get_ports LEMO_TRG]

set_property PACKAGE_PIN W21            [get_ports TRG_OUT_P]
set_property PACKAGE_PIN W22            [get_ports TRG_OUT_N]
set_property IOSTANDARD LVCMOS33        [get_ports TRG_OUT_P]
set_property IOSTANDARD LVCMOS33        [get_ports TRG_OUT_N]

############################ MO MDT ############################
set_property PACKAGE_PIN H13            [get_ports MO]
set_property IOSTANDARD LVCMOS12        [get_ports MO]
set_property PULLDOWN TRUE              [get_ports MO]

##########################ART VMM3 MDT############################

set_property IOSTANDARD DIFF_HSUL_12    [get_ports ART_1_P]
set_property PACKAGE_PIN A18            [get_ports ART_1_P]
set_property PACKAGE_PIN A19            [get_ports ART_1_N]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports ART_1_N]

#########################DATA0 VMM3#############################

set_property IOSTANDARD DIFF_HSUL_12    [get_ports DATA0_P[0]]
set_property PACKAGE_PIN B15            [get_ports DATA0_P[0]]
set_property PACKAGE_PIN B16            [get_ports DATA0_N[0]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports DATA0_N[0]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports DATA0_P[1]]
set_property PACKAGE_PIN T21            [get_ports DATA0_P[1]]
set_property PACKAGE_PIN U21            [get_ports DATA0_N[1]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports DATA0_N[1]]
set_property PULLDOWN TRUE              [get_ports DATA0_P[1]]

set_property IOSTANDARD TMDS_33         [get_ports DATA0_P[2]]
set_property PACKAGE_PIN N17            [get_ports DATA0_P[2]]
set_property PACKAGE_PIN P17            [get_ports DATA0_N[2]]
set_property IOSTANDARD TMDS_33         [get_ports DATA0_N[2]]
set_property PULLDOWN TRUE              [get_ports DATA0_P[2]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports DATA0_P[3]]
set_property PACKAGE_PIN M18            [get_ports DATA0_P[3]]
set_property PACKAGE_PIN L18            [get_ports DATA0_N[3]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports DATA0_N[3]]
set_property PULLDOWN TRUE              [get_ports DATA0_P[3]]

set_property IOSTANDARD LVDS_25         [get_ports DATA0_P[4]]
set_property PACKAGE_PIN E1             [get_ports DATA0_P[4]]
set_property PACKAGE_PIN D1             [get_ports DATA0_N[4]]
set_property IOSTANDARD LVDS_25         [get_ports DATA0_N[4]]
set_property PULLDOWN TRUE              [get_ports DATA0_P[4]]

set_property IOSTANDARD LVDS_25         [get_ports DATA0_P[5]]
set_property PACKAGE_PIN M1             [get_ports DATA0_P[5]]
set_property PACKAGE_PIN L1             [get_ports DATA0_N[5]]
set_property IOSTANDARD LVDS_25         [get_ports DATA0_N[5]]
set_property PULLDOWN TRUE              [get_ports DATA0_P[5]]

set_property IOSTANDARD LVDS_25         [get_ports DATA0_P[6]]
set_property PACKAGE_PIN Y4             [get_ports DATA0_P[6]]
set_property PACKAGE_PIN AA4            [get_ports DATA0_N[6]]
set_property IOSTANDARD LVDS_25         [get_ports DATA0_N[6]]
set_property PULLDOWN TRUE              [get_ports DATA0_P[6]]

set_property IOSTANDARD LVDS_25         [get_ports DATA0_P[7]]
set_property PACKAGE_PIN Y6             [get_ports DATA0_P[7]]
set_property PACKAGE_PIN AA6            [get_ports DATA0_N[7]]
set_property IOSTANDARD LVDS_25         [get_ports DATA0_N[7]]
set_property PULLDOWN TRUE              [get_ports DATA0_P[7]]

######################### DATA1 VMM3 #############################

set_property IOSTANDARD DIFF_HSUL_12    [get_ports DATA1_P[0]]
set_property PACKAGE_PIN C14            [get_ports DATA1_P[0]]
set_property PACKAGE_PIN C15            [get_ports DATA1_N[0]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports DATA1_N[0]]

set_property IOSTANDARD TMDS_33         [get_ports DATA1_P[1]]
set_property PACKAGE_PIN H17            [get_ports DATA1_P[1]]
set_property PACKAGE_PIN H18            [get_ports DATA1_N[1]]
set_property IOSTANDARD TMDS_33         [get_ports DATA1_N[1]]
set_property PULLDOWN TRUE              [get_ports DATA1_P[1]]

set_property IOSTANDARD TMDS_33         [get_ports DATA1_P[2]]
set_property PACKAGE_PIN P19            [get_ports DATA1_P[2]]
set_property PACKAGE_PIN R19            [get_ports DATA1_N[2]]
set_property IOSTANDARD TMDS_33         [get_ports DATA1_N[2]]
set_property PULLDOWN TRUE              [get_ports DATA1_P[2]]

set_property IOSTANDARD TMDS_33         [get_ports DATA1_P[3]]
set_property PACKAGE_PIN D20            [get_ports DATA1_P[3]]
set_property PACKAGE_PIN C20            [get_ports DATA1_N[3]]
set_property IOSTANDARD TMDS_33         [get_ports DATA1_N[3]]
set_property PULLDOWN TRUE              [get_ports DATA1_P[3]]

set_property IOSTANDARD LVDS_25         [get_ports DATA1_P[4]]
set_property PACKAGE_PIN E2             [get_ports DATA1_P[4]]
set_property PACKAGE_PIN D2             [get_ports DATA1_N[4]]
set_property IOSTANDARD LVDS_25         [get_ports DATA1_N[4]]
set_property PULLDOWN TRUE              [get_ports DATA1_P[4]]

set_property IOSTANDARD LVDS_25         [get_ports DATA1_P[5]]
set_property PACKAGE_PIN L3             [get_ports DATA1_P[5]]
set_property PACKAGE_PIN K3             [get_ports DATA1_N[5]]
set_property IOSTANDARD LVDS_25         [get_ports DATA1_N[5]]
set_property PULLDOWN TRUE              [get_ports DATA1_P[5]]

set_property IOSTANDARD TMDS_33         [get_ports DATA1_P[6]]
set_property PACKAGE_PIN W2             [get_ports DATA1_P[6]]
set_property PACKAGE_PIN Y2             [get_ports DATA1_N[6]]
set_property IOSTANDARD TMDS_33         [get_ports DATA1_N[6]]
set_property PULLDOWN TRUE              [get_ports DATA1_P[6]]

set_property IOSTANDARD LVDS_25         [get_ports DATA1_P[7]]
set_property PACKAGE_PIN Y8             [get_ports DATA1_P[7]]
set_property PACKAGE_PIN Y7             [get_ports DATA1_N[7]]
set_property IOSTANDARD LVDS_25         [get_ports DATA1_N[7]]
set_property PULLDOWN TRUE              [get_ports DATA1_P[7]]

########################## CKDT VMM3 ##############################

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKDT_P[0]]
set_property PACKAGE_PIN B20            [get_ports CKDT_P[0]]
set_property PACKAGE_PIN A20            [get_ports CKDT_N[0]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKDT_N[0]]

set_property IOSTANDARD TMDS_33         [get_ports CKDT_P[1]]
set_property PACKAGE_PIN AA18           [get_ports CKDT_P[1]]
set_property PACKAGE_PIN AB18           [get_ports CKDT_N[1]]
set_property IOSTANDARD TMDS_33         [get_ports CKDT_N[1]]
set_property PULLDOWN TRUE              [get_ports CKDT_P[1]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKDT_P[2]]
set_property PACKAGE_PIN B21            [get_ports CKDT_P[2]]
set_property PACKAGE_PIN A21            [get_ports CKDT_N[2]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKDT_N[2]]
set_property PULLDOWN TRUE              [get_ports CKDT_P[2]]

set_property IOSTANDARD LVDS_25         [get_ports CKDT_P[3]]
set_property PACKAGE_PIN B1             [get_ports CKDT_P[3]]
set_property PACKAGE_PIN A1             [get_ports CKDT_N[3]]
set_property IOSTANDARD LVDS_25         [get_ports CKDT_N[3]]
set_property PULLDOWN TRUE              [get_ports CKDT_P[3]]

set_property IOSTANDARD LVDS_25         [get_ports CKDT_P[4]]
set_property PACKAGE_PIN F3             [get_ports CKDT_P[4]]
set_property PACKAGE_PIN E3             [get_ports CKDT_N[4]]
set_property IOSTANDARD LVDS_25         [get_ports CKDT_N[4]]
set_property PULLDOWN TRUE              [get_ports CKDT_P[4]]

set_property IOSTANDARD LVDS_25         [get_ports CKDT_P[5]]
set_property PACKAGE_PIN U2             [get_ports CKDT_P[5]]
set_property PACKAGE_PIN V2             [get_ports CKDT_N[5]]
set_property IOSTANDARD LVDS_25         [get_ports CKDT_N[5]]
set_property PULLDOWN TRUE              [get_ports CKDT_P[5]]

set_property IOSTANDARD TMDS_33         [get_ports CKDT_P[6]]
set_property PACKAGE_PIN W19            [get_ports CKDT_P[6]]
set_property PACKAGE_PIN W20            [get_ports CKDT_N[6]]
set_property IOSTANDARD TMDS_33         [get_ports CKDT_N[6]]
set_property PULLDOWN TRUE              [get_ports CKDT_P[6]]

set_property IOSTANDARD TMDS_33         [get_ports CKDT_P[7]]
set_property PACKAGE_PIN N13            [get_ports CKDT_P[7]]
set_property PACKAGE_PIN N14            [get_ports CKDT_N[7]]
set_property IOSTANDARD TMDS_33         [get_ports CKDT_N[7]]
set_property PULLDOWN TRUE              [get_ports CKDT_P[7]]

########################## CKBC VMM3 ##############################

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKBC_P[0]]
set_property PACKAGE_PIN J19            [get_ports CKBC_P[0]]
set_property PACKAGE_PIN H19            [get_ports CKBC_N[0]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKBC_N[0]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKBC_P[1]]
set_property PACKAGE_PIN N22            [get_ports CKBC_P[1]]
set_property PACKAGE_PIN M22            [get_ports CKBC_N[1]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKBC_N[1]]
set_property PULLDOWN TRUE              [get_ports CKBC_P[1]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKBC_P[2]]
set_property PACKAGE_PIN E13            [get_ports CKBC_P[2]]
set_property PACKAGE_PIN E14            [get_ports CKBC_N[2]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKBC_N[2]]
set_property PULLDOWN TRUE              [get_ports CKBC_P[2]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKBC_P[3]]
set_property PACKAGE_PIN F19            [get_ports CKBC_P[3]]
set_property PACKAGE_PIN F20            [get_ports CKBC_N[3]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKBC_N[3]]
set_property PULLDOWN TRUE              [get_ports CKBC_P[3]]

set_property IOSTANDARD LVDS_25         [get_ports CKBC_P[4]]
set_property PACKAGE_PIN K1             [get_ports CKBC_P[4]]
set_property PACKAGE_PIN J1             [get_ports CKBC_N[4]]
set_property IOSTANDARD LVDS_25         [get_ports CKBC_N[4]]
set_property PULLDOWN TRUE              [get_ports CKBC_P[4]]

set_property IOSTANDARD LVDS_25         [get_ports CKBC_P[5]]
set_property PACKAGE_PIN K6             [get_ports CKBC_P[5]]
set_property PACKAGE_PIN J6             [get_ports CKBC_N[5]]
set_property IOSTANDARD LVDS_25         [get_ports CKBC_N[5]]
set_property PULLDOWN TRUE              [get_ports CKBC_P[5]]

set_property IOSTANDARD LVDS_25         [get_ports CKBC_P[6]]
set_property PACKAGE_PIN R3             [get_ports CKBC_P[6]]
set_property PACKAGE_PIN R2             [get_ports CKBC_N[6]]
set_property IOSTANDARD LVDS_25         [get_ports CKBC_N[6]]
set_property PULLDOWN TRUE              [get_ports CKBC_P[6]]

set_property IOSTANDARD LVDS_25         [get_ports CKBC_P[7]]
set_property PACKAGE_PIN T5             [get_ports CKBC_P[7]]
set_property PACKAGE_PIN U5             [get_ports CKBC_N[7]]
set_property IOSTANDARD LVDS_25         [get_ports CKBC_N[7]]
set_property PULLDOWN TRUE              [get_ports CKBC_P[7]]

########################## CKTP VMM3 ##############################

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKTP_P[0]]
set_property PACKAGE_PIN H20            [get_ports CKTP_P[0]]
set_property PACKAGE_PIN G20            [get_ports CKTP_N[0]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKTP_N[0]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKTP_P[1]]
set_property PACKAGE_PIN F13            [get_ports CKTP_P[1]]
set_property PACKAGE_PIN F14            [get_ports CKTP_N[1]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKTP_N[1]]
set_property PULLDOWN TRUE              [get_ports CKTP_P[1]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKTP_P[2]]
set_property PACKAGE_PIN C22            [get_ports CKTP_P[2]]
set_property PACKAGE_PIN B22            [get_ports CKTP_N[2]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKTP_N[2]]
set_property PULLDOWN TRUE              [get_ports CKTP_P[2]]

set_property IOSTANDARD LVDS_25         [get_ports CKTP_P[3]]
set_property PACKAGE_PIN P2             [get_ports CKTP_P[3]]
set_property PACKAGE_PIN N2             [get_ports CKTP_N[3]]
set_property IOSTANDARD LVDS_25         [get_ports CKTP_N[3]]
set_property PULLDOWN TRUE              [get_ports CKTP_P[3]]

set_property IOSTANDARD LVDS_25         [get_ports CKTP_P[4]]
set_property PACKAGE_PIN H2             [get_ports CKTP_P[4]]
set_property PACKAGE_PIN G2             [get_ports CKTP_N[4]]
set_property IOSTANDARD LVDS_25         [get_ports CKTP_N[4]]
set_property PULLDOWN TRUE              [get_ports CKTP_P[4]]

set_property IOSTANDARD LVDS_25         [get_ports CKTP_P[5]]
set_property PACKAGE_PIN M3             [get_ports CKTP_P[5]]
set_property PACKAGE_PIN M2             [get_ports CKTP_N[5]]
set_property IOSTANDARD LVDS_25         [get_ports CKTP_N[5]]
set_property PULLDOWN TRUE              [get_ports CKTP_P[5]]

set_property IOSTANDARD LVDS_25         [get_ports CKTP_P[6]]
set_property PACKAGE_PIN U3             [get_ports CKTP_P[6]]
set_property PACKAGE_PIN V3             [get_ports CKTP_N[6]]
set_property IOSTANDARD LVDS_25         [get_ports CKTP_N[6]]
set_property PULLDOWN TRUE              [get_ports CKTP_P[6]]

set_property IOSTANDARD LVDS_25         [get_ports CKTP_P[7]]
set_property PACKAGE_PIN R4             [get_ports CKTP_P[7]]
set_property PACKAGE_PIN T4             [get_ports CKTP_N[7]]
set_property IOSTANDARD LVDS_25         [get_ports CKTP_N[7]]
set_property PULLDOWN TRUE              [get_ports CKTP_P[7]]

########################## CKTK VMM3 ##############################

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKTK_P[0]]
set_property PACKAGE_PIN M21            [get_ports CKTK_P[0]]
set_property PACKAGE_PIN L21            [get_ports CKTK_N[0]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKTK_N[0]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKTK_P[1]]
set_property PACKAGE_PIN G21            [get_ports CKTK_P[1]]
set_property PACKAGE_PIN G22            [get_ports CKTK_N[1]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKTK_N[1]]
set_property PULLDOWN TRUE              [get_ports CKTK_P[1]]

set_property IOSTANDARD LVDS_25         [get_ports CKTK_P[2]]
set_property PACKAGE_PIN Y13            [get_ports CKTK_P[2]]
set_property PACKAGE_PIN AA14           [get_ports CKTK_N[2]]
set_property IOSTANDARD LVDS_25         [get_ports CKTK_N[2]]
set_property PULLDOWN TRUE              [get_ports CKTK_P[2]]

set_property IOSTANDARD LVDS_25         [get_ports CKTK_P[3]]
set_property PACKAGE_PIN W14            [get_ports CKTK_P[3]]
set_property PACKAGE_PIN Y14            [get_ports CKTK_N[3]]
set_property IOSTANDARD LVDS_25         [get_ports CKTK_N[3]]
set_property PULLDOWN TRUE              [get_ports CKTK_P[3]]

set_property IOSTANDARD LVDS_25         [get_ports CKTK_P[4]]
set_property PACKAGE_PIN C2             [get_ports CKTK_P[4]]
set_property PACKAGE_PIN B2             [get_ports CKTK_N[4]]
set_property IOSTANDARD LVDS_25         [get_ports CKTK_N[4]]
set_property PULLDOWN TRUE              [get_ports CKTK_P[4]]

set_property IOSTANDARD LVDS_25         [get_ports CKTK_P[5]]
set_property PACKAGE_PIN P5             [get_ports CKTK_P[5]]
set_property PACKAGE_PIN P4             [get_ports CKTK_N[5]]
set_property IOSTANDARD LVDS_25         [get_ports CKTK_N[5]]
set_property PULLDOWN TRUE              [get_ports CKTK_P[5]]

set_property IOSTANDARD LVDS_25         [get_ports CKTK_P[6]]
set_property PACKAGE_PIN AB3            [get_ports CKTK_P[6]]
set_property PACKAGE_PIN AB2            [get_ports CKTK_N[6]]
set_property IOSTANDARD LVDS_25         [get_ports CKTK_N[6]]
set_property PULLDOWN TRUE              [get_ports CKTK_P[6]]

set_property IOSTANDARD TMDS_33         [get_ports CKTK_P[7]]
set_property PACKAGE_PIN P16            [get_ports CKTK_P[7]]
set_property PACKAGE_PIN R17            [get_ports CKTK_N[7]]
set_property IOSTANDARD TMDS_33         [get_ports CKTK_N[7]]
set_property PULLDOWN TRUE              [get_ports CKTK_P[7]]

############################# SDI VMM3 ###########################

set_property PACKAGE_PIN G17            [get_ports SDI[0]]
set_property IOSTANDARD LVCMOS12        [get_ports SDI[0]]

set_property PACKAGE_PIN L19            [get_ports SDI[1]]
set_property IOSTANDARD LVCMOS12        [get_ports SDI[1]]
set_property PULLDOWN TRUE              [get_ports SDI[1]]

set_property PACKAGE_PIN AA15           [get_ports SDI[2]]
set_property IOSTANDARD LVCMOS25        [get_ports SDI[2]]
set_property PULLDOWN TRUE              [get_ports SDI[2]]

set_property PACKAGE_PIN M15            [get_ports SDI[3]]
set_property IOSTANDARD LVCMOS12        [get_ports SDI[3]]
set_property PULLDOWN TRUE              [get_ports SDI[3]]

set_property PACKAGE_PIN H3             [get_ports SDI[4]]
set_property IOSTANDARD LVCMOS25        [get_ports SDI[4]]
set_property PULLDOWN TRUE              [get_ports SDI[4]]

set_property PACKAGE_PIN L5             [get_ports SDI[5]]
set_property IOSTANDARD LVCMOS25        [get_ports SDI[5]]
set_property PULLDOWN TRUE              [get_ports SDI[5]]

set_property PACKAGE_PIN W9             [get_ports SDI[6]]
set_property IOSTANDARD LVCMOS25        [get_ports SDI[6]]
set_property PULLDOWN TRUE              [get_ports SDI[6]]
    
set_property PACKAGE_PIN V9             [get_ports SDI[7]]
set_property IOSTANDARD LVCMOS25        [get_ports SDI[7]]
set_property PULLDOWN TRUE              [get_ports SDI[7]]

############################# SDO VMM3 ###########################

set_property PACKAGE_PIN G18            [get_ports SDO[0]]
set_property IOSTANDARD LVCMOS12        [get_ports SDO[0]]

set_property PACKAGE_PIN L20            [get_ports SDO[1]]
set_property IOSTANDARD LVCMOS12        [get_ports SDO[1]]
set_property PULLDOWN TRUE              [get_ports SDO[1]]

set_property PACKAGE_PIN M17            [get_ports SDO[2]]
set_property IOSTANDARD LVCMOS12        [get_ports SDO[2]]
set_property PULLDOWN TRUE              [get_ports SDO[2]]

set_property PACKAGE_PIN D17            [get_ports SDO[3]]
set_property IOSTANDARD LVCMOS12        [get_ports SDO[3]]
set_property PULLDOWN TRUE              [get_ports SDO[3]]

set_property PACKAGE_PIN G3             [get_ports SDO[4]]
set_property IOSTANDARD LVCMOS25        [get_ports SDO[4]]
set_property PULLDOWN TRUE              [get_ports SDO[4]]

set_property PACKAGE_PIN L4             [get_ports SDO[5]]
set_property IOSTANDARD LVCMOS25        [get_ports SDO[5]]
set_property PULLDOWN TRUE              [get_ports SDO[5]]

set_property PACKAGE_PIN Y9             [get_ports SDO[6]]
set_property IOSTANDARD LVCMOS25        [get_ports SDO[6]]
set_property PULLDOWN TRUE              [get_ports SDO[6]]

set_property PACKAGE_PIN V8             [get_ports SDO[7]]
set_property IOSTANDARD LVCMOS25        [get_ports SDO[7]]
set_property PULLDOWN TRUE              [get_ports SDO[7]]

############################# TKI/TKO ###########################

set_property PACKAGE_PIN K17            [get_ports TKI_P]
set_property PACKAGE_PIN J17            [get_ports TKI_N]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports TKI_P]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports TKI_N]

set_property PACKAGE_PIN P6             [get_ports TKI_MMFE8_P]
set_property PACKAGE_PIN N5             [get_ports TKI_MMFE8_N]

set_property IOSTANDARD LVDS_25         [get_ports TKI_MMFE8_P]
set_property IOSTANDARD LVDS_25         [get_ports TKI_MMFE8_N]

set_property PACKAGE_PIN J20            [get_ports TKO_P]
set_property PACKAGE_PIN J21            [get_ports TKO_N]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports TKO_P]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports TKO_N]

########################## ENA VMM3 ##############################

set_property IOSTANDARD DIFF_HSUL_12    [get_ports ENA_P[0]]
set_property PACKAGE_PIN F18            [get_ports ENA_P[0]]
set_property PACKAGE_PIN E18            [get_ports ENA_N[0]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports ENA_N[0]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports ENA_P[1]]
set_property PACKAGE_PIN K18            [get_ports ENA_P[1]]
set_property PACKAGE_PIN K19            [get_ports ENA_N[1]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports ENA_N[1]]
set_property PULLDOWN  TRUE             [get_ports ENA_P[1]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports ENA_P[2]]
set_property PACKAGE_PIN A13            [get_ports ENA_P[2]]
set_property PACKAGE_PIN A14            [get_ports ENA_N[2]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports ENA_N[2]]
set_property PULLDOWN TRUE              [get_ports ENA_P[2]]

set_property IOSTANDARD TMDS_33         [get_ports ENA_P[3]]
set_property PACKAGE_PIN P15            [get_ports ENA_P[3]]
set_property PACKAGE_PIN R16            [get_ports ENA_N[3]]
set_property IOSTANDARD TMDS_33         [get_ports ENA_N[3]]
set_property PULLDOWN TRUE              [get_ports ENA_P[3]]

set_property IOSTANDARD LVDS_25         [get_ports ENA_P[4]]
set_property PACKAGE_PIN G1             [get_ports ENA_P[4]]
set_property PACKAGE_PIN F1             [get_ports ENA_N[4]]
set_property IOSTANDARD LVDS_25         [get_ports ENA_N[4]]
set_property PULLDOWN TRUE              [get_ports ENA_P[4]]

set_property IOSTANDARD LVDS_25         [get_ports ENA_P[5]]
set_property PACKAGE_PIN K4             [get_ports ENA_P[5]]
set_property PACKAGE_PIN J4             [get_ports ENA_N[5]]
set_property IOSTANDARD LVDS_25         [get_ports ENA_N[5]]
set_property PULLDOWN TRUE              [get_ports ENA_P[5]]

set_property IOSTANDARD LVDS_25         [get_ports ENA_P[6]]
set_property PACKAGE_PIN AA5            [get_ports ENA_P[6]]
set_property PACKAGE_PIN AB5            [get_ports ENA_N[6]]
set_property IOSTANDARD LVDS_25         [get_ports ENA_N[6]]
set_property PULLDOWN TRUE              [get_ports ENA_P[6]]

set_property IOSTANDARD LVDS_25         [get_ports ENA_P[7]]
set_property PACKAGE_PIN V7             [get_ports ENA_P[7]]
set_property PACKAGE_PIN W7             [get_ports ENA_N[7]]
set_property IOSTANDARD LVDS_25         [get_ports ENA_N[7]]
set_property PULLDOWN TRUE              [get_ports ENA_P[7]]

########################## CS VMM3 ##############################

set_property PACKAGE_PIN E19            [get_ports CS[0]]
set_property IOSTANDARD LVCMOS12        [get_ports CS[0]]

set_property PACKAGE_PIN F16            [get_ports CS[1]]
set_property IOSTANDARD LVCMOS12        [get_ports CS[1]]
set_property PULLDOWN TRUE              [get_ports CS[1]]

set_property PACKAGE_PIN D14            [get_ports CS[2]]
set_property IOSTANDARD LVCMOS12        [get_ports CS[2]]
set_property PULLDOWN TRUE              [get_ports CS[2]]

set_property PACKAGE_PIN U7             [get_ports CS[3]]
set_property IOSTANDARD LVCMOS25        [get_ports CS[3]]
set_property PULLDOWN TRUE              [get_ports CS[3]]

set_property PACKAGE_PIN K2             [get_ports CS[4]]
set_property IOSTANDARD LVCMOS25        [get_ports CS[4]]
set_property PULLDOWN TRUE              [get_ports CS[4]]

set_property PACKAGE_PIN N4             [get_ports CS[5]]
set_property IOSTANDARD LVCMOS25        [get_ports CS[5]]
set_property PULLDOWN TRUE              [get_ports CS[5]]

set_property PACKAGE_PIN W1             [get_ports CS[6]]
set_property IOSTANDARD LVCMOS25        [get_ports CS[6]]
set_property PULLDOWN TRUE              [get_ports CS[6]]

set_property PACKAGE_PIN R6             [get_ports CS[7]]
set_property IOSTANDARD LVCMOS25        [get_ports CS[7]]
set_property PULLDOWN TRUE              [get_ports CS[7]]

########################## SCK VMM3 ##############################

set_property PACKAGE_PIN D19            [get_ports SCK[0]]
set_property IOSTANDARD LVCMOS12        [get_ports SCK[0]]

set_property PACKAGE_PIN E17            [get_ports SCK[1]]
set_property IOSTANDARD LVCMOS12        [get_ports SCK[1]]
set_property PULLDOWN TRUE              [get_ports SCK[1]]

set_property PACKAGE_PIN D15            [get_ports SCK[2]]
set_property IOSTANDARD LVCMOS12        [get_ports SCK[2]]
set_property PULLDOWN TRUE              [get_ports SCK[2]]

set_property PACKAGE_PIN E22            [get_ports SCK[3]]
set_property IOSTANDARD LVCMOS12        [get_ports SCK[3]]
set_property PULLDOWN TRUE              [get_ports SCK[3]]

set_property PACKAGE_PIN J2             [get_ports SCK[4]]
set_property IOSTANDARD LVCMOS25        [get_ports SCK[4]]
set_property PULLDOWN TRUE              [get_ports SCK[4]]

set_property PACKAGE_PIN N3             [get_ports SCK[5]]
set_property IOSTANDARD LVCMOS25        [get_ports SCK[5]]
set_property PULLDOWN TRUE              [get_ports SCK[5]]

set_property PACKAGE_PIN Y1             [get_ports SCK[6]]
set_property IOSTANDARD LVCMOS25        [get_ports SCK[6]]
set_property PULLDOWN TRUE              [get_ports SCK[6]]

set_property PACKAGE_PIN T6             [get_ports SCK[7]]
set_property IOSTANDARD LVCMOS25        [get_ports SCK[7]]
set_property PULLDOWN TRUE              [get_ports SCK[7]]

########################## CKART VMM3 ##############################

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKART_P[0]]
set_property PACKAGE_PIN C18            [get_ports CKART_P[0]]
set_property PACKAGE_PIN C19            [get_ports CKART_N[0]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKART_N[0]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKART_P[1]]
set_property PACKAGE_PIN E21            [get_ports CKART_P[1]]
set_property PACKAGE_PIN D21            [get_ports CKART_N[1]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKART_N[1]]
set_property PULLDOWN TRUE              [get_ports CKART_P[1]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKART_P[2]]
set_property PACKAGE_PIN E16            [get_ports CKART_P[2]]
set_property PACKAGE_PIN D16            [get_ports CKART_N[2]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKART_N[2]]
set_property PULLDOWN TRUE              [get_ports CKART_P[2]]

set_property IOSTANDARD LVDS_25         [get_ports CKART_P[3]]
set_property PACKAGE_PIN M6             [get_ports CKART_P[3]]
set_property PACKAGE_PIN M5             [get_ports CKART_N[3]]
set_property IOSTANDARD LVDS_25         [get_ports CKART_N[3]]
set_property PULLDOWN TRUE              [get_ports CKART_P[3]]

set_property IOSTANDARD LVDS_25         [get_ports CKART_P[4]]
set_property PACKAGE_PIN H4             [get_ports CKART_P[4]]
set_property PACKAGE_PIN G4             [get_ports CKART_N[4]]
set_property IOSTANDARD LVDS_25         [get_ports CKART_N[4]]
set_property PULLDOWN TRUE              [get_ports CKART_P[4]]

set_property IOSTANDARD LVDS_25         [get_ports CKART_P[5]]
set_property PACKAGE_PIN T1             [get_ports CKART_P[5]]
set_property PACKAGE_PIN U1             [get_ports CKART_N[5]]
set_property IOSTANDARD LVDS_25         [get_ports CKART_N[5]]
set_property PULLDOWN TRUE              [get_ports CKART_P[5]]

set_property IOSTANDARD LVDS_25         [get_ports CKART_P[6]]
set_property PACKAGE_PIN AA1            [get_ports CKART_P[6]]
set_property PACKAGE_PIN AB1            [get_ports CKART_N[6]]
set_property IOSTANDARD LVDS_25         [get_ports CKART_N[6]]
set_property PULLDOWN TRUE              [get_ports CKART_P[6]]

set_property IOSTANDARD LVDS_25         [get_ports CKART_P[7]]
set_property PACKAGE_PIN W6             [get_ports CKART_P[7]]
set_property PACKAGE_PIN W5             [get_ports CKART_N[7]]
set_property IOSTANDARD LVDS_25         [get_ports CKART_N[7]]
set_property PULLDOWN TRUE              [get_ports CKART_P[7]]

set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKART_P[8]]
set_property PACKAGE_PIN B17            [get_ports CKART_P[8]]
set_property PACKAGE_PIN B18            [get_ports CKART_N[8]]
set_property IOSTANDARD DIFF_HSUL_12    [get_ports CKART_N[8]]
set_property PULLDOWN TRUE              [get_ports CKART_P[8]]

########################### XADC GPVMM #############################
# Dedicated Analog Inputs
set_property IOSTANDARD LVCMOS25        [get_ports VP0]
set_property PACKAGE_PIN L10            [get_ports VP0]
set_property IOSTANDARD LVCMOS25        [get_ports VN0]
set_property PACKAGE_PIN M9             [get_ports VN0]

### Analog Multiplexer Pins
set_property PACKAGE_PIN T20            [get_ports MUXADDR[0]]
set_property IOSTANDARD LVCMOS33        [get_ports MUXADDR[0]]
set_property PULLDOWN TRUE              [get_ports MUXADDR[0]]
set_property PACKAGE_PIN P14            [get_ports MUXADDR[1]]
set_property IOSTANDARD LVCMOS33        [get_ports MUXADDR[1]]
set_property PULLDOWN TRUE              [get_ports MUXADDR[1]]
set_property PACKAGE_PIN R14            [get_ports MUXADDR[2]]
set_property IOSTANDARD LVCMOS33        [get_ports MUXADDR[2]]
set_property PULLDOWN TRUE              [get_ports MUXADDR[2]]
set_property PACKAGE_PIN R18            [get_ports MUXADDR_P]
set_property IOSTANDARD TMDS_33         [get_ports MUXADDR_P]
set_property PACKAGE_PIN T18            [get_ports MUXADDR_N]
set_property IOSTANDARD TMDS_33         [get_ports MUXADDR_N]
set_property PULLDOWN TRUE              [get_ports MUXADDR_P]

##PDO
set_property PACKAGE_PIN G16            [get_ports VAUX8_V_N]
set_property PACKAGE_PIN G15            [get_ports VAUX8_V_P]
##TDO
set_property PACKAGE_PIN H14            [get_ports VAUX1_V_N]
set_property PACKAGE_PIN J14            [get_ports VAUX1_V_P]

set_property PACKAGE_PIN AB10           [get_ports VAUX0_V_N]
set_property PACKAGE_PIN AA9            [get_ports VAUX0_V_P]
set_property PULLDOWN TRUE              [get_ports VAUX0_V_P]

set_property PACKAGE_PIN J22            [get_ports VAUX2_V_P]
set_property PACKAGE_PIN H22            [get_ports VAUX2_V_N]
set_property PULLDOWN TRUE              [get_ports VAUX2_V_P]

set_property PACKAGE_PIN K22            [get_ports VAUX3_V_N]
set_property PACKAGE_PIN K21            [get_ports VAUX3_V_P]
set_property PULLDOWN TRUE              [get_ports VAUX3_V_P]

set_property PACKAGE_PIN H15            [get_ports VAUX9_V_N]
set_property PACKAGE_PIN J15            [get_ports VAUX9_V_P]
set_property PULLDOWN TRUE              [get_ports VAUX9_V_P]

set_property PACKAGE_PIN W17            [get_ports VAUX10_V_N]
set_property PACKAGE_PIN V17            [get_ports VAUX10_V_P]
set_property PULLDOWN TRUE              [get_ports VAUX10_V_P]

set_property PACKAGE_PIN U18            [get_ports VAUX10_V_N]
set_property PACKAGE_PIN U17            [get_ports VAUX11_V_P]
set_property PULLDOWN TRUE              [get_ports VAUX11_V_P]

set_property IOSTANDARD LVCMOS25        [get_ports VAUX0_V_P]
set_property IOSTANDARD LVCMOS25        [get_ports VAUX0_V_N]
set_property IOSTANDARD LVCMOS12        [get_ports VAUX1_V_N]
set_property IOSTANDARD LVCMOS12        [get_ports VAUX1_V_P]
set_property IOSTANDARD LVCMOS12        [get_ports VAUX2_V_N]
set_property IOSTANDARD LVCMOS12        [get_ports VAUX2_V_P]
set_property IOSTANDARD LVCMOS12        [get_ports VAUX3_V_N]
set_property IOSTANDARD LVCMOS12        [get_ports VAUX3_V_P]
set_property IOSTANDARD LVCMOS12        [get_ports VAUX8_V_N]
set_property IOSTANDARD LVCMOS12        [get_ports VAUX8_V_P]
set_property IOSTANDARD LVCMOS12        [get_ports VAUX9_V_N]
set_property IOSTANDARD LVCMOS12        [get_ports VAUX9_V_P]
set_property IOSTANDARD LVCMOS33        [get_ports VAUX10_V_N]
set_property IOSTANDARD LVCMOS33        [get_ports VAUX10_V_P]
set_property IOSTANDARD LVCMOS33        [get_ports VAUX10_V_N]
set_property IOSTANDARD LVCMOS33        [get_ports VAUX11_V_P]

######################### SPI FLASH ##########################

set_property IOSTANDARD LVCMOS33        [get_ports IO0_IO]
set_property IOSTANDARD LVCMOS33        [get_ports IO1_IO]
set_property IOSTANDARD LVCMOS33        [get_ports SS_IO]
set_property PACKAGE_PIN P22            [get_ports IO0_IO]
set_property PACKAGE_PIN R22            [get_ports IO1_IO]
set_property PACKAGE_PIN T19            [get_ports SS_IO]
set_property OFFCHIP_TERM NONE          [get_ports IO0_IO]
set_property OFFCHIP_TERM NONE          [get_ports IO1_IO]
set_property OFFCHIP_TERM NONE          [get_ports SS_IO]

######################### I2C EEPROM #########################

set_property PACKAGE_PIN L6             [get_ports SCL]
set_property IOSTANDARD LVCMOS25        [get_ports SCL]
set_property PULLDOWN TRUE              [get_ports SCL]

set_property PACKAGE_PIN F21            [get_ports SDA]
set_property IOSTANDARD LVCMOS12        [get_ports SDA]
set_property PULLDOWN TRUE              [get_ports SDA]

set_property PACKAGE_PIN AA3            [get_ports EEPROM_GND[0]]
set_property IOSTANDARD LVCMOS25        [get_ports EEPROM_GND[0]]
set_property PULLDOWN TRUE              [get_ports EEPROM_GND[0]]

set_property PACKAGE_PIN J5             [get_ports EEPROM_GND[1]]
set_property IOSTANDARD LVCMOS25        [get_ports EEPROM_GND[1]]
set_property PULLDOWN TRUE              [get_ports EEPROM_GND[1]]

set_property PACKAGE_PIN U6             [get_ports EEPROM_GND[2]]
set_property IOSTANDARD LVCMOS25        [get_ports EEPROM_GND[2]]
set_property PULLDOWN TRUE              [get_ports EEPROM_GND[2]]

set_property PACKAGE_PIN W15            [get_ports EEPROM_GND[3]]
set_property IOSTANDARD LVCMOS25        [get_ports EEPROM_GND[3]]
set_property PULLDOWN TRUE              [get_ports EEPROM_GND[3]]

################# GENERAL CONSTRAINTS ########################
set_property CONFIG_MODE SPIx4                  [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4    [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE    [current_design]